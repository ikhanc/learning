a:10:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:23:"Generating SSH Keypairs";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1;}i:4;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:296:"Using cryptographic keypairs for authentication makes authentication much easier for you and allows you to completely disable password authentication on your server (if you wish) which is a much more secure way of handling authentication since password guessing is rendered useless in that case.
";}i:2;i:41;}i:5;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:694:"
#generate public/private keypair
root@server ~# ssh-keygen -b 1024 -C user@domain.com

#copy contents of public key to your list of authorized keys
root@server ~# cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys
The above command will also generate a private keyfile (id_rsa by default) and you'll want to make sure to protect that key.  Do not leave it on your server if it is also used to authenticate to your server.

If your home system is running Linux, you can use that keyfile directly to authenticate via ssh.  If you are connecting through Putty, you'll want to download a copy of Puttygen and use that to convert your private key into a format which is compatible with Putty. ";}i:2;i:3;i:3;s:695:">
#generate public/private keypair
root@server ~# ssh-keygen -b 1024 -C user@domain.com

#copy contents of public key to your list of authorized keys
root@server ~# cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys
The above command will also generate a private keyfile (id_rsa by default) and you'll want to make sure to protect that key.  Do not leave it on your server if it is also used to authenticate to your server.

If your home system is running Linux, you can use that keyfile directly to authenticate via ssh.  If you are connecting through Putty, you'll want to download a copy of Puttygen and use that to convert your private key into a format which is compatible with Putty. ";}i:2;i:342;}i:6;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:0:"";}i:2;i:1044;}i:7;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1044;}i:8;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1044;}i:9;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:1044;}}