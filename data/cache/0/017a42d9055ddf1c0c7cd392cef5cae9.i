a:212:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:43:"Securing WordPress Sites (Basic + Advanced)";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1;}i:4;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"LINKS:
";}i:2;i:62;}i:5;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:33:"http://wpsecure.net/server-guide/";i:1;N;}i:2;i:69;}i:6;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:102;}i:7;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:105;}i:8;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:5:"Basic";i:1;i:2;i:2;i:105;}i:2;i:105;}i:9;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:105;}i:10;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:105;}i:11;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:123;}i:12;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:124;}i:13;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:36:"
1. Keep your WordPress up to date. ";}i:2;i:126;}i:14;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:162;}i:15;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:56:"
2. Keep your home computer and WiFi connection secure. ";}i:2;i:164;}i:16;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:220;}i:17;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:106:"
This seems obvious but keyloggers and malware on your home computer/WiFi can easily compromise your site.";}i:2;i:222;}i:18;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:328;}i:19;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:330;}i:20;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:330;}i:21;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:61:"3. Get your plugins and themes from the official repository. ";}i:2;i:332;}i:22;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:393;}i:23;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:77:"
Get your themes/plugins from a reputable source or the official repository. ";}i:2;i:395;}i:24;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:472;}i:25;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:474;}i:26;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:474;}i:27;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:24:"4. Choose a solid host. ";}i:2;i:476;}i:28;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:500;}i:29;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:35:"
5. Choose a really good password. ";}i:2;i:502;}i:30;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:537;}i:31;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:99:"
A good password is combination of small letters, capital letters, numbers and special characters. ";}i:2;i:539;}i:32;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:638;}i:33;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:638;}i:34;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:44:"6. Update your plugins and theme regularly 
";}i:2;i:640;}i:35;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:684;}i:36;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:686;}i:37;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:689;}i:38;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:8:"Advanced";i:1;i:2;i:2;i:689;}i:2;i:689;}i:39;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:689;}i:40;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:689;}i:41;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:714;}i:42;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:0:"";}i:2;i:716;}i:43;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:717;}i:44;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:717;}i:45;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:26:"1.Limit Admin access by IP";i:1;i:3;i:2;i:717;}i:2;i:717;}i:46;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:717;}i:47;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:717;}i:48;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:756;}i:49;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:757;}i:50;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:163:"
If you have static IP then can limiting the WP admin section by IP address in your .htaccess file. This .htaccess must go in your admin folder, not the root path.";}i:2;i:759;}i:51;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:922;}i:52;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:924;}i:53;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:924;}i:54;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:106:"
<LIMIT GET POST PUT>
order deny,allow
deny from all
allow from xx.xx.xx.xx  //( your static IP)
</LIMIT> ";}i:2;i:3;i:3;s:107:">
<LIMIT GET POST PUT>
order deny,allow
deny from all
allow from xx.xx.xx.xx  //( your static IP)
</LIMIT> ";}i:2;i:933;}i:55;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1047;}i:56;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1051;}i:57;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:27:"2. Limit Login access by IP";i:1;i:3;i:2;i:1051;}i:2;i:1051;}i:58;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:1051;}i:59;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1051;}i:60;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:188:"The login file, wp-login.php, does not reside in the admin folder, it is in the root so you can deny access to just the login by IP as well. This must be in the root .htaccess file/folder.";}i:2;i:1093;}i:61;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1281;}i:62;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1281;}i:63;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:101:"
<Files wp-login.php>
Order Deny,Allow
Deny from All
Allow from x.x.x.x //( your static IP)
</Files> ";}i:2;i:3;i:3;s:102:">
<Files wp-login.php>
Order Deny,Allow
Deny from All
Allow from x.x.x.x //( your static IP)
</Files> ";}i:2;i:1291;}i:64;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1400;}i:65;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1404;}i:66;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:26:"3. Disable Trace and Track";i:1;i:3;i:2;i:1404;}i:2;i:1404;}i:67;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:1404;}i:68;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1404;}i:69;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:260:"HTTP Trace attack (XST) can be used to return header requests and grab cookies and other information and is used along with a cross site scripting attacks (XSS) . This can be turned off in your apache configuration file or through .htaccess with the following.";}i:2;i:1445;}i:70;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1705;}i:71;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1705;}i:72;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:76:"
RewriteEngine On
RewriteCond %{REQUEST_METHOD} ^TRACE
RewriteRule .* - [F] ";}i:2;i:3;i:3;s:77:">
RewriteEngine On
RewriteCond %{REQUEST_METHOD} ^TRACE
RewriteRule .* - [F] ";}i:2;i:1715;}i:73;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1799;}i:74;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1803;}i:75;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:31:"4. Forbid Proxy comment posting";i:1;i:3;i:2;i:1803;}i:2;i:1803;}i:76;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:1803;}i:77;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1803;}i:78;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:158:"This will deny any requests that use a proxy server when posting to comments eliminating some spam and proxy requests, script courtesy of perishablepress.com.";}i:2;i:1846;}i:79;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2004;}i:80;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2004;}i:81;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:368:"

RewriteCond %{REQUEST_METHOD} =POST
RewriteCond %{HTTP:VIA}%{HTTP:FORWARDED}%{HTTP:USERAGENT_VIA}%{HTTP:X_FORWARDED_FOR}%{HTTP:PROXY_CONNECTION} !^$ [OR]
RewriteCond %{HTTP:XPROXY_CONNECTION}%{HTTP:HTTP_PC_REMOTE_ADDR}%{HTTP:HTTP_CLIENT_IP} !^$
RewriteCond %{REQUEST_URI} !^/(wp-login.php|wp-admin/|wp-content/plugins/|wp-includes/).* [NC]
RewriteRule .* - [F,NS,L] ";}i:2;i:3;i:3;s:369:">

RewriteCond %{REQUEST_METHOD} =POST
RewriteCond %{HTTP:VIA}%{HTTP:FORWARDED}%{HTTP:USERAGENT_VIA}%{HTTP:X_FORWARDED_FOR}%{HTTP:PROXY_CONNECTION} !^$ [OR]
RewriteCond %{HTTP:XPROXY_CONNECTION}%{HTTP:HTTP_PC_REMOTE_ADDR}%{HTTP:HTTP_CLIENT_IP} !^$
RewriteCond %{REQUEST_URI} !^/(wp-login.php|wp-admin/|wp-content/plugins/|wp-includes/).* [NC]
RewriteRule .* - [F,NS,L] ";}i:2;i:2013;}i:82;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2389;}i:83;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:2393;}i:84;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:23:"5. Disable PHP settings";i:1;i:3;i:2;i:2393;}i:2;i:2393;}i:85;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:2393;}i:86;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2393;}i:87;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:169:"Please be aware some these settings will take away some functionality, these edits go in your php.ini file ( some shared hosting does not allow for editing the php.ini).";}i:2;i:2428;}i:88;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2597;}i:89;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2597;}i:90;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:2599;}i:91;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:792:"
display_errors = Off       //safe to disable on live site
register_globals = Off    //off by default but a good reminder to check
expose_php = Off         //safe to disable
allow_url_fopen = Off    //might break something
allow_url_include = Off  //might break something
log_errors = On           //logging errors is always a good idea if you check them
error_log = /var/log/phperror.log
enable_dl = Off           //might break something
disable_functions="popen,exec,system,passthru,proc_open,shell_exec,show_source,php
file_uploads = Off //will most likely break something
NOTE:- Highly advisable, at the very least, to turn display errors to off, many WP files ( some depreciated but still included) will return the path/location of your web root since they return an error on page load. ";}i:2;i:3;i:3;s:793:">
display_errors = Off       //safe to disable on live site
register_globals = Off    //off by default but a good reminder to check
expose_php = Off         //safe to disable
allow_url_fopen = Off    //might break something
allow_url_include = Off  //might break something
log_errors = On           //logging errors is always a good idea if you check them
error_log = /var/log/phperror.log
enable_dl = Off           //might break something
disable_functions="popen,exec,system,passthru,proc_open,shell_exec,show_source,php
file_uploads = Off //will most likely break something
NOTE:- Highly advisable, at the very least, to turn display errors to off, many WP files ( some depreciated but still included) will return the path/location of your web root since they return an error on page load. ";}i:2;i:2605;}i:92;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3405;}i:93;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:3407;}i:94;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:21:"6. Enable Suhosin PHP";i:1;i:3;i:2;i:3407;}i:2;i:3407;}i:95;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:3407;}i:96;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3407;}i:97;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:255:"Suhosin is a PHP security hardening system the that protects against a variety of exploits and works with any normal PHP installation. Some PHP installs already bundle Suhosin, if you want to check if you have it installed create an empty php file and add";}i:2;i:3440;}i:98;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3695;}i:99;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3695;}i:100;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:1065:"
<? phpinfo(); ?> <code/>
and then check it in your browser ( remember to delete this file when your done!).

To install Suhosin you can follow the guidelines here: Install Suhosin

 

==== 7. Remove WordPress header outputs ====

WordPress can add a lot of stuff in your header for various services, this will remove everything, but take care, it also removes some functionality ( for instance if someone is looking for your RSS feed). If you want to keep some just comment the line out.

 

<code>
// remove junk from head
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'noindex', 1); ";}i:2;i:3;i:3;s:1066:">
<? phpinfo(); ?> <code/>
and then check it in your browser ( remember to delete this file when your done!).

To install Suhosin you can follow the guidelines here: Install Suhosin

 

==== 7. Remove WordPress header outputs ====

WordPress can add a lot of stuff in your header for various services, this will remove everything, but take care, it also removes some functionality ( for instance if someone is looking for your RSS feed). If you want to keep some just comment the line out.

 

<code>
// remove junk from head
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'noindex', 1); ";}i:2;i:3705;}i:101;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4778;}i:102;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:4782;}i:103;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:39:"8. Remove l10n script (added in WP 3.1)";i:1;i:3;i:2;i:4782;}i:2;i:4782;}i:104;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:4782;}i:105;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4782;}i:106;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:45:"This script help translate comments entities.";}i:2;i:4833;}i:107;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4878;}i:108;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4878;}i:109;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:54:"
if (!is_admin()) {
  wp_deregister_script('l10n');
} ";}i:2;i:3;i:3;s:55:">
if (!is_admin()) {
  wp_deregister_script('l10n');
} ";}i:2;i:4888;}i:110;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4950;}i:111;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:4954;}i:112;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:36:"9. Hide hard urls, use relative ones";i:1;i:3;i:2;i:4954;}i:2;i:4954;}i:113;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:4954;}i:114;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4954;}i:115;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:31:"https://gist.github.com/2315279";i:1;N;}i:2;i:5002;}i:116;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5033;}i:117;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:5035;}i:118;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:21:"10. Hide /wp-content/";i:1;i:3;i:2;i:5035;}i:2;i:5035;}i:119;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:5035;}i:120;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5035;}i:121;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:31:"https://gist.github.com/2315295";i:1;N;}i:2;i:5068;}i:122;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5099;}i:123;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:5101;}i:124;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:26:"11. Deny bad query strings";i:1;i:3;i:2;i:5101;}i:2;i:5101;}i:125;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:5101;}i:126;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5101;}i:127;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:373:"This script goes in your .htaccess and will attempt to prevent malicious string attacks on your site (XSS). Please be aware some of these strings might be used for plugins or themes and doing so will disable the functionality. This script from perishablepress is fairly safe to use and should not break anything important. A more advanced one can be found on askapache.com.";}i:2;i:5139;}i:128;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5512;}i:129;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5512;}i:130;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:787:"
# QUERY STRING EXPLOITS
<IfModule mod_rewrite.c>
 RewriteCond %{QUERY_STRING} ../    [NC,OR]
 RewriteCond %{QUERY_STRING} boot.ini [NC,OR]
 RewriteCond %{QUERY_STRING} tag=     [NC,OR]
 RewriteCond %{QUERY_STRING} ftp:     [NC,OR]
 RewriteCond %{QUERY_STRING} http:    [NC,OR]
 RewriteCond %{QUERY_STRING} https:   [NC,OR]
 RewriteCond %{QUERY_STRING} mosConfig [NC,OR]
 RewriteCond %{QUERY_STRING} ^.*([|]|(|)|<|>|'|"|;|?|*).* [NC,OR]
 RewriteCond %{QUERY_STRING} ^.*(%22|%27|%3C|%3E|%5C|%7B|%7C).* [NC,OR]
 RewriteCond %{QUERY_STRING} ^.*(%0|%A|%B|%C|%D|%E|%F|127.0).* [NC,OR]
 RewriteCond %{QUERY_STRING} ^.*(globals|encode|config|localhost|loopback).* [NC,OR]
 RewriteCond %{QUERY_STRING} ^.*(request|select|insert|union|declare|drop).* [NC]
 RewriteRule ^(.*)$ - [F,L]
</IfModule> ";}i:2;i:3;i:3;s:788:">
# QUERY STRING EXPLOITS
<IfModule mod_rewrite.c>
 RewriteCond %{QUERY_STRING} ../    [NC,OR]
 RewriteCond %{QUERY_STRING} boot.ini [NC,OR]
 RewriteCond %{QUERY_STRING} tag=     [NC,OR]
 RewriteCond %{QUERY_STRING} ftp:     [NC,OR]
 RewriteCond %{QUERY_STRING} http:    [NC,OR]
 RewriteCond %{QUERY_STRING} https:   [NC,OR]
 RewriteCond %{QUERY_STRING} mosConfig [NC,OR]
 RewriteCond %{QUERY_STRING} ^.*([|]|(|)|<|>|'|"|;|?|*).* [NC,OR]
 RewriteCond %{QUERY_STRING} ^.*(%22|%27|%3C|%3E|%5C|%7B|%7C).* [NC,OR]
 RewriteCond %{QUERY_STRING} ^.*(%0|%A|%B|%C|%D|%E|%F|127.0).* [NC,OR]
 RewriteCond %{QUERY_STRING} ^.*(globals|encode|config|localhost|loopback).* [NC,OR]
 RewriteCond %{QUERY_STRING} ^.*(request|select|insert|union|declare|drop).* [NC]
 RewriteRule ^(.*)$ - [F,L]
</IfModule> ";}i:2;i:5522;}i:131;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:6317;}i:132;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:6321;}i:133;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:44:"12. Delete default MySQL “test” database";i:1;i:3;i:2;i:6321;}i:2;i:6321;}i:134;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:6321;}i:135;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:6321;}i:136;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:170:"MySQL installs with a default database, it is very advisable that you delete this as it allows for any user role access, also delete any other unused databases and users.";}i:2;i:6377;}i:137;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:6547;}i:138;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:6547;}i:139;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:28:"
mysql> DROP DATABASE test; ";}i:2;i:3;i:3;s:29:">
mysql> DROP DATABASE test; ";}i:2;i:6557;}i:140;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:6593;}i:141;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:6597;}i:142;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:46:"13. Fingerprint source code and header removal";i:1;i:3;i:2;i:6597;}i:2;i:6597;}i:143;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:6597;}i:144;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:6597;}i:145;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:630:"Many plugins and themes write information into the source code, and some even write info directly to your HTTP headers. This can be used to fingerprint which version and plugins you are using. There is no easy way to get rid of this info, sometimes there is a action filter you can use but more often then not you have to dive in and directly edit the files. There are some caching and minify plugins that remove source code comments but they do not work 100% of the time. PHP 5.3 also has a new header function that can sometimes be effective, this example shows how to remove the x-powered-by header if it is set to “geuss”.";}i:2;i:6655;}i:146;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:7285;}i:147;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:7285;}i:148;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:39:"
header_remove('x-powered-by', guess); ";}i:2;i:3;i:3;s:40:">
header_remove('x-powered-by', guess); ";}i:2;i:7295;}i:149;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:7342;}i:150;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:7346;}i:151;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:40:"14. Disable non WordPress attack vectors";i:1;i:3;i:2;i:7346;}i:2;i:7346;}i:152;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:7346;}i:153;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:7346;}i:154;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:271:"You can disable non WordpPress services running on your server. For instance some attack vectors go straight for your phpMyAdmin, which can compromise you entire server with full access. For the truly paranoid you should disable services like phpMyAdmin, Cpanel, Webmin, ";}i:2;i:7398;}i:155;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:3:"FTP";}i:2;i:7669;}i:156;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:99:", or anything else that might allow entry, and lock down all ports to only allow access to HTTP 80.";}i:2;i:7672;}i:157;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:7771;}i:158;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:7773;}i:159;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:39:"15. Use Apache authorization for access";i:1;i:3;i:2;i:7773;}i:2;i:7773;}i:160;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:7773;}i:161;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:7773;}i:162;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:200:"You can restrict site usage, say for your admin section, based on apache authorization and password files and groups. This process is to detailed to go into here, so have a look at the official docs, ";}i:2;i:7824;}i:163;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:28:"http://httpd.apache.org/Auth";i:1;N;}i:2;i:8024;}i:164;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:130:". Another option is to check out askapache.com’s wordpress plugin that interfaces directly with this AskApache Password Protect.";}i:2;i:8052;}i:165;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:8182;}i:166;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:8187;}i:167;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:27:"16. PHP intrusion detection";i:1;i:3;i:2;i:8187;}i:2;i:8187;}i:168;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:8187;}i:169;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:8187;}i:170;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:54:"PHPIDS is an intrusion detection layer for PHP run by ";}i:2;i:8226;}i:171;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:18:"http://phpids.org/";i:1;N;}i:2;i:8280;}i:172;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:231:" that blocks malicious code from doing nasty things to your site. It is especially useful if your site has any user submitted content forms such as comments and contacts. They even have a demo where you can test malicious scripts, ";}i:2;i:8298;}i:173;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:23:"http://demo.phpids.org/";i:1;N;}i:2;i:8529;}i:174;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:234:"
The cool thing about PHPIDS is that it is constantly updated through an XML feed and there is a WordPress plugin that works directly with the updates, it even logs intrusion attempts. It is called Mute Screamer and can be found here ";}i:2;i:8552;}i:175;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:50:"http://wordpress.org/extend/plugins/mute-screamer/";i:1;N;}i:2;i:8786;}i:176;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:8836;}i:177;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:8841;}i:178;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:45:"17. Change the database prefix on a live site";i:1;i:3;i:2;i:8841;}i:2;i:8841;}i:179;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:8841;}i:180;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:8841;}i:181;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:595:"As mentioned in the basic guide WordPress installs with a default database prefix (‘wp_”) which can be used by bots and attackers to gain access to your database by easily guessing your database names. Changing the prefix on a live site can be a scary task so proceed with caution. There are many methods to do this and even some plugins, it is advisable to do some research and testing before hand. Below I have outlined the steps for a manual change where you download the dump and change the text with a text editor as a “safe method”, alternatively you can do this directly in MySQL.";}i:2;i:8898;}i:182;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:9493;}i:183;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:9493;}i:184;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:663:"1. Make a complete database dump through MySQL ( not through the WordPress admin).
2. Copy the dump into 2 files, one back-up that you don’t touch, and 1 for editing.
3. Use a solid text editor to find/replace all instances of “wp_” with your new prefix on the dump you want to edit.
4. De-activate your plugins.
5. Turn on maintenance mode at an hour when traffic is low.
6. Drop your old database and import your edited one with the new prefix’s
7. Change your database settings in wp-config.php to the new prefix.
8. Re-activate your plugins
9. Refresh your permalink structure by hitting save ( even without changing structure)
10. Cross your fingers.";}i:2;i:9495;}i:185;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:10158;}i:186;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:10163;}i:187;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:34:"18. Additional resources and Links";i:1;i:3;i:2;i:10163;}i:2;i:10163;}i:188;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:10163;}i:189;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:10163;}i:190;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:260:"Perishable Press maintains a great security section and a blacklist of referrers –>blacklist.txt
AskApache, everything you ever wanted to know and more about apache, file permissions, WordPress and .htaccess.
Check out our Server Guide for locking down your ";}i:2;i:10209;}i:191;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:2:"OS";}i:2;i:10469;}i:192;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:".";}i:2;i:10471;}i:193;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:10472;}i:194;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:10476;}i:195;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:41:"19. Character string filter for .htaccess";i:1;i:3;i:2;i:10476;}i:2;i:10476;}i:196;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:10476;}i:197;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:10476;}i:198;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:203:"I left this to the end due to the length, this blocks bad character matches ( mostly XSS) from messing about with your site, this goes in your root .htaccess file, some strings might break functionality.";}i:2;i:10529;}i:199;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:10732;}i:200;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:10732;}i:201;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:1936:"

RedirectMatch 403 ^
 RedirectMatch 403 `
 RedirectMatch 403 {
 RedirectMatch 403 }
 RedirectMatch 403 ~
 RedirectMatch 403 "
 RedirectMatch 403 $
 RedirectMatch 403 <
 RedirectMatch 403 >
 RedirectMatch 403 |
 RedirectMatch 403 ..
 RedirectMatch 403 //
 RedirectMatch 403 %0
 RedirectMatch 403 %A
 RedirectMatch 403 %B
 RedirectMatch 403 %C
 RedirectMatch 403 %D
 RedirectMatch 403 %E
 RedirectMatch 403 %F
 RedirectMatch 403 %22
 RedirectMatch 403 %27
 RedirectMatch 403 %28
 RedirectMatch 403 %29
 RedirectMatch 403 %3C
 RedirectMatch 403 %3E
 RedirectMatch 403 %3F
 RedirectMatch 403 %5B
 RedirectMatch 403 %5C
 RedirectMatch 403 %5D
 RedirectMatch 403 %7B
 RedirectMatch 403 %7C
 RedirectMatch 403 %7D
 # COMMON PATTERNS
 Redirectmatch 403 _vpi
 RedirectMatch 403 .inc
 Redirectmatch 403 xAou6
 Redirectmatch 403 db_name
 Redirectmatch 403 select(
 Redirectmatch 403 convert(
 Redirectmatch 403 /query/
 RedirectMatch 403 ImpEvData
 Redirectmatch 403 .XMLHTTP
 Redirectmatch 403 proxydeny
 RedirectMatch 403 function.
 Redirectmatch 403 remoteFile
 Redirectmatch 403 servername
 Redirectmatch 403 &rptmode=
 Redirectmatch 403 sys_cpanel
 RedirectMatch 403 db_connect
 RedirectMatch 403 doeditconfig
 RedirectMatch 403 check_proxy
 Redirectmatch 403 system_user
 Redirectmatch 403 /(null)/
 Redirectmatch 403 clientrequest
 Redirectmatch 403 option_value
 RedirectMatch 403 ref.outcontrol
 # SPECIFIC EXPLOITS
 RedirectMatch 403 errors.
 RedirectMatch 403 config.
 RedirectMatch 403 include.
 RedirectMatch 403 display.
 RedirectMatch 403 register.
 Redirectmatch 403 password.
 RedirectMatch 403 maincore.
 RedirectMatch 403 authorize.
 Redirectmatch 403 macromates.
 RedirectMatch 403 head_auth.
 RedirectMatch 403 submit_links.
 RedirectMatch 403 change_action.
 Redirectmatch 403 com_facileforms/
 RedirectMatch 403 admin_db_utilities.
 RedirectMatch 403 admin.webring.docs.
 Redirectmatch 403 Table/Latest/index.
</IfModule>] ";}i:2;i:3;i:3;s:1937:">

RedirectMatch 403 ^
 RedirectMatch 403 `
 RedirectMatch 403 {
 RedirectMatch 403 }
 RedirectMatch 403 ~
 RedirectMatch 403 "
 RedirectMatch 403 $
 RedirectMatch 403 <
 RedirectMatch 403 >
 RedirectMatch 403 |
 RedirectMatch 403 ..
 RedirectMatch 403 //
 RedirectMatch 403 %0
 RedirectMatch 403 %A
 RedirectMatch 403 %B
 RedirectMatch 403 %C
 RedirectMatch 403 %D
 RedirectMatch 403 %E
 RedirectMatch 403 %F
 RedirectMatch 403 %22
 RedirectMatch 403 %27
 RedirectMatch 403 %28
 RedirectMatch 403 %29
 RedirectMatch 403 %3C
 RedirectMatch 403 %3E
 RedirectMatch 403 %3F
 RedirectMatch 403 %5B
 RedirectMatch 403 %5C
 RedirectMatch 403 %5D
 RedirectMatch 403 %7B
 RedirectMatch 403 %7C
 RedirectMatch 403 %7D
 # COMMON PATTERNS
 Redirectmatch 403 _vpi
 RedirectMatch 403 .inc
 Redirectmatch 403 xAou6
 Redirectmatch 403 db_name
 Redirectmatch 403 select(
 Redirectmatch 403 convert(
 Redirectmatch 403 /query/
 RedirectMatch 403 ImpEvData
 Redirectmatch 403 .XMLHTTP
 Redirectmatch 403 proxydeny
 RedirectMatch 403 function.
 Redirectmatch 403 remoteFile
 Redirectmatch 403 servername
 Redirectmatch 403 &rptmode=
 Redirectmatch 403 sys_cpanel
 RedirectMatch 403 db_connect
 RedirectMatch 403 doeditconfig
 RedirectMatch 403 check_proxy
 Redirectmatch 403 system_user
 Redirectmatch 403 /(null)/
 Redirectmatch 403 clientrequest
 Redirectmatch 403 option_value
 RedirectMatch 403 ref.outcontrol
 # SPECIFIC EXPLOITS
 RedirectMatch 403 errors.
 RedirectMatch 403 config.
 RedirectMatch 403 include.
 RedirectMatch 403 display.
 RedirectMatch 403 register.
 Redirectmatch 403 password.
 RedirectMatch 403 maincore.
 RedirectMatch 403 authorize.
 Redirectmatch 403 macromates.
 RedirectMatch 403 head_auth.
 RedirectMatch 403 submit_links.
 RedirectMatch 403 change_action.
 Redirectmatch 403 com_facileforms/
 RedirectMatch 403 admin_db_utilities.
 RedirectMatch 403 admin.webring.docs.
 Redirectmatch 403 Table/Latest/index.
</IfModule>] ";}i:2;i:10742;}i:202;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:12686;}i:203;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:12688;}i:204;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:21:"20. Install Wordfence";i:1;i:3;i:2;i:12688;}i:2;i:12688;}i:205;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:12688;}i:206;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:12688;}i:207;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:15:"Install this - ";}i:2;i:12720;}i:208;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:39:"http://wordpress.org/plugins/wordfence/";i:1;N;}i:2;i:12735;}i:209;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:12774;}i:210;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:12775;}i:211;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:12775;}}