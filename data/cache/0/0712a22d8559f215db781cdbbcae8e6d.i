a:54:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"Requirements";}i:2;i:1;}i:3;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:13;}i:4;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:13;}i:5;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:38:"A registered domain name with valid A ";}i:2;i:15;}i:6;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:3:"DNS";}i:2;i:53;}i:7;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:172:" records to point back to server public IP Address.
Nginx web server installed with SSL enabled and Virtual Hosts enabled (only for multiple domains or subdomains hosting).";}i:2;i:56;}i:8;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:228;}i:9;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:228;}i:10;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:152:"If Nginx is already running, stop the daemon with the following command and run ss utility to confirm that port 80 is no longer in use in network stack.";}i:2;i:230;}i:11;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:382;}i:12;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:382;}i:13;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"#Stop nginx
";}i:2;i:385;}i:14;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:18:"service nginx stop";}i:2;i:3;i:3;s:19:">service nginx stop";}i:2;i:402;}i:15;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:428;}i:16;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:428;}i:17;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:37:"#Check if port 80 is no longer used.
";}i:2;i:430;}i:18;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:7:"ss -tln";}i:2;i:3;i:3;s:8:">ss -tln";}i:2;i:472;}i:19;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:487;}i:20;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:487;}i:21;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:39:"Install git (If not already installed)
";}i:2;i:489;}i:22;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:15:"yum install git";}i:2;i:3;i:3;s:16:">yum install git";}i:2;i:533;}i:23;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:556;}i:24;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:556;}i:25;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:21:"#Clone lets encrypt.
";}i:2;i:558;}i:26;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:60:"cd /opt
git clone https://github.com/letsencrypt/letsencrypt";}i:2;i:3;i:3;s:61:">cd /opt
git clone https://github.com/letsencrypt/letsencrypt";}i:2;i:584;}i:27;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:652;}i:28;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:652;}i:29;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:35:"#Change directory to lets encrypt.
";}i:2;i:654;}i:30;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:99:"cd letsencrypt/ 

./letsencrypt-auto certonly --standalone -d your_domain.tld -d www.yourdomain.tld";}i:2;i:3;i:3;s:100:">cd letsencrypt/ 

./letsencrypt-auto certonly --standalone -d your_domain.tld -d www.yourdomain.tld";}i:2;i:694;}i:31;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:801;}i:32;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:801;}i:33;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:195:"Append Below Parameters in your virtualhost file(/etc/nginx/conf.d/defaults.conf) or in fastcgi_params file (/etc/nginx/fastcgi_params). Make sure to include them in your nginx virtual host file.";}i:2;i:804;}i:34;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:999;}i:35;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:999;}i:36;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:30:"include        fastcgi_params;";}i:2;i:3;i:3;s:31:">include        fastcgi_params;";}i:2;i:1006;}i:37;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1044;}i:38;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1044;}i:39;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:84:"
fastcgi_param   HTTPS               on;
fastcgi_param   HTTP_SCHEME         https;
";}i:2;i:3;i:3;s:85:">
fastcgi_param   HTTPS               on;
fastcgi_param   HTTP_SCHEME         https;
";}i:2;i:1051;}i:40;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1143;}i:41;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1143;}i:42;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:28:"Add SSL conf nginx excerpt:
";}i:2;i:1146;}i:43;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:322:"
# SSL configuration
listen 443 ssl default_server;
ssl_certificate /etc/letsencrypt/live/your_domain.tld/fullchain.pem;
ssl_certificate_key /etc/letsencrypt/live/your_domain.tld/privkey.pem;
ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
ssl_prefer_server_ciphers on;
ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';
";}i:2;i:3;i:3;s:323:">
# SSL configuration
listen 443 ssl default_server;
ssl_certificate /etc/letsencrypt/live/your_domain.tld/fullchain.pem;
ssl_certificate_key /etc/letsencrypt/live/your_domain.tld/privkey.pem;
ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
ssl_prefer_server_ciphers on;
ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';
";}i:2;i:1179;}i:44;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1509;}i:45;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1509;}i:46;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:14:"Restart nginx
";}i:2;i:1511;}i:47;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:23:"
service nginx restart
";}i:2;i:3;i:3;s:24:">
service nginx restart
";}i:2;i:1530;}i:48;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1561;}i:49;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1561;}i:50;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:97:"

if ($scheme = http) {
       return 301 https://$server_name$request_uri;
      }
      
      ";}i:2;i:3;i:3;s:98:">

if ($scheme = http) {
       return 301 https://$server_name$request_uri;
      }
      
      ";}i:2;i:1568;}i:51;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:0:"";}i:2;i:1673;}i:52;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1673;}i:53;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:1673;}}