a:35:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:38:"How To Install Policyd on Zimbra 8.6? ";}i:2;i:1;}i:3;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:39;}i:4;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:41;}i:5;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:41;}i:6;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:19:"# Activate Policyd
";}i:2;i:43;}i:7;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:102:"
su - zimbra
zmprov ms `zmhostname` +zimbraServiceInstalled cbpolicyd +zimbraServiceEnabled cbpolicyd
";}i:2;i:3;i:3;s:103:">
su - zimbra
zmprov ms `zmhostname` +zimbraServiceInstalled cbpolicyd +zimbraServiceEnabled cbpolicyd
";}i:2;i:67;}i:8;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:177;}i:9;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:177;}i:10;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:59:"# Activate Policyd WebUI
Run the following command as root
";}i:2;i:179;}i:11;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:67:"
cd /opt/zimbra/httpd/htdocs/ && ln -s ../../cbpolicyd/share/webui
";}i:2;i:3;i:3;s:68:">
cd /opt/zimbra/httpd/htdocs/ && ln -s ../../cbpolicyd/share/webui
";}i:2;i:243;}i:12;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:318;}i:13;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:318;}i:14;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:209:"Edit file /opt/zimbra/cbpolicyd/share/webui/includes/config.php and putting “#” on front of all the lines beginning with $DB_DSN and adding the following line just before the line beginning with $DB_USER.
";}i:2;i:320;}i:15;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:68:"
$DB_DSN="sqlite:/opt/zimbra/data/cbpolicyd/db/cbpolicyd.sqlitedb";
";}i:2;i:3;i:3;s:69:">
$DB_DSN="sqlite:/opt/zimbra/data/cbpolicyd/db/cbpolicyd.sqlitedb";
";}i:2;i:534;}i:16;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:610;}i:17;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:610;}i:18;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"Example:
";}i:2;i:612;}i:19;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:137:"
#$DB_DSN="mysql:host=localhost;dbname=cluebringer";
$DB_DSN="sqlite:/opt/zimbra/data/cbpolicyd/db/cbpolicyd.sqlitedb";
$DB_USER="root";
";}i:2;i:3;i:3;s:138:">
#$DB_DSN="mysql:host=localhost;dbname=cluebringer";
$DB_DSN="sqlite:/opt/zimbra/data/cbpolicyd/db/cbpolicyd.sqlitedb";
$DB_USER="root";
";}i:2;i:626;}i:20;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:771;}i:21;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:771;}i:22;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:51:"Restart Zimbra service  and Zimbra Apache service.
";}i:2;i:773;}i:23;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:73:"
su - zimbra -c "zmcontrol restart"
su - zimbra -c "zmapachectl restart"
";}i:2;i:3;i:3;s:74:">
su - zimbra -c "zmcontrol restart"
su - zimbra -c "zmapachectl restart"
";}i:2;i:829;}i:24;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:910;}i:25;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:910;}i:26;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:53:"You can now access the Policyd Webui with browser at ";}i:2;i:912;}i:27;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:3:"URL";}i:2;i:965;}i:28;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:968;}i:29;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:36:"http://IPZimbra:7780/webui/index.php";i:1;N;}i:2;i:969;}i:30;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1005;}i:31;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1005;}i:32;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:58:"Note: Make sure 7780 Port is open on your server firewall.";}i:2;i:1007;}i:33;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1007;}i:34;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:1007;}}