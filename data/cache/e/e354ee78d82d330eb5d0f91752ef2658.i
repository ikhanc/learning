a:52:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:26:"Setup SFTP for ftp servers";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1;}i:4;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:50:"1. Login as root and type the following commands:
";}i:2;i:43;}i:5;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:584:"
cd /usr/src
wget http://files.directadmin.com/services/customapache/proftpd-1.3.2.tar.gz
tar xvzf proftpd-1.3.2.tar.gz
cd proftpd-1.3.2
cd contrib
wget http://www.castaglia.org/proftpd/modules/proftpd-mod-sftp-0.9.6.tar.gz
tar xvfz proftpd-mod-sftp-0.9.6.tar.gz
cd ..

(remove the spaces between hyphens for the below commands)

install_user=ftp install_group=ftp ./configure \

- -prefix=/usr - -sysconfdir=/etc \

-localstatedir=/var/run - -mandir=/usr/man \

- -without-pam - -disable-auth-pam - -enable-openssl \

- -with-modules=mod_ratio:mod_readme:mod_sftp

make
make install ";}i:2;i:3;i:3;s:585:">
cd /usr/src
wget http://files.directadmin.com/services/customapache/proftpd-1.3.2.tar.gz
tar xvzf proftpd-1.3.2.tar.gz
cd proftpd-1.3.2
cd contrib
wget http://www.castaglia.org/proftpd/modules/proftpd-mod-sftp-0.9.6.tar.gz
tar xvfz proftpd-mod-sftp-0.9.6.tar.gz
cd ..

(remove the spaces between hyphens for the below commands)

install_user=ftp install_group=ftp ./configure \

- -prefix=/usr - -sysconfdir=/etc \

-localstatedir=/var/run - -mandir=/usr/man \

- -without-pam - -disable-auth-pam - -enable-openssl \

- -with-modules=mod_ratio:mod_readme:mod_sftp

make
make install ";}i:2;i:98;}i:6;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:690;}i:7;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:690;}i:8;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:"2.";}i:2;i:692;}i:9;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:43:" Restart Proftpd:

service proftpd restart ";}i:2;i:3;i:3;s:44:"> Restart Proftpd:

service proftpd restart ";}i:2;i:699;}i:10;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:750;}i:11;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:750;}i:12;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:43:"3. Make sure you can still connect to your ";}i:2;i:752;}i:13;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:3:"FTP";}i:2;i:795;}i:14;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:38:" server. You should see version 1.3.2:";}i:2;i:798;}i:15;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:836;}i:16;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:836;}i:17;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:15:"ftp example.com";}i:2;i:838;}i:18;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:853;}i:19;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:853;}i:20;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:36:"You should see something like this: ";}i:2;i:855;}i:21;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:891;}i:22;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:39:"
Connected to example.com (208.86.x.x).";}i:2;i:893;}i:23;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:932;}i:24;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:32:"
220 ProFTPD 1.3.2 Server ready.";}i:2;i:934;}i:25;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:966;}i:26;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:20:"
Name (example.com):";}i:2;i:968;}i:27;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:988;}i:28;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:988;}i:29;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:55:"4. Open /etc/proftpd.conf and change port number to 22.";}i:2;i:990;}i:30;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1045;}i:31;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1045;}i:32;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:"5.";}i:2;i:1047;}i:33;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:148:" Add the following lines to your /etc/proftpd.conf file:

SFTPEngine On
SFTPHostKey /etc/ssh/ssh_host_rsa_key
SFTPHostKey /etc/ssh/ssh_host_dsa_key ";}i:2;i:3;i:3;s:149:"> Add the following lines to your /etc/proftpd.conf file:

SFTPEngine On
SFTPHostKey /etc/ssh/ssh_host_rsa_key
SFTPHostKey /etc/ssh/ssh_host_dsa_key ";}i:2;i:1054;}i:34;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1210;}i:35;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1210;}i:36;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:"6.";}i:2;i:1212;}i:37;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:122:" COMMENT OUT the 'bytes' log in /etc/proftpd.conf as well:

ExtendedLog /var/log/proftpd/1.2.3.4.bytes WRITE,READ userlog ";}i:2;i:3;i:3;s:123:"> COMMENT OUT the 'bytes' log in /etc/proftpd.conf as well:

ExtendedLog /var/log/proftpd/1.2.3.4.bytes WRITE,READ userlog ";}i:2;i:1219;}i:38;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:130:"
7. Open up /usr/local/directadmin/data/templates/custom/proftpd.vhosts.conf and add the following lines which are missing there:
";}i:2;i:1349;}i:39;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:186:"
<VirtualHost |ip|>
ServerName "ServerName"
AuthUserFile AuthUserFile

SFTPEngine On
Port 22

SFTPHostKey /etc/ssh/ssh_host_rsa_key
SFTPHostKey /etc/ssh/ssh_host_dsa_key

</VirtualHost> ";}i:2;i:3;i:3;s:187:">
<VirtualHost |ip|>
ServerName "ServerName"
AuthUserFile AuthUserFile

SFTPEngine On
Port 22

SFTPHostKey /etc/ssh/ssh_host_rsa_key
SFTPHostKey /etc/ssh/ssh_host_dsa_key

</VirtualHost> ";}i:2;i:1484;}i:40;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1678;}i:41;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1678;}i:42;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:"8.";}i:2;i:1680;}i:43;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:43:" Restart proftpd:

service proftpd restart ";}i:2;i:3;i:3;s:44:"> Restart proftpd:

service proftpd restart ";}i:2;i:1687;}i:44;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1738;}i:45;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1738;}i:46;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:83:"9. Test it out! If you telnet to the new port you should see a greeting like this:
";}i:2;i:1740;}i:47;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:236:"
telnet example.com 22
Trying 208.86.x.x...
Connected to example.com.
Escape character is '^]'.
SS´['Å0-mod_sftp/0.9.6
Lß ç_ªC.ÃÇdiffie-hellman-group-exchange-sha256,diffie-hellman-group-exchange-sha1,diffie-hellman-group14- ... ";}i:2;i:3;i:3;s:237:">
telnet example.com 22
Trying 208.86.x.x...
Connected to example.com.
Escape character is '^]'.
SS´['Å0-mod_sftp/0.9.6
Lß ç_ªC.ÃÇdiffie-hellman-group-exchange-sha256,diffie-hellman-group-exchange-sha1,diffie-hellman-group14- ... ";}i:2;i:1828;}i:48;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:0:"";}i:2;i:2072;}i:49;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2072;}i:50;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:2072;}i:51;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:2072;}}