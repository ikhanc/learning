a:45:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:102:"- Set up Permalinks in wordpress. Under settings, click Permalinks and select the format you’d like.";}i:2;i:1;}i:3;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:103;}i:4;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:44:"    e.g. /%year%/%monthnum%/%postname%/
    ";}i:2;i:103;}i:5;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:103;}i:6;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:21:"- Click Save Changes.";}i:2;i:153;}i:7;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:174;}i:8;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:174;}i:9;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:32:"- This works well, however, the ";}i:2;i:176;}i:10;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:3:"URL";}i:2;i:208;}i:11;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:94:" isn’t so pretty. If you notice, all the URLs have index.php appended to them–which sucks.";}i:2;i:211;}i:12;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:305;}i:13;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:305;}i:14;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:20:"- This is where the ";}i:2;i:307;}i:15;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:3:"URL";}i:2;i:327;}i:16;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:106:"_Rewrite mod comes in. Once you have the mod installed, create a web.config file with the following rules.";}i:2;i:330;}i:17;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:436;}i:18;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:836:"
  <?xml version="1.0" encoding="UTF-8"?>
    <configuration>
     <system.webServer>
      <defaultDocument>
        <!-- Set the default document -->
         <files>
          <remove value="index.php" />
          <add value="index.php" />
         </files>
      </defaultDocument>
      <httpErrors errorMode="Detailed"/>
   <rewrite>
      <rules>
          <rule name="wordpress" patternSyntax="Wildcard">
              <match url="*" />
                  <conditions>
                      <add input="{REQUEST_FILENAME}" matchType="IsFile" negate="true" />
                      <add input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="true" />
                  </conditions>
              <action type="Rewrite" url="index.php" />
          </rule>
      </rules>
  </rewrite>
  </system.webServer>
  </configuration>";}i:2;i:436;}i:19;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:436;}i:20;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:155:"Note: Make sure there’s no white space at the beginning or end of the file. It’ll spit out errors.
The first part of the config file..Remove Index.php:";}i:2;i:1327;}i:21;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1482;}i:22;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:170:"
  <defaultDocument>
   <!-- Set the default document -->
    <files>
      <remove value="index.php" />
      <add value="index.php" />
    </files>
  </defaultDocument>";}i:2;i:1482;}i:23;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1482;}i:24;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:101:"This pretty much tells the application to function without the awful “index.php” appended in the ";}i:2;i:1671;}i:25;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:3:"URL";}i:2;i:1772;}i:26;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:39:".
The second part let’s troubleshoot:";}i:2;i:1775;}i:27;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1814;}i:28;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1814;}i:29;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:23:" <httpErrors errorMode=";}i:2;i:1818;}i:30;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:1841;}i:31;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:8:"Detailed";}i:2;i:1842;}i:32;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:1850;}i:33;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:"/>";}i:2;i:1851;}i:34;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1853;}i:35;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1853;}i:36;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:189:"Tells the app to show us exactly whats going on when an error happens, you can leave this out. I use it for troubleshooting purposes.
And now the WordPress Rule, this lets you use whatever ";}i:2;i:1855;}i:37;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:3:"URL";}i:2;i:2044;}i:38;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:24:" structure you’d like.";}i:2;i:2047;}i:39;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2071;}i:40;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:467:"
  <rewrite>
      <rules>
          <rule name="wordpress" patternSyntax="Wildcard">
              <match url="*" />
                  <conditions>
                      <add input="{REQUEST_FILENAME}" matchType="IsFile" negate="true" />
                      <add input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="true" />
                  </conditions>
              <action type="Rewrite" url="index.php" />
          </rule>
      </rules>
  </rewrite>";}i:2;i:2071;}i:41;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2071;}i:42;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:156:"Place this web.config file on the root of your wordpress installation and it should function the same as an .htaccess file in Apache.
And there you have it.";}i:2;i:2567;}i:43;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2723;}i:44;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:2723;}}