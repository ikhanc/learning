a:24:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:20:"Xen setup on Hetzner";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1;}i:4;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:19:"Edit the eth0 file:";}i:2;i:39;}i:5;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:58;}i:6;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:58;}i:7;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:44:"vi /etc/sysconfig/network-scripts/ifcfg-eth0";}i:2;i:60;}i:8;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:104;}i:9;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:104;}i:10;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:27:"Make the following changes:";}i:2;i:106;}i:11;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:133;}i:12;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:133;}i:13;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:57:"1) Comment out the broadcast setting - #BROADCAST=X.X.X.X";}i:2;i:135;}i:14;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:192;}i:15;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:192;}i:16;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:36:"2) Change netmask to 255.255.255.255";}i:2;i:194;}i:17;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:230;}i:18;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:230;}i:19;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:1280:"
Now, edit the file: /etc/sysctl.conf

net.ipv4.ip_forward=1
net.ipv4.conf.all.rp_filter=1
net.ipv4.icmp_echo_ignore_broadcasts=1
net.ipv4.conf.default.proxy_arp=1

 

Edit file: /etc/xen/scripts/vif-common.sh:

Make the following changes:

Comment out the original line like so:
# ip addr show "$1" | awk "/^.*inet.*$1$/{print $2}" | sed -n '1 s,/.*,,p'
And insert the following line below the commented out one:
ip -4 -o addr show primary dev $1 | awk '$3 == "inet" {print $4; exit}' | sed 's#/.*##'

 

Edit file: /etc/xen/xend-config.sxp

Make the following changes:

Comment out:

#(network-script network-bridge)
#(vif-script vif-bridge)

And add:

(network-script 'network-route netdev=eth0')
(vif-script vif-route)

Reboot the Dom0(Main machine)

 

Then create the VM and make following changes:

CentOS:

Add this to ifcfg-eth0 file:

SCOPE="peer <IP Address of Host>"

And create a new file "route-eth0" in /etc/sysconfig/network-scripts/ with following:

ADDRESS0=0.0.0.0

NETMASK0=0.0.0.0

GATEWAY0=<IP Address of Host>

 

Ubuntu:

Add this to /etc/network/interfaces:

pointopoint <IP Address of host>

 

Lock the ifcfg-eth0 file (in DomU):

chattr +aui /etc/sysconfig/network-scripts/ifcfg-eth0

 

That's it! Now you can ping any external network from your VM!! ";}i:2;i:3;i:3;s:1281:">
Now, edit the file: /etc/sysctl.conf

net.ipv4.ip_forward=1
net.ipv4.conf.all.rp_filter=1
net.ipv4.icmp_echo_ignore_broadcasts=1
net.ipv4.conf.default.proxy_arp=1

 

Edit file: /etc/xen/scripts/vif-common.sh:

Make the following changes:

Comment out the original line like so:
# ip addr show "$1" | awk "/^.*inet.*$1$/{print $2}" | sed -n '1 s,/.*,,p'
And insert the following line below the commented out one:
ip -4 -o addr show primary dev $1 | awk '$3 == "inet" {print $4; exit}' | sed 's#/.*##'

 

Edit file: /etc/xen/xend-config.sxp

Make the following changes:

Comment out:

#(network-script network-bridge)
#(vif-script vif-bridge)

And add:

(network-script 'network-route netdev=eth0')
(vif-script vif-route)

Reboot the Dom0(Main machine)

 

Then create the VM and make following changes:

CentOS:

Add this to ifcfg-eth0 file:

SCOPE="peer <IP Address of Host>"

And create a new file "route-eth0" in /etc/sysconfig/network-scripts/ with following:

ADDRESS0=0.0.0.0

NETMASK0=0.0.0.0

GATEWAY0=<IP Address of Host>

 

Ubuntu:

Add this to /etc/network/interfaces:

pointopoint <IP Address of host>

 

Lock the ifcfg-eth0 file (in DomU):

chattr +aui /etc/sysconfig/network-scripts/ifcfg-eth0

 

That's it! Now you can ping any external network from your VM!! ";}i:2;i:239;}i:20;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:0:"";}i:2;i:1527;}i:21;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1527;}i:22;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1527;}i:23;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:1527;}}