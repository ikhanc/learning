a:52:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:27:"1. Create new RAID-1 array
";}i:2;i:1;}i:3;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:81:"mdadm --create --verbose /dev/md0 --level=1 --raid-devices=2 /dev/sda1 /dev/sdb1
";}i:2;i:3;i:3;s:82:">mdadm --create --verbose /dev/md0 --level=1 --raid-devices=2 /dev/sda1 /dev/sdb1
";}i:2;i:33;}i:4;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:122;}i:5;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:122;}i:6;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:21:"Create RAID-10 array
";}i:2;i:124;}i:7;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:101:"mdadm --create --verbose /dev/md0 --level=10 --raid-devices=4 /dev/sda1 /dev/sdb1 /dev/sdc1 /dev/sdd1";}i:2;i:3;i:3;s:102:">mdadm --create --verbose /dev/md0 --level=10 --raid-devices=4 /dev/sda1 /dev/sdb1 /dev/sdc1 /dev/sdd1";}i:2;i:150;}i:8;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:259;}i:9;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:259;}i:10;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:30:"2. Fail disk from RAID array:
";}i:2;i:261;}i:11;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:31:"mdadm --fail /dev/md1 /dev/sda1";}i:2;i:3;i:3;s:32:">mdadm --fail /dev/md1 /dev/sda1";}i:2;i:296;}i:12;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:335;}i:13;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:335;}i:14;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:32:"3. Remove disk from RAID array:
";}i:2;i:337;}i:15;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:33:"mdadm --remove /dev/md1 /dev/sda1";}i:2;i:3;i:3;s:34:">mdadm --remove /dev/md1 /dev/sda1";}i:2;i:374;}i:16;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:415;}i:17;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:415;}i:18;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:31:"4. Add disk to existing array:
";}i:2;i:417;}i:19;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:30:"mdadm --add /dev/md1 /dev/sdb1";}i:2;i:3;i:3;s:31:">mdadm --add /dev/md1 /dev/sdb1";}i:2;i:453;}i:20;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:491;}i:21;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:491;}i:22;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:32:"5. Check status of RAID arrays:
";}i:2;i:493;}i:23;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:17:"cat /proc/mdstat ";}i:2;i:3;i:3;s:18:">cat /proc/mdstat ";}i:2;i:530;}i:24;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:4:"
OR
";}i:2;i:555;}i:25;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:17:"mdadm -D /dev/md1";}i:2;i:3;i:3;s:18:">mdadm -D /dev/md1";}i:2;i:564;}i:26;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:589;}i:27;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:589;}i:28;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:22:"6. Stop a RAID array:
";}i:2;i:591;}i:29;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:21:"mdadm --stop /dev/md1";}i:2;i:3;i:3;s:22:">mdadm --stop /dev/md1";}i:2;i:618;}i:30;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:647;}i:31;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:647;}i:32;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:24:"7. Remove a RAID array:
";}i:2;i:649;}i:33;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:23:"mdadm --remove /dev/md1";}i:2;i:3;i:3;s:24:">mdadm --remove /dev/md1";}i:2;i:678;}i:34;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:709;}i:35;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:709;}i:36;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:53:"8. Create same partition on /dev/sdb as on /dev/sda:
";}i:2;i:711;}i:37;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:36:"sfdisk -d /dev/sda | sfdisk /dev/sdb";}i:2;i:3;i:3;s:37:">sfdisk -d /dev/sda | sfdisk /dev/sdb";}i:2;i:769;}i:38;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:813;}i:39;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:813;}i:40;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:46:"9. Copy bootloader from /dev/sda to /dev/sdb:
";}i:2;i:815;}i:41;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:135:"# grub
grub> find /grub/stage1
grub> device (hd0) /dev/sdb (or /dev/hdb for IDE drives)
grub> root (hd0,0)
grub> setup (hd0)
grub>quit
";}i:2;i:3;i:3;s:136:"># grub
grub> find /grub/stage1
grub> device (hd0) /dev/sdb (or /dev/hdb for IDE drives)
grub> root (hd0,0)
grub> setup (hd0)
grub>quit
";}i:2;i:866;}i:42;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1009;}i:43;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1009;}i:44;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:15:"References:
1. ";}i:2;i:1015;}i:45;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:29:"http://blog.ioflood.com/?p=26";i:1;N;}i:2;i:1030;}i:46;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"
2.";}i:2;i:1059;}i:47;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:96:"http://www.adaptec.com/nr/rdonlyres/14b2fd84-f7a0-4ac5-a07a-214123ea3dd6/0/4423_sw_hwraid_10.pdf";i:1;N;}i:2;i:1062;}i:48;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:4:"
3. ";}i:2;i:1158;}i:49;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:25:"http://tansi.info/hybrid/";i:1;N;}i:2;i:1162;}i:50;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1187;}i:51;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:1187;}}