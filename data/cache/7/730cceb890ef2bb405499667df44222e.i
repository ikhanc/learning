a:98:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:236:"This guide assumes that you've set InnoDB as the default engine in my.cnf of master DB and the databases you are going to replicate use the same engine, to verify this open MySQL monitor as root and run the following commands at master:";}i:2;i:1;}i:3;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:237;}i:4;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:237;}i:5;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:239;}i:6;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:14:" show engines;";}i:2;i:241;}i:7;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:255;}i:8;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:257;}i:9;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:257;}i:10;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:18:"This should give;
";}i:2;i:259;}i:11;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:1090:"
+------------+---------+------------------------------------------------------------+--------------+------+------------+
| Engine     | Support | Comment                                                    | Transactions | XA   | Savepoints |
+------------+---------+------------------------------------------------------------+--------------+------+------------+
| MRG_MYISAM | YES     | Collection of identical MyISAM tables                      | NO           | NO   | NO         |
| CSV        | YES     | CSV storage engine                                         | NO           | NO   | NO         |
| MyISAM     | YES     | Default engine as of MySQL 3.23 with great performance     | NO           | NO   | NO         |
| InnoDB     | DEFAULT | Supports transactions, row-level locking, and foreign keys | YES          | YES  | YES        |
| MEMORY     | YES     | Hash based, stored in memory, useful for temporary tables  | NO           | NO   | NO         |
+------------+---------+------------------------------------------------------------+--------------+------+------------+
";}i:2;i:3;i:3;s:1091:">
+------------+---------+------------------------------------------------------------+--------------+------+------------+
| Engine     | Support | Comment                                                    | Transactions | XA   | Savepoints |
+------------+---------+------------------------------------------------------------+--------------+------+------------+
| MRG_MYISAM | YES     | Collection of identical MyISAM tables                      | NO           | NO   | NO         |
| CSV        | YES     | CSV storage engine                                         | NO           | NO   | NO         |
| MyISAM     | YES     | Default engine as of MySQL 3.23 with great performance     | NO           | NO   | NO         |
| InnoDB     | DEFAULT | Supports transactions, row-level locking, and foreign keys | YES          | YES  | YES        |
| MEMORY     | YES     | Hash based, stored in memory, useful for temporary tables  | NO           | NO   | NO         |
+------------+---------+------------------------------------------------------------+--------------+------+------------+
";}i:2;i:282;}i:12;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1380;}i:13;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1380;}i:14;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:26:"Alternatively you can run:";}i:2;i:1382;}i:15;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1408;}i:16;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1408;}i:17;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:1410;}i:18;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:74:" SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE engine = 'InnoDB';";}i:2;i:1412;}i:19;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:1486;}i:20;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1488;}i:21;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1488;}i:22;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:362:"For replication what we'll be doing basically is, enabling bin-logging and taking a non-locking (read-lock) dump with the binlog position included. In effect, you’re creating a copy of the db marked with a timestamp, which allows the slave to catch up once you’ve migrated the data over. This seems like the best way to set up a MySQL slave with no downtime,";}i:2;i:1491;}i:23;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1853;}i:24;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1853;}i:25;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:90:"Now to start replication open my.cnf at master and add the following lines below [mysqld]
";}i:2;i:1855;}i:26;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:121:"
server-id=1
binlog-format=mixed
log-bin=mysql-bin
datadir=/var/lib/mysql
innodb_flush_log_at_trx_commit=1
sync_binlog=1
";}i:2;i:3;i:3;s:122:">
server-id=1
binlog-format=mixed
log-bin=mysql-bin
datadir=/var/lib/mysql
innodb_flush_log_at_trx_commit=1
sync_binlog=1
";}i:2;i:1950;}i:27;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:139:"
Save the file and restart the master mysql server and create a replication user that your slave server will use to connect to the master:
";}i:2;i:2079;}i:28;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:149:"
mysql -u root -p

CREATE USER replicant@slave-server-ip;
GRANT REPLICATION SLAVE ON *.* TO replicant@slave-server-ip IDENTIFIED BY 'password';
exit
";}i:2;i:3;i:3;s:150:">
mysql -u root -p

CREATE USER replicant@slave-server-ip;
GRANT REPLICATION SLAVE ON *.* TO replicant@slave-server-ip IDENTIFIED BY 'password';
exit
";}i:2;i:2223;}i:29;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:171:"
In the above statement we have only allowed a single IP (of slave) to connect to our master, in case you want to have multiple slave replication units, use % in place of ";}i:2;i:2380;}i:30;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:2551;}i:31;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:15:"slave-server-ip";}i:2;i:2552;}i:32;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:2567;}i:33;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:".";}i:2;i:2568;}i:34;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2569;}i:35;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2569;}i:36;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:68:"Next, create a mysql dump file with the binlog position in your pwd.";}i:2;i:2571;}i:37;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2639;}i:38;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2639;}i:39;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:2641;}i:40;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:22:" mysqldump -u root -p ";}i:2;i:2643;}i:41;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"--";}i:2;i:2665;}i:42;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:17:"skip-lock-tables ";}i:2;i:2667;}i:43;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"--";}i:2;i:2684;}i:44;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:19:"single-transaction ";}i:2;i:2686;}i:45;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"--";}i:2;i:2705;}i:46;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:11:"flush-logs ";}i:2;i:2707;}i:47;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"--";}i:2;i:2718;}i:48;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"hex-blob ";}i:2;i:2720;}i:49;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"--";}i:2;i:2729;}i:50;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:30:"master-data=2 -A  > ~/dump.sql";}i:2;i:2731;}i:51;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:2761;}i:52;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2763;}i:53;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2763;}i:54;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:92:"Examine the head of the file and note down the values for MASTER_LOG_FILE and MASTER_LOG_POS";}i:2;i:2765;}i:55;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2857;}i:56;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2857;}i:57;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:2859;}i:58;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:27:" head dump.sql -n80 | grep ";}i:2;i:2861;}i:59;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:2888;}i:60;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:14:"MASTER_LOG_POS";}i:2;i:2889;}i:61;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:2903;}i:62;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:2904;}i:63;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2906;}i:64;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2906;}i:65;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:147:"Transfer the dump.sql file to the slave server using scp.
Also open port 3306 on the master server, this port will be used by the slave to connect.";}i:2;i:2908;}i:66;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3055;}i:67;a:3:{i:0;s:2:"hr";i:1;a:0:{}i:2;i:3055;}i:68;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3055;}i:69;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:11:"Slave setup";}i:2;i:3153;}i:70;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3164;}i:71;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3164;}i:72;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:61:"Open my.cnf file and add the following lines below [mysqld]:
";}i:2;i:3166;}i:73;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:109:"
server-id=2
binlog-format=mixed
log_bin=mysql-bin
relay-log=mysql-relay-bin
log-slave-updates=1
read-only=1
";}i:2;i:3;i:3;s:110:">
server-id=2
binlog-format=mixed
log_bin=mysql-bin
relay-log=mysql-relay-bin
log-slave-updates=1
read-only=1
";}i:2;i:3232;}i:74;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3349;}i:75;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3349;}i:76;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:61:"Restart the MySQL service at slave, and import the dump file:";}i:2;i:3351;}i:77;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3412;}i:78;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3412;}i:79;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:3414;}i:80;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:37:" mysql -u root -p < /path/to/dump.sql";}i:2;i:3416;}i:81;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:3453;}i:82;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3455;}i:83;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3455;}i:84;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:71:"Now log into MySQL console and run the following to start replication:
";}i:2;i:3457;}i:85;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:205:"
CHANGE MASTER TO MASTER_HOST='master-server-ip',MASTER_USER='replicant',MASTER_PASSWORD='slave-user-password', MASTER_LOG_FILE='value we copied above', MASTER_LOG_POS=value we copied above;

START SLAVE;
";}i:2;i:3;i:3;s:206:">
CHANGE MASTER TO MASTER_HOST='master-server-ip',MASTER_USER='replicant',MASTER_PASSWORD='slave-user-password', MASTER_LOG_FILE='value we copied above', MASTER_LOG_POS=value we copied above;

START SLAVE;
";}i:2;i:3533;}i:86;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3746;}i:87;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3746;}i:88;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:90:"To verify that the replication process is error free, run the following in MySQL console:
";}i:2;i:3748;}i:89;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:3838;}i:90;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:20:"SHOW SLAVE STATUS \G";}i:2;i:3840;}i:91;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:3860;}i:92;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3862;}i:93;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3862;}i:94;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:36:"This should give you something like
";}i:2;i:3864;}i:95;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:512:"
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 180.149.242.98
                  Master_User: replicant
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000004
          Read_Master_Log_Pos: 524
               Relay_Log_File: mysql-relay-bin.000025
                Relay_Log_Pos: 251
        Relay_Master_Log_File: mysql-bin.000004
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
";}i:2;i:3;i:3;s:513:">
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 180.149.242.98
                  Master_User: replicant
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000004
          Read_Master_Log_Pos: 524
               Relay_Log_File: mysql-relay-bin.000025
                Relay_Log_Pos: 251
        Relay_Master_Log_File: mysql-bin.000004
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
";}i:2;i:3905;}i:96;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4425;}i:97;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:4425;}}