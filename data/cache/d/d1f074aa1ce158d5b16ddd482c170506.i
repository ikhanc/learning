a:61:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:33:"Setup OVH node with Xen (SolusVM)";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1;}i:4;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:45:"1) Partition the node with following config:
";}i:2;i:52;}i:5;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:78:"
/ - 50GB

/boot - 20GB

swap - 16GB

Rest space is to be left unpartitioned.
";}i:2;i:3;i:3;s:79:">
/ - 50GB

/boot - 20GB

swap - 16GB

Rest space is to be left unpartitioned.
";}i:2;i:102;}i:6;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:188;}i:7;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:188;}i:8;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:67:"2) Login to node and edit the file /etc/yum.conf. Comment the line ";}i:2;i:192;}i:9;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:259;}i:10;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:15:"exclude kernel*";}i:2;i:260;}i:11;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:275;}i:12;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:".";}i:2;i:276;}i:13;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:277;}i:14;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:277;}i:15;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:20:"3) Install SolusVM:
";}i:2;i:282;}i:16;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:110:"
wget http://soluslabs.com/installers/solusvm/install
chmod 755 install
./install
Select Options accordingly.
";}i:2;i:3;i:3;s:111:">
wget http://soluslabs.com/installers/solusvm/install
chmod 755 install
./install
Select Options accordingly.
";}i:2;i:307;}i:17;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:425;}i:18;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:425;}i:19;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:102:"4) Edit the file /etc/grub.conf and select the Xen kernel. Also, add the following to the kernel line:";}i:2;i:429;}i:20;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:531;}i:21;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:531;}i:22;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:533;}i:23;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:13:"dom0_mem=512M";}i:2;i:534;}i:24;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:547;}i:25;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:548;}i:26;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:548;}i:27;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:21:"5) Reboot the server.";}i:2;i:553;}i:28;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:574;}i:29;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:574;}i:30;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:65:"6) Run the command: php /usr/local/solusvm/includes/xenkernel.php";}i:2;i:579;}i:31;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:644;}i:32;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:644;}i:33;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:55:"7) Check the kernel: uname -r (It should be Xen kernel)";}i:2;i:649;}i:34;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:704;}i:35;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:704;}i:36;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:47:"8) Create a new partition with remaining space:";}i:2;i:709;}i:37;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:756;}i:38;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:756;}i:39;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:20:"fdisk /dev/<sda/sdb>";}i:2;i:758;}i:40;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:778;}i:41;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:778;}i:42;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:28:"(Select options accordingly)";}i:2;i:780;}i:43;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:808;}i:44;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:808;}i:45;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:52:"9) Create Physical Volume: pvcreate /dev/<sda#/sdb#>";}i:2;i:813;}i:46;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:865;}i:47;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:865;}i:48;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:61:"10) Create Volume Group: vgcreate VolGroup00 /dev/<sda#/sdb#>";}i:2;i:870;}i:49;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:931;}i:50;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:931;}i:51;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:39:"11) lvcreate -L +1GB -n LVM1 VolGroup00";}i:2;i:936;}i:52;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:975;}i:53;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:975;}i:54;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:34:"12) mkfs.ext3 /dev/VolGroup00/LVM1";}i:2;i:980;}i:55;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1014;}i:56;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1014;}i:57;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:76:"13) Login to SOlusVM Master, add the node, add IPs and do template+ISO sync.";}i:2;i:1019;}i:58;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1095;}i:59;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1096;}i:60;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:1096;}}