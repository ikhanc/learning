a:120:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:82:"Mysql Master/Master Replication & File sync for quick easy scaling or load balance";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:100;}i:4;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:17:"MySQL Replication";i:1;i:3;i:2;i:100;}i:2;i:100;}i:5;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:100;}i:6;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:100;}i:7;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:253:"First you need to edit your my.cnf , this will be located in /etc/ or /etc/mysql/ . Add in the following items onto both servers
master-host = 192.168.16.4
master-user = replication
master-password = slave321
master-port = 3306
master-connect-retry = 60";}i:2;i:130;}i:8;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:383;}i:9;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:383;}i:10;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:239:"Make sure you change the IP to the IP of the OTHER server, so server 1 has server 2 IP and vice versa, it just needs to know what to connect too and how. Check that you replace the 'replication' with user of choice, and passwords of choice";}i:2;i:385;}i:11;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:624;}i:12;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:624;}i:13;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:165:"Next you need to put the following into one server to give it an id
server-id = 1
replicate-same-server-id = 0
auto-increment-increment = 2
auto-increment-offset = 1";}i:2;i:626;}i:14;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:791;}i:15;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:791;}i:16;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:121:"And this into the other
server-id = 2
replicate-same-server-id = 0
auto-increment-increment = 2
auto-increment-offset = 2";}i:2;i:793;}i:17;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:914;}i:18;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:914;}i:19;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:257:"This identifies the servers with an id, and also sets auto_increment to be offset so you do not get clashes with trying to overwrite any data.
If you only want to replicate the one database you can add a line like this on both
replicate-do-db = databasename";}i:2;i:916;}i:20;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1173;}i:21;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1173;}i:22;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:227:"If you want to sync the entire database then you leave that out. If you want to ignore syncing for just one database then do something like this
binlog-ignore-db=mysql # input the database that should be ignored for replication";}i:2;i:1175;}i:23;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1402;}i:24;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1402;}i:25;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:133:"Okay, next you need to check the binlog line is uncommented if it is commented out, or add one
log_bin = /var/log/mysql/mysql-bin.log";}i:2;i:1404;}i:26;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1537;}i:27;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1537;}i:28;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:115:"Remove any instances of skip-networking and if you have bind-address make sure it looks like
bind-address = 0.0.0.0";}i:2;i:1539;}i:29;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1654;}i:30;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1654;}i:31;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:601:"Save and exit the mysql config now, and login to mysql on the command line. You will need to add the user you just setup on those configs that will be used for replication. Run this on both mysql servers, changing the IP/user/pass for the ones in your configs.
grant replication slave on *.* to 'replication'@192.168.16.5 identified by 'slave';
Make sure the user for each server has the hostname of the other server they are getting connections from on both of those.
Now the hard work is done, restart mysql on both servers. Now all you need to do is login via command line and start each as a slave";}i:2;i:1656;}i:32;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2257;}i:33;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2257;}i:34;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:47:"mysql> start slave;
mysql> show slave status\G;";}i:2;i:2259;}i:35;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2306;}i:36;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2306;}i:37;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:2308;}i:38;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:2310;}i:39;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:2312;}i:40;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:2314;}i:41;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:2316;}i:42;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:2318;}i:43;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:2320;}i:44;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:2322;}i:45;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:2324;}i:46;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:2326;}i:47;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:2328;}i:48;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:2330;}i:49;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:2332;}i:50;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"* 1. row ";}i:2;i:2334;}i:51;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:2343;}i:52;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:2345;}i:53;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:2347;}i:54;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:2349;}i:55;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:2351;}i:56;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:2353;}i:57;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:2355;}i:58;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:2357;}i:59;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:2359;}i:60;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:2361;}i:61;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:2363;}i:62;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:2365;}i:63;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:2367;}i:64;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:"*";}i:2;i:2369;}i:65;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2370;}i:66;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2370;}i:67;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:804:"Slave_IO_State: Waiting for master to send event
Master_Host: 192.168.16.4
Master_User: replica
Master_Port: 3306
Connect_Retry: 60
Master_Log_File: MASTERMYSQL01-bin.000009
Read_Master_Log_Pos: 4
Relay_Log_File: MASTERMYSQL02-relay-bin.000015
Relay_Log_Pos: 3630
Relay_Master_Log_File: MASTERMYSQL01-bin.000009
Slave_IO_Running: Yes
Slave_SQL_Running: Yes
Replicate_Do_DB:
Replicate_Ignore_DB:
Replicate_Do_Table:
Replicate_Ignore_Table:
Replicate_Wild_Do_Table:
Replicate_Wild_Ignore_Table:
Last_Errno: 0
Last_Error:
Skip_Counter: 0
Exec_Master_Log_Pos: 4
Relay_Log_Space: 3630
Until_Condition: None
Until_Log_File:
Until_Log_Pos: 0
Master_SSL_Allowed: No
Master_SSL_CA_File:
Master_SSL_CA_Path:
Master_SSL_Cert:
Master_SSL_Cipher:
Master_SSL_Key:
Seconds_Behind_Master: 1519187
1 row in set (0.00 sec)";}i:2;i:2372;}i:68;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3176;}i:69;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3176;}i:70;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:116:"If there are any errors they should show up here, and if you get stuck at all let us know, we are happy to help out!";}i:2;i:3178;}i:71;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3294;}i:72;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:3296;}i:73;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:16:"File Replication";i:1;i:3;i:2;i:3296;}i:2;i:3296;}i:74;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:3296;}i:75;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3296;}i:76;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:221:"For file replication we are going to use Unison, its easy to install and use . Make sure you have passwordless ssh setup on both servers so they can connect to each other without passwords or prompts (preferably as root).";}i:2;i:3325;}i:77;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3546;}i:78;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3546;}i:79;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:328:"Now create a directory in /root/.unison/
This is where you will store your configs. You can have multiple configs and include them from each other if needed, but in this tutorial we will be only using the one for now called default.prf
Create a file called /root/.unison/default.prf and insert something similar to the following";}i:2;i:3548;}i:80;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3876;}i:81;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3876;}i:82;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:40:"log = true
logfile = /var/log/unison.log";}i:2;i:3878;}i:83;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3918;}i:84;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3918;}i:85;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:58:"# Roots of the synchronization
root = /var/www
root = ssh:";}i:2;i:3920;}i:86;a:3:{i:0;s:13:"emphasis_open";i:1;a:0:{}i:2;i:3978;}i:87;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:16:"192.168.1.5:1022";}i:2;i:3980;}i:88;a:3:{i:0;s:14:"emphasis_close";i:1;a:0:{}i:2;i:3996;}i:89;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:80:"var/www # this is the IP of the remote server, SSH is on port 1022 as an example";}i:2;i:3998;}i:90;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4078;}i:91;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4078;}i:92;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:175:"# things you want to ignore - check manual if you want to do this
ignore = Name *.tmp ## ignores all files with the extension .tmp
#ignore = Name *cache*
ignore = Path .unison";}i:2;i:4080;}i:93;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4255;}i:94;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4255;}i:95;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:101:"auto=true
batch=true
confirmbigdel=true
fastcheck=true
group=true
owner=true
prefer=newer
silent=true";}i:2;i:4257;}i:96;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4358;}i:97;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4358;}i:98;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:713:"Now you should be able to run 'unison' from the command line and see any output, make sure things are ok. Pays to test a few times, I had errors regarding tmp files which was why i ignored those. No need to sync a cache for a website either if that is what you are syncing.
Despite this running on one server, it will sync both ways, so this only needs to be run on the one server not both (unless you have multiple servers).
When you have the unison as you want it to run, add that into a crontab and have it run every minute or two.
I have mine run only every 10 minutes or so, but you can have it running every minute if you need, or even have it triggered by another method if needed.
Here is my crontab entry";}i:2;i:4360;}i:99;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5073;}i:100;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5073;}i:101;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:45:"*/10 * * * * /usr/bin/unison > /dev/null 2>&1";}i:2;i:5075;}i:102;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5120;}i:103;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5120;}i:104;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:91:"Pays to create and delete files to test this on both sides to make sure it is working also.";}i:2;i:5122;}i:105;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5213;}i:106;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5213;}i:107;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:74:"Now you have 2 servers replicating each other. This means you can now add ";}i:2;i:5215;}i:108;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:3:"DNS";}i:2;i:5289;}i:109;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:17:" so that doing a ";}i:2;i:5292;}i:110;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:3:"DNS";}i:2;i:5309;}i:111;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:281:" lookup on a website shows 2 IPs, this means people will be spread over the 2 servers instead of the one, lowering the load on a single server. You can scale this up to multiple servers if required, and then take servers out of the array if they need work, without having downtime.";}i:2;i:5312;}i:112;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5593;}i:113;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5593;}i:114;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:541:"Things to note:
You may also want to sync configs (eg apache configs, websites, passwd files for users, etc).
Master/Master replication can be buggy - especially if you have very busy servers. There may be clashes on updates on both servers at the same time or things getting out of sync. This is why monitoring is critical to make sure it is going ok. For some sites it may be more suitable to have a single mysql on its own for some sites, and give it huge memory and CPU instead.
Other alternative ways for replication are described here ";}i:2;i:5595;}i:115;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:82:"http://dev.mysql.com/doc/refman/5.1/en/mysql-cluster-replication-multi-master.html";i:1;N;}i:2;i:6136;}i:116;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:133:"
As a general rule, there is no one fix or solution for scaling upwards, this post was just to get you started on some of the basics.";}i:2;i:6218;}i:117;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:6351;}i:118;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:6351;}i:119;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:6351;}}