a:5:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:6492:"
#!/bin/bash

su - zimbra -c 'zmcontrol status | grep cbpolicyd> /tmp/temp.mine; logout'
pstatus="$(cat /tmp/temp.mine | awk '{print $2}')"

if [ "$pstatus" = "Running" ]; then
        echo "Policyd is already installed and running."
    #echo "Exiting........"
    echo -n "Do you want to disable Policyd?{yes/no}: "
    read disablecheck
    case $disablecheck in
        yes )
            echo "Disabling cbpolicyd......"
            su - zimbra -c 'zmprov ms $HOSTNAME -zimbraServiceEnabled cbpolicyd; zmcontrol restart; logout'
            echo "renaming webui directory...."
            mv /opt/zimbra/cbpolicyd/share/webui /opt/zimbra/cbpolicyd/share/webui.old
            exit
            #echo "It will disable it."
        ;;
        no )
            echo "Policyd is not disabled. It is running."
            echo "Exiting......"
            exit
        ;;
        * )
            echo "Please select yes or no"
        ;;
    esac
    #exit
else

domain="test.com"             # Set the domain name
policyd_limit_emails=200                # How many emails to sent in the defined time
policyd_limit_time=3600                 # Time limit in seconds

for IPS in `ifconfig | awk '/inet addr/{print substr($2,6)}'`
do
        if [ "$IPS" != "127.0.0.1" ]; then
                ip=$IPS
        fi
done

dist=`grep DISTRIB_ID /etc/*-release | awk -F '=' '{print $2}'`

if [ "$dist" = "Ubuntu" ]; then
        os="ubuntu"
else
        dist=`cat /etc/*-release | head -1 | awk '{print $1}'`
        if [ "$dist" = "CentOS" ]; then
                os="centos"
        fi
fi

echo ""
echo "-------------------------------------- Policyd Installation start -------------------------------";
echo "Policyd is being installed with domain $domain, policyd_limit_emails $policyd_limit_emails and policyd_limit_time $policyd_limit_time"
echo "You can change these settings in script file. Kindly make sure your Hostname is properly configured. Your current Hostname is $HOSTNAME"
echo "Please make sure you are installing this as root."
echo -n  "Are these setting correct and you want to continue?[yes/no]: "
read confirmation
case $confirmation in
    yes ) echo "yes"

########################### Policyd Installation and Configuration #######################

    ## Activate Policyd ##
echo ""
echo "Activating Policyd......"

su - zimbra -c 'zmprov ms $HOSTNAME +zimbraServiceInstalled cbpolicyd +zimbraServiceEnabled cbpolicyd; logout'
#zmprov ms $HOSTNAME +zimbraServiceInstalled cbpolicyd +zimbraServiceEnabled cbpolicyd

    ## Activate Policyd WebUI Run the following command as root ##
echo ""
echo "Activating Poliyd WebUI........"

cd /opt/zimbra/httpd/htdocs/ && ln -s ../../cbpolicyd/share/webui
echo "Making backup of config file /opt/zimbra/cbpolicyd/share/webui/includes/config.php ............"
cp /opt/zimbra/cbpolicyd/share/webui/includes/config.php /opt/zimbra/cbpolicyd/share/webui/includes/config.php.bak
sed -i /DB_DSN/s/^/#/ /opt/zimbra/cbpolicyd/share/webui/includes/config.php
sed -i '/DB_USER/i $DB_DSN="sqlite:/opt/zimbra/data/cbpolicyd/db/cbpolicyd.sqlitedb"; ' /opt/zimbra/cbpolicyd/share/webui/includes/config.php
su - zimbra -c 'zmcontrol restart; zmapachectl restart; logout'

    ## Limit per user 500 email per hour ##
echo ""
echo "Setting Limits for email per hour per user........"

cat>/root/rate-limit.sql <<EOL
BEGIN TRANSACTION;
INSERT INTO policy_groups (Name,Comment,Disabled) VALUES ('list_domains','',0);
INSERT INTO policy_group_members (PolicyGroupID,Member,Comment,Disabled) VALUES (3,'@$domain','',0);
INSERT INTO policy_group_members (PolicyGroupID,Member,Comment,Disabled) VALUES (3,'@$HOSTNAME','',0);
INSERT INTO policies (Name,Priority,Description,Disabled) VALUES ('rate limit',20,'rate limit for user',0);
INSERT INTO policy_members (PolicyID,Source,Destination,Comment,Disabled) VALUES (6,'%list_domain','!%list_domain','',0);
INSERT INTO policy_members (PolicyID,Source,Destination,Comment,Disabled) VALUES (6,'!%list_domain','any','',0);
INSERT INTO quotas (PolicyID,Name,Track,Period,Verdict,Data,LastQuota,Comment,Disabled) VALUES (6,'Rate Limit','Sender:user@domain',$policyd_limit_time,'DEFER','Sorry, your quota to sending mail in this hour has been exceeded.',0,'',0);
INSERT INTO quotas_limits (QuotasID,Type,CounterLimit,Comment,Disabled) VALUES (3,'MessageCount',$policyd_limit_emails,'',0);
COMMIT;
EOL

sqlite3 /opt/zimbra/data/cbpolicyd/db/cbpolicyd.sqlitedb </root/rate-limit.sql
rm -vf /root/rate-limit.sql

########################### Protecting Policyd Web UI #######################################
echo ""
echo "Protecting Policyd WebUI with .htaccess........."

cd /opt/zimbra/cbpolicyd/share/webui/
cat>.htaccess <<EOL
AuthUserFile /opt/zimbra/cbpolicyd/share/webui/.htpasswd
AuthGroupFile /dev/null
AuthName "User and Password"
AuthType Basic

<LIMIT GET>
require valid-user
</LIMIT>
EOL

randpass=`</dev/urandom tr -dc _A-Z-a-z-0-9 | head -c8`
emailDetails=tech@mantraventures.com

touch .htpasswd
/opt/zimbra/httpd/bin/htpasswd -cb .htpasswd policyadmin $randpass
echo " ">> /opt/zimbra/conf/httpd.conf
echo "Alias /webui /opt/zimbra/cbpolicyd/share/webui/">> /opt/zimbra/conf/httpd.conf
echo "<Directory /opt/zimbra/cbpolicyd/share/webui/>">> /opt/zimbra/conf/httpd.conf
echo "# Comment out the following 3 lines to make web ui accessible from anywhere">> /opt/zimbra/conf/httpd.conf
echo "AllowOverride AuthConfig">> /opt/zimbra/conf/httpd.conf
echo "Order Deny,Allow">> /opt/zimbra/conf/httpd.conf
echo "Allow from all">> /opt/zimbra/conf/httpd.conf
echo "</Directory>">> /opt/zimbra/conf/httpd.conf

su - zimbra -c "zmapachectl restart; logout"

echo "Policyd is configured. You can login at http://$ip:7780/webui/index.php with login details policyadmin/$randpass"

if [ "$os" = "ubuntu" ]; then
        apt-get install mailutils -y
elif [ "$os" = "centos" ]; then
        yum install mailx -y
else
        echo ""
fi

echo -e "Policyd web ui login details \n\n URL: http://$ip:7780/webui/index.php \n Username: policyadmin \n Password: $randpass" | mail -s "Policyd installation login details for $HOSTNAME ($ip)" $emailDetails

echo "Email also sent with these details at $emailDetails"

if [ "$os" = "ubuntu" ]; then
        service postfix stop
elif [ "$os" = "centos" ]; then
        echo ""
else
        echo ""
fi

    ;;

        no )
        echo "You have cancelled the installation. Exiting...."
        exit
        ;;

        * )
        echo "Please enter yes or no."
        ;;
esac

fi
";}i:2;i:3;i:3;s:6493:">
#!/bin/bash

su - zimbra -c 'zmcontrol status | grep cbpolicyd> /tmp/temp.mine; logout'
pstatus="$(cat /tmp/temp.mine | awk '{print $2}')"

if [ "$pstatus" = "Running" ]; then
        echo "Policyd is already installed and running."
    #echo "Exiting........"
    echo -n "Do you want to disable Policyd?{yes/no}: "
    read disablecheck
    case $disablecheck in
        yes )
            echo "Disabling cbpolicyd......"
            su - zimbra -c 'zmprov ms $HOSTNAME -zimbraServiceEnabled cbpolicyd; zmcontrol restart; logout'
            echo "renaming webui directory...."
            mv /opt/zimbra/cbpolicyd/share/webui /opt/zimbra/cbpolicyd/share/webui.old
            exit
            #echo "It will disable it."
        ;;
        no )
            echo "Policyd is not disabled. It is running."
            echo "Exiting......"
            exit
        ;;
        * )
            echo "Please select yes or no"
        ;;
    esac
    #exit
else

domain="test.com"             # Set the domain name
policyd_limit_emails=200                # How many emails to sent in the defined time
policyd_limit_time=3600                 # Time limit in seconds

for IPS in `ifconfig | awk '/inet addr/{print substr($2,6)}'`
do
        if [ "$IPS" != "127.0.0.1" ]; then
                ip=$IPS
        fi
done

dist=`grep DISTRIB_ID /etc/*-release | awk -F '=' '{print $2}'`

if [ "$dist" = "Ubuntu" ]; then
        os="ubuntu"
else
        dist=`cat /etc/*-release | head -1 | awk '{print $1}'`
        if [ "$dist" = "CentOS" ]; then
                os="centos"
        fi
fi

echo ""
echo "-------------------------------------- Policyd Installation start -------------------------------";
echo "Policyd is being installed with domain $domain, policyd_limit_emails $policyd_limit_emails and policyd_limit_time $policyd_limit_time"
echo "You can change these settings in script file. Kindly make sure your Hostname is properly configured. Your current Hostname is $HOSTNAME"
echo "Please make sure you are installing this as root."
echo -n  "Are these setting correct and you want to continue?[yes/no]: "
read confirmation
case $confirmation in
    yes ) echo "yes"

########################### Policyd Installation and Configuration #######################

    ## Activate Policyd ##
echo ""
echo "Activating Policyd......"

su - zimbra -c 'zmprov ms $HOSTNAME +zimbraServiceInstalled cbpolicyd +zimbraServiceEnabled cbpolicyd; logout'
#zmprov ms $HOSTNAME +zimbraServiceInstalled cbpolicyd +zimbraServiceEnabled cbpolicyd

    ## Activate Policyd WebUI Run the following command as root ##
echo ""
echo "Activating Poliyd WebUI........"

cd /opt/zimbra/httpd/htdocs/ && ln -s ../../cbpolicyd/share/webui
echo "Making backup of config file /opt/zimbra/cbpolicyd/share/webui/includes/config.php ............"
cp /opt/zimbra/cbpolicyd/share/webui/includes/config.php /opt/zimbra/cbpolicyd/share/webui/includes/config.php.bak
sed -i /DB_DSN/s/^/#/ /opt/zimbra/cbpolicyd/share/webui/includes/config.php
sed -i '/DB_USER/i $DB_DSN="sqlite:/opt/zimbra/data/cbpolicyd/db/cbpolicyd.sqlitedb"; ' /opt/zimbra/cbpolicyd/share/webui/includes/config.php
su - zimbra -c 'zmcontrol restart; zmapachectl restart; logout'

    ## Limit per user 500 email per hour ##
echo ""
echo "Setting Limits for email per hour per user........"

cat>/root/rate-limit.sql <<EOL
BEGIN TRANSACTION;
INSERT INTO policy_groups (Name,Comment,Disabled) VALUES ('list_domains','',0);
INSERT INTO policy_group_members (PolicyGroupID,Member,Comment,Disabled) VALUES (3,'@$domain','',0);
INSERT INTO policy_group_members (PolicyGroupID,Member,Comment,Disabled) VALUES (3,'@$HOSTNAME','',0);
INSERT INTO policies (Name,Priority,Description,Disabled) VALUES ('rate limit',20,'rate limit for user',0);
INSERT INTO policy_members (PolicyID,Source,Destination,Comment,Disabled) VALUES (6,'%list_domain','!%list_domain','',0);
INSERT INTO policy_members (PolicyID,Source,Destination,Comment,Disabled) VALUES (6,'!%list_domain','any','',0);
INSERT INTO quotas (PolicyID,Name,Track,Period,Verdict,Data,LastQuota,Comment,Disabled) VALUES (6,'Rate Limit','Sender:user@domain',$policyd_limit_time,'DEFER','Sorry, your quota to sending mail in this hour has been exceeded.',0,'',0);
INSERT INTO quotas_limits (QuotasID,Type,CounterLimit,Comment,Disabled) VALUES (3,'MessageCount',$policyd_limit_emails,'',0);
COMMIT;
EOL

sqlite3 /opt/zimbra/data/cbpolicyd/db/cbpolicyd.sqlitedb </root/rate-limit.sql
rm -vf /root/rate-limit.sql

########################### Protecting Policyd Web UI #######################################
echo ""
echo "Protecting Policyd WebUI with .htaccess........."

cd /opt/zimbra/cbpolicyd/share/webui/
cat>.htaccess <<EOL
AuthUserFile /opt/zimbra/cbpolicyd/share/webui/.htpasswd
AuthGroupFile /dev/null
AuthName "User and Password"
AuthType Basic

<LIMIT GET>
require valid-user
</LIMIT>
EOL

randpass=`</dev/urandom tr -dc _A-Z-a-z-0-9 | head -c8`
emailDetails=tech@mantraventures.com

touch .htpasswd
/opt/zimbra/httpd/bin/htpasswd -cb .htpasswd policyadmin $randpass
echo " ">> /opt/zimbra/conf/httpd.conf
echo "Alias /webui /opt/zimbra/cbpolicyd/share/webui/">> /opt/zimbra/conf/httpd.conf
echo "<Directory /opt/zimbra/cbpolicyd/share/webui/>">> /opt/zimbra/conf/httpd.conf
echo "# Comment out the following 3 lines to make web ui accessible from anywhere">> /opt/zimbra/conf/httpd.conf
echo "AllowOverride AuthConfig">> /opt/zimbra/conf/httpd.conf
echo "Order Deny,Allow">> /opt/zimbra/conf/httpd.conf
echo "Allow from all">> /opt/zimbra/conf/httpd.conf
echo "</Directory>">> /opt/zimbra/conf/httpd.conf

su - zimbra -c "zmapachectl restart; logout"

echo "Policyd is configured. You can login at http://$ip:7780/webui/index.php with login details policyadmin/$randpass"

if [ "$os" = "ubuntu" ]; then
        apt-get install mailutils -y
elif [ "$os" = "centos" ]; then
        yum install mailx -y
else
        echo ""
fi

echo -e "Policyd web ui login details \n\n URL: http://$ip:7780/webui/index.php \n Username: policyadmin \n Password: $randpass" | mail -s "Policyd installation login details for $HOSTNAME ($ip)" $emailDetails

echo "Email also sent with these details at $emailDetails"

if [ "$os" = "ubuntu" ]; then
        service postfix stop
elif [ "$os" = "centos" ]; then
        echo ""
else
        echo ""
fi

    ;;

        no )
        echo "You have cancelled the installation. Exiting...."
        exit
        ;;

        * )
        echo "Please enter yes or no."
        ;;
esac

fi
";}i:2;i:6;}i:3;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:6506;}i:4;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:6506;}}