a:10:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:28:"Setting Up MySQL replication";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1;}i:4;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:144:"MySQL replication sets up master and slave databases.  The slave database keeps itself synchronized with the master database in near real time.
";}i:2;i:46;}i:5;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:1329:"
In the master server [mysqld] section of /etc/my.cnf you should have something like:mysqld
server-id=1
log-bin
log-warnings
log-slave-updates
skip-bdb
On the master server run:GRANT REPLICATION SLAVE ON . TO
mysql_slave@slaveserverip identified by 'somepassword'
In the slave MySQL server's /etc/my.cnf file you should have something like:server-id = 2
log-bin

so user changes do not replicate to the slaves
replicate-ignore-db=mysql
Note that the server IDs need to be unique for all the servers in your mysql replication cluster.
On the master server run: SHOW MASTER STATUS.

It should output something similar to:--------------------------------------------------------+

| File  | Position | Binlog_Do_DB |  Binlog_Ignore_DB |
--------------------------------------------------------+

| hostxx-bin.000376  | 105373861   |
 
 
--------------------------------------------------------+
On the slave server's MySQL client use some of the values from that command (specifically 'File' and 'Position') to run the following:
CHANGE MASTER TO MASTER_HOST='masterserverip'
, MASTER_USER='mysql_slave', MASTER_PASSWORD='somepassword'
, MASTER_LOG_FILE='<File>'
, MASTER_LOG_POS=<Position>;

SLAVE START;
Further documentation can be found on the official MySQL site here: 

http://dev.mysql.com/doc/refman/5.0/en/replication.html ";}i:2;i:3;i:3;s:1330:">
In the master server [mysqld] section of /etc/my.cnf you should have something like:mysqld
server-id=1
log-bin
log-warnings
log-slave-updates
skip-bdb
On the master server run:GRANT REPLICATION SLAVE ON . TO
mysql_slave@slaveserverip identified by 'somepassword'
In the slave MySQL server's /etc/my.cnf file you should have something like:server-id = 2
log-bin

so user changes do not replicate to the slaves
replicate-ignore-db=mysql
Note that the server IDs need to be unique for all the servers in your mysql replication cluster.
On the master server run: SHOW MASTER STATUS.

It should output something similar to:--------------------------------------------------------+

| File  | Position | Binlog_Do_DB |  Binlog_Ignore_DB |
--------------------------------------------------------+

| hostxx-bin.000376  | 105373861   |
 
 
--------------------------------------------------------+
On the slave server's MySQL client use some of the values from that command (specifically 'File' and 'Position') to run the following:
CHANGE MASTER TO MASTER_HOST='masterserverip'
, MASTER_USER='mysql_slave', MASTER_PASSWORD='somepassword'
, MASTER_LOG_FILE='<File>'
, MASTER_LOG_POS=<Position>;

SLAVE START;
Further documentation can be found on the official MySQL site here: 

http://dev.mysql.com/doc/refman/5.0/en/replication.html ";}i:2;i:195;}i:6;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:0:"";}i:2;i:1532;}i:7;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1532;}i:8;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1532;}i:9;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:1532;}}