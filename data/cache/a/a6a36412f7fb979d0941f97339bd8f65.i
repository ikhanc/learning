a:37:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:26:"Setup Xen HVM with SolusVM";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1;}i:4;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:45:"1. Install a master SolusVM on another node:
";}i:2;i:45;}i:5;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:154:"
wget http://soluslabs.com/installers/solusvm/install
chmod 755 install
./install

--> Select Install SolusVM Master.
--> Then, select No Virtualization. ";}i:2;i:3;i:3;s:155:">
wget http://soluslabs.com/installers/solusvm/install
chmod 755 install
./install

--> Select Install SolusVM Master.
--> Then, select No Virtualization. ";}i:2;i:95;}i:6;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:257;}i:7;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:257;}i:8;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:69:"2. Install Slave SolusVM on the main node where you want your VPS's.
";}i:2;i:259;}i:9;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:159:"
wget http://soluslabs.com/installers/solusvm/install
chmod 755 install
./install

--> Select Install SolusVM Slave.
--> Then, select Start Slave Install(XEN) ";}i:2;i:3;i:3;s:160:">
wget http://soluslabs.com/installers/solusvm/install
chmod 755 install
./install

--> Select Install SolusVM Slave.
--> Then, select Start Slave Install(XEN) ";}i:2;i:333;}i:10;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:500;}i:11;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:500;}i:12;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:38:"Then do the steps below in the master:";}i:2;i:502;}i:13;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:540;}i:14;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:540;}i:15;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:56:"1. Goto admincp and add a IP block. Then add Ip's in it.";}i:2;i:542;}i:16;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:598;}i:17;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:598;}i:18;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:17:"2. Add client(s).";}i:2;i:600;}i:19;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:617;}i:20;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:617;}i:21;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:46:"3. Add ISO's and templates. Then perform sync.";}i:2;i:619;}i:22;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:665;}i:23;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:665;}i:24;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:20:"4. Then create VM's.";}i:2;i:667;}i:25;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:687;}i:26;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:687;}i:27;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:90:"NOTE: If there is problem booting a new VM, type the following after ssh to slave machine:";}i:2;i:689;}i:28;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:779;}i:29;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:779;}i:30;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:35:"xm create /home/xen/vmXXX/vmXXX.cfg";}i:2;i:781;}i:31;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:816;}i:32;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:816;}i:33;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:28:"(where XXX is the VM number)";}i:2;i:818;}i:34;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:846;}i:35;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:846;}i:36;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:846;}}