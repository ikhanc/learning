a:5:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:26813:"
#!/bin/bash

######################## Zimbra Installation ###########################

echo "\n"
echo "############### Zimbra Installation Start ######################"
echo "\n"
echo -n "Please enter domain name : "        # Asking domain name for input
read domain

mkdir /opt/customlog
echo $domain> /opt/customlog/createddomain
chmod 777 /opt/customlog/createddomain

#echo -n "Please enter email on which you want to be notified while DDOS attack: "
#read email_hack_notice

##### Password Generation #####

randpass=`</dev/urandom tr -dc _A-Z-a-z-0-9 | head -c8`

#### Getting IP ####

for IPS in `ifconfig | awk '/inet addr/{print substr($2,6)}'`
do
        if [ "$IPS" != "127.0.0.1" ]; then
                ip=$IPS
        fi
done

echo "">> /etc/hosts
#echo "$ip $domain">> /etc/hosts

#sed  -i '1i nameserver 127.0.0.1' /etc/resolv.conf

#cat>/etc/resolv.conf <<EOL
#nameserver 127.0.0.1
#nameserver 8.8.8.8
#nameserver 8.8.4.4
#EOL

echo "Before Starting installation, Please disable SELinux by 'setenforce 0' and setup your hostname"
echo -n "Are above mentioned settings correct?[yes/no]: "
read setting
case $setting in

    yes )

dist=`grep DISTRIB_ID /etc/*-release | awk -F '=' '{print $2}'`

if [ "$dist" = "Ubuntu" ]; then
        os="ubuntu"
else
        dist=`cat /etc/*-release | head -1 | awk '{print $1}'`
        if [ "$dist" = "CentOS" ]; then
                os="centos"
        fi
fi

touch /opt/customlog/alloutput.txt
chmod 777 /opt/customlog/alloutput.txt

#echo $os

if [ "$os" = "ubuntu" ]; then
    echo ""
    #echo ""> /etc/resolvconf/resolv.conf.d/head
    #echo ""> /etc/resolvconf/resolv.conf.d/base
    #echo ""> /etc/resolvconf/resolv.conf.d/tail
    #echo "nameserver 127.0.0.1">> /etc/resolvconf/resolv.conf.d/head
    #echo "nameserver 8.8.8.8">> /etc/resolvconf/resolv.conf.d/head
    #echo "nameserver 8.8.4.4">> /etc/resolvconf/resolv.conf.d/head
    #resolvconf -u

elif [ "$os" = "centos" ]; then
    echo ""> /etc/resolv.conf
    echo "nameserver 127.0.0.1">> /etc/resolv.conf
    echo "nameserver 8.8.8.8">> /etc/resolv.conf
    echo "nameserver 8.8.4.4">> /etc/resolv.conf
else
    echo ""
fi

if [ "$os" = "ubuntu" ]; then
    echo "ubuntu installation"
    #apt-get update -y
    #apt-get -y install unzip net-tools sysstat openssh-clients perl-core libaio nmap-ncat libstdc++.so.6 wget nc
    #apt-get install unzip net-tools sysstat libgmp10 libperl5.18 pax sysstat sqlite3 wget libaio1
    #setenforce 0
    #service iptables stop
    #update-rc.d iptables disable
    #service sendmail stop 2>/dev/null
    #update-rc.d sendmail disable
    #service postfix stop 2>/dev/null
    #update-rc.d postfix disable
    #service apache2 stop
    #apt-get remove apache2 -y
    #cd /opt
    #wget https://files.zimbra.com/downloads/8.6.0_GA/zcs-8.6.0_GA_1153.UBUNTU14_64.20141215151116.tgz
    #tar zcs-8.6.0_GA_1153.UBUNTU14_64.20141215151116.tgz
    #cd zcs-8.6.0_GA_1153.UBUNTU14_64.20141215151116
    #printf 'y\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\nno\nno\n\n7\n4\nnJ1vh2hS\nr\na\nyes\n\nyes\nno\n\n' | ./install.sh
    #su - zimbra -c 'zmcontrol restart; logout'

    apt-get update -y
    apt-get install -y bind9utils netcat-openbsd sudo libidn11 libpcre3 libgmp10 libexpat1 libstdc++6 libperl5.18 libaio1 resolvconf unzip pax sysstat sqlite3
    setenforce 0
    service iptables stop
    update-rc.d iptables disable
    service sendmail stop
    update-rc.d sendmail disable
    service postfix stop
    update-rc.d postfix disable
    service apache2 stop
    apt-get remove apache2 -y
    cd /opt
    wget https://files.zimbra.com/downloads/8.6.0_GA/zcs-8.6.0_GA_1153.UBUNTU14_64.20141215151116.tgz
    tar xfz zcs-8.6.0_GA_1153.UBUNTU14_64.20141215151116.tgz
    cd zcs-8.6.0_GA_1153.UBUNTU14_64.20141215151116
    #printf 'y\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\nno\n\n7\n4\nnJ1vh2hS\nr\na\nyes\n\nyes\nno\n\n' | ./install.sh
    ./install.sh
    su - zimbra -c 'zmcontrol restart; logout'

elif [ "$os" = "centos" ]; then
    echo "centos installation"
    yum update -y
    yum -y install unzip net-tools sysstat openssh-clients perl-core libaio nmap-ncat libstdc++.so.6 wget nc
    setenforce 0
    service iptables stop
    chkconfig iptables off
    service sendmail stop 2>/dev/null
    chkconfig sendmail off
    #yum remove sendmail -y
        service postfix stop 2>/opt/customlog/alloutput.txt
    chkconfig postfix off
    #yum remove postfix -y
    service httpd stop
    chkconfig httpd off
    yum remove httpd -y
    yum remove httpd-tools -y
    yum install sysstat -y
    #service sendmail stop 2>/dev/null
    #chkconfig sendmail off
    cd /opt
    wget https://files.zimbra.com/downloads/8.6.0_GA/zcs-8.6.0_GA_1153.RHEL6_64.20141215151155.tgz
    tar xfz zcs-8.6.0_GA_1153.RHEL6_64.20141215151155.tgz
    cd zcs-8.6.0_GA_1153.RHEL6_64.20141215151155
    #printf 'y\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\nno\nno\n\n7\n4\nnJ1vh2hS\nr\na\nyes\n\nyes\nno\n\n' | ./install.sh --platform-override
    ./install.sh --platform-override
    su - zimbra -c 'zmcontrol restart; logout'
    #zmcontrol restart
    #logout

else
    echo "Please choose an operating system CentOS or Ubuntu"

fi
    ;;

    no )
        echo "Please set these settings."
    ;;

    * ) echo "Please choose yes or no"
    ;;
esac

####### Adding the domain #######
su - zimbra -c 'newdomain=$(cat /opt/customlog/createddomain); zmprov cd $newdomain zimbraAuthMech zimbra; logout'
#zmprov cd $domain zimbraAuthMech zimbra

###################################################### Adding Custom Settings #########################################################

################ Bounce Message To Sender If User Quota Is Full ##################

su - zimbra -c 'zmprov gacf zimbraLmtpPermanentFailureWhenOverQuota; zmprov mcf zimbraLmtpPermanentFailureWhenOverQuota TRUE; zmcontrol restart; logout'
#zmprov gacf zimbraLmtpPermanentFailureWhenOverQuota
#zmprov mcf zimbraLmtpPermanentFailureWhenOverQuota TRUE
#zmcontrol restart
#logout

################################## IMAP and Port Setting for Hostname ################################

    ## Format of -i: IMAP-STORE:IMAP-PROXY:IMAPS-STORE:IMAPS-PROXY and similar for Ports -p ##

su - zimbra -c '/opt/zimbra/libexec/zmproxyconfig -e -m -o -H $HOSTNAME -i 143:8143:993:8993 -p 110:8110:995:8995; zmcontrol restart; zmconfigdctl restart; logout'
#/opt/zimbra/libexec/zmproxyconfig -e -m -o -H $HOSTNAME -i 143:8143:993:8993 -p 110:8110:995:8995
#zmcontrol restart
#zmconfigdctl restart
#logout

    ## From Global Settings> AS/AV, uncheck “Block Encrypted archives” and save ##
su - zimbra -c 'zmprov mcf zimbraVirusBlockEncryptedArchive FALSE; zmconfigdctl restart; logout'
#zmprov mcf zimbraVirusBlockEncryptedArchive FALSE
#zmconfigdctl restart
#logout

    ## From Global Settings> MTA, uncheck “TLS authentication only” and save. ##
su - zimbra -c 'zmprov mcf zimbraMtaTlsAuthOnly FALSE; zmcontrol restart; logout'
#zmprov mcf zimbraMtaTlsAuthOnly FALSE
#zmcontrol restart
#logout

    ## Enable HTTP in webmail, run below commands as zimbra user. ##
su - zimbra -c 'zmtlsctl both; zmcontrol stop; zmcontrol start; logout'
su - zimbra -c 'zmprov ms $HOSTNAME zimbraReverseProxyMailMode both; zmcontrol restart; logout'
su - zimbra -c 'zmprov ms $HOSTNAME zimbraReverseProxySSLToUpstreamEnabled FALSE; logout'
su - zimbra -c '/opt/zimbra/libexec/zmproxyconfig -e -w -o -a 8080:80:8443:443 -x both -H $HOSTNAME; logout'
su - zimbra -c 'zmtlsctl both; zmprov ms $HOSTNAME zimbraReverseProxyMailMode both; zmprov ms $HOSTNAME zimbraMailMode both; zmcontrol restart; logout'
#zmtlsctl both
#zmcontrol stop
#zmcontrol start
#logout

    ## Maximum attachment size ##
su - zimbra -c 'max_attach_size=26214400; zmprov mcf zimbraMtaMaxMessageSize $max_attach_size; zmconfigdctl restart; logout'
#zmprov mcf zimbraMtaMaxMessageSize 26214400
#zmconfigdctl restart
#logout

########################################## Password Policy, Failed Login Policy, Session Timeout ##########################################

    ## Password Policy ##
su - zimbra -c 'zmprov mc default zimbraPasswordMinLength 8; zmprov mc default zimbraPasswordMinUpperCaseChars 1; zmprov mc default zimbraPasswordMinLowerCaseChars 1; zmprov mc default zimbraPasswordMinNumericChars 1; zmconfigdctl restart; logout'
#zmprov mc default zimbraPasswordMinLength 8
#zmprov mc default zimbraPasswordMinUpperCaseChars 1
#zmprov mc default zimbraPasswordMinLowerCaseChars 1
#zmprov mc default zimbraPasswordMinNumericChars 1
#logout

    ## Failed Login Policy ##
su - zimbra -c 'zmprov mc default zimbraPasswordLockoutEnabled TRUE; zmprov mc default zimbraPasswordLockoutMaxFailures 5; zmprov mc default zimbraPasswordLockoutDuration 1h; zmconfigdctl restart; logout'
#zmprov mc default zimbraPasswordLockoutEnabled TRUE
#zmprov mc default zimbraPasswordLockoutMaxFailures 5
#zmprov mc default zimbraPasswordLockoutDuration 1h
#logout

    ## Session Timeout ##
su - zimbra -c 'zmprov mc default zimbraMailIdleSessionTimeout 1h; zmconfigdctl restart; logout'
#zmprov mc default zimbraMailIdleSessionTimeout 1h
#zmconfigdctl restart
#logout

########################################## Anti SPAM Settings ##########################################

    ## Set Kill Percent: 50 and Tag Percent: 25 ##
su - zimbra -c 'zmprov mcf zimbraSpamKillPercent 50; zmprov mcf zimbraSpamTagPercent 25; zmcontrol restart; logout'
#zmprov mcf zimbraSpamKillPercent 50
#zmprov mcf zimbraSpamTagPercent 25
#zmcontrol restart
#logout

    ## Mail Spoofing ##

# Reject Emails if Sender or reciever does not match from LDAP directory #
su - zimbra -c 'zmprov mcf zimbraMtaSmtpdRejectUnlistedRecipient yes; zmprov mcf zimbraMtaSmtpdRejectUnlistedSender yes; zmmtactl restart; zmcontrol restart; logout'
#zmprov mcf zimbraMtaSmtpdRejectUnlistedRecipient yes
#zmprov mcf zimbraMtaSmtpdRejectUnlistedSender yes
#zmmtactl restart
#zmcontrol restart
#echo "$HOSTNAME REJECT">> /opt/zimbra/conf/domainrestrict           #### For Zimbra older versions 8.0.x and previous
#echo "$domain REJECT">> /opt/zimbra/conf/domainrestrict             #### For Zimbra older versions 8.0.x and previous
#postmap  /opt/zimbra/conf/domainrestrict                             #### For Zimbra older versions 8.0.x and previous
#zmcontrol restart                                                    #### For Zimbra older versions 8.0.x and previous
#logout

# Enforcing a match between FROM address and sasl username #
su - zimbra -c 'zmprov mcf zimbraMtaSmtpdSenderLoginMaps  proxy:ldap:/opt/zimbra/conf/ldap-slm.cf +zimbraMtaSmtpdSenderRestrictions reject_authenticated_sender_login_mismatch; logout'
cp /opt/zimbra/conf/zmconfigd/smtpd_sender_restrictions.cf /opt/zimbra/conf/zmconfigd/smtpd_sender_restrictions.cf.bak
sed -i /permit_mynetworks/s/^/#/ /opt/zimbra/conf/zmconfigd/smtpd_sender_restrictions.cf
sed -i '/permit_mynetworks/i permit_mynetworks, reject_sender_login_mismatch ' /opt/zimbra/conf/zmconfigd/smtpd_sender_restrictions.cf
su - zimbra -c 'zmcontrol restart; logout'
#zmprov mcf zimbraMtaSmtpdSenderLoginMaps  proxy:ldap:/opt/zimbra/conf/ldap-slm.cf +zimbraMtaSmtpdSenderRestrictions reject_authenticated_sender_login_mismatch
#zmcontrol restart                                                    #### For Zimbra older versions 8.0.x and previous
#logout

    ## Enable SpamAssassin rule updates via sa-update.  ##
su - zimbra -c 'zmlocalconfig antispam_enable_rule_updates; zmlocalconfig antispam_enable_restarts; zmlocalconfig -e antispam_enable_rule_updates=true; zmlocalconfig -e antispam_enable_restarts=true; zmamavisdctl restart; zmmtactl restart; logout'
#zmlocalconfig antispam_enable_rule_updates
#zmlocalconfig antispam_enable_restarts
#zmlocalconfig -e antispam_enable_rule_updates=true
#zmlocalconfig -e antispam_enable_restarts=true
#zmamavisdctl restart
#zmmtactl restart
#logout

    ## Enable RBL and DNS checks  ##
su - zimbra -c 'zmprov mcf +zimbraMtaRestriction "reject_invalid_hostname"; zmprov mcf +zimbraMtaRestriction "reject_non_fqdn_hostname"; zmprov mcf +zimbraMtaRestriction "reject_non_fqdn_sender"; zmprov mcf +zimbraMtaRestriction "reject_unknown_sender_domain"; zmprov mcf +zimbraMtaRestriction "reject_rbl_client bl.spamcop.net"; zmprov mcf +zimbraMtaRestriction "reject_rbl_client sbl.spamhaus.org"; zmcontrol restart; zmconfigdctl restart; zmprov gcf zimbraMtaRestriction'
#zmprov mcf +zimbraMtaRestriction "reject_invalid_hostname"
#zmprov mcf +zimbraMtaRestriction "reject_non_fqdn_hostname"
#zmprov mcf +zimbraMtaRestriction "reject_non_fqdn_sender"
#zmprov mcf +zimbraMtaRestriction "reject_unknown_sender_domain"
#zmprov mcf +zimbraMtaRestriction "reject_rbl_client bl.spamcop.net"
#zmprov mcf +zimbraMtaRestriction "reject_rbl_client sbl.spamhaus.org"
#zmcontrol restart
#zmconfigdctl restart
#zmprov gcf zimbraMtaRestriction

    ## Increase SPAM Fail score.  ##

cp /opt/zimbra/data/spamassassin/rules/50_scores.cf /opt/zimbra/data/spamassassin/rules/50_scores.cf.bak

sed -i /SPF_NONE/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_NONE/i score SPF_NONE 2 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_HELO_NONE/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_HELO_NONE/i score SPF_HELO_NONE 0 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_PASS/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_PASS/i score SPF_PASS -0.001 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_HELO_PASS/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_HELO_PASS/i score SPF_HELO_PASS -0.001 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_FAIL/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_FAIL/i score SPF_FAIL 2 2.919 2 2.001 # n=0 n=2 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_HELO_FAIL/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_HELO_FAIL/i score SPF_HELO_FAIL 2 2.001 2 2.001 # n=0 n=2 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_HELO_NEUTRAL/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_HELO_NEUTRAL/i score SPF_HELO_NEUTRAL 2 2.001 2 2.112 # n=0 n=2 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_HELO_SOFTFAIL/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_HELO_SOFTFAIL/i score SPF_HELO_SOFTFAIL 2 2.896 2 2.732 # n=0 n=2 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_NEUTRAL/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_NEUTRAL/i score SPF_NEUTRAL 2 2.652 2 2.779 # n=0 n=2 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_SOFTFAIL/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_SOFTFAIL/i score SPF_SOFTFAIL 2 2.972 2 2.665 # n=0 n=2 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /BAYES_00/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/BAYES_00/i score BAYES_00  0  0 -0.001 -0.001 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /URIBL_BLOCKED/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/URIBL_BLOCKED/i score URIBL_BLOCKED 0 2.001 0 2.001 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /FSL_HELO_NON_FQDN_1/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/FSL_HELO_NON_FQDN_1/i score FSL_HELO_NON_FQDN_1 2.361 2.001 1.783 2.001 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

su - zimbra -c 'zmtrainsa --cleanup; logout'

##################################### DDOS Checks and Send Email #####################################

    ## Configuration ##
su - zimbra -c 'email_hack_notice=alerts@mantraventures.com; zmlocalconfig -e zimbra_swatch_ipacct_threshold=10; zmlocalconfig -e zimbra_swatch_acct_threshold=20; zmlocalconfig -e zimbra_swatch_ip_threshold=5; zmlocalconfig -e zimbra_swatch_total_threshold=100; zmlocalconfig -e zimbra_swatch_threshold_seconds=60; zmlocalconfig -e zimbra_swatch_notice_user=$email_hack_notice; zmauditswatchctl restart; logout'
#zmlocalconfig -e zimbra_swatch_ipacct_threshold=10
#zmlocalconfig -e zimbra_swatch_acct_threshold=20
#zmlocalconfig -e zimbra_swatch_ip_threshold=5
#zmlocalconfig -e zimbra_swatch_total_threshold=100
#zmlocalconfig -e zimbra_swatch_threshold_seconds=60
#zmlocalconfig -e zimbra_swatch_notice_user=$email_hack_notice
#zmauditswatchctl restart
#logout

    ## Activating in Boot Sequence ##
cd /opt
wget https://wiki.zimbra.com/images/d/d9/Zmauditswatch.tar      #For CentOS
tar xvf Zmauditswatch.tar
mv zmauditswatch.txt zmauditswatch
cp zmauditswatch /etc/init.d/zmauditswatch
chmod 755 /etc/init.d/zmauditswatch
if [ "$os" = "ubuntu" ]; then
        update-rc.d zmauditswatch defaults
    update-rc.d zmauditswatch enable
elif [ "$os" = "centos" ]; then
        chkconfig zmauditswatch on
else
        echo ""
fi

################################### Adding Mail Queue alert in Zimbra ###############################################

if [ "$os" = "ubuntu" ]; then
    echo ""
    #apt-get install mailutils -y
elif [ "$os" = "centos" ]; then
    yum install mailx -y
else
    echo ""
fi

cat>/root/queue.sh <<EOL
#!/bin/bash

hostname=$HOSTNAME
to=alerts@mantraventures.com
limit=250

mail="/opt/zimbra/postfix/sbin/postqueue -p | tail -n 1 |    cut -d' ' -f5"

queue=$(bash -c "$mail")

if [ $queue -gt $limit ];then

echo "Queued mails on $hostname is $queue" | mail -s "Mail Queue Alert at $hostname" $to

fi
EOL

chmod +x /root/queue.sh
(crontab -l ; echo "0 * * * *  sh /root/queue.sh") | sort - | uniq - | crontab -

########################### Policyd Installation and Configuration #######################

    ## Activate Policyd ##
su - zimbra -c 'zmprov ms $HOSTNAME +zimbraServiceInstalled cbpolicyd +zimbraServiceEnabled cbpolicyd; logout'
#zmprov ms $HOSTNAME +zimbraServiceInstalled cbpolicyd +zimbraServiceEnabled cbpolicyd

    ## Activate Policyd WebUI Run the following command as root ##
cd /opt/zimbra/httpd/htdocs/ && ln -s ../../cbpolicyd/share/webui
cp /opt/zimbra/cbpolicyd/share/webui/includes/config.php /opt/zimbra/cbpolicyd/share/webui/includes/config.php.bak
sed -i /DB_DSN/s/^/#/ /opt/zimbra/cbpolicyd/share/webui/includes/config.php
sed -i '/DB_USER/i $DB_DSN="sqlite:/opt/zimbra/data/cbpolicyd/db/cbpolicyd.sqlitedb"; ' /opt/zimbra/cbpolicyd/share/webui/includes/config.php
su - zimbra -c 'zmcontrol restart; zmapachectl restart; logout'

    ## Limit 500 email per user per hour ##

policyd_limit_emails=200         #How many emails to sent in the defined time
policyd_limit_time=3600         #time limit in seconds

cat>/root/rate-limit.sql <<EOL
BEGIN TRANSACTION;
INSERT INTO policy_groups (Name,Comment,Disabled) VALUES ('list_domains','',0);
INSERT INTO policy_group_members (PolicyGroupID,Member,Comment,Disabled) VALUES (3,'@$domain','',0);
INSERT INTO policy_group_members (PolicyGroupID,Member,Comment,Disabled) VALUES (3,'@$HOSTNAME','',0);
INSERT INTO policies (Name,Priority,Description,Disabled) VALUES ('rate limit',20,'rate limit for user',0);
INSERT INTO policy_members (PolicyID,Source,Destination,Comment,Disabled) VALUES (6,'%list_domain','any','',0);
INSERT INTO quotas (PolicyID,Name,Track,Period,Verdict,Data,LastQuota,Comment,Disabled) VALUES (6,'Rate Limit','Sender:user@domain',$policyd_limit_time,'DEFER','Sorry, your quota to sending mail in this hour has been exceeded.',0,'',0);
INSERT INTO quotas_limits (QuotasID,Type,CounterLimit,Comment,Disabled) VALUES (3,'MessageCount',$policyd_limit_emails,'',0);
COMMIT;
EOL

sqlite3 /opt/zimbra/data/cbpolicyd/db/cbpolicyd.sqlitedb </root/rate-limit.sql
rm -vf /root/rate-limit.sql

########################### Protecting Policyd Web UI #######################################
cd /opt/zimbra/cbpolicyd/share/webui/
cat>.htaccess <<EOL
AuthUserFile /opt/zimbra/cbpolicyd/share/webui/.htpasswd
AuthGroupFile /dev/null
AuthName "Username and Password"
AuthType Basic

<LIMIT GET>
require valid-user
</LIMIT>
EOL
touch .htpasswd
/opt/zimbra/httpd/bin/htpasswd -cb .htpasswd policyadmin $randpass
cp /opt/zimbra/conf/httpd.conf /opt/zimbra/conf/httpd.conf.bak
echo " ">> /opt/zimbra/conf/httpd.conf
echo "Alias /webui /opt/zimbra/cbpolicyd/share/webui/">> /opt/zimbra/conf/httpd.conf
echo "<Directory /opt/zimbra/cbpolicyd/share/webui/>">> /opt/zimbra/conf/httpd.conf
echo "# Comment out the following 3 lines to make web ui accessible from anywhere">> /opt/zimbra/conf/httpd.conf
echo "AllowOverride AuthConfig">> /opt/zimbra/conf/httpd.conf
echo "Order Deny,Allow">> /opt/zimbra/conf/httpd.conf
echo "Allow from all">> /opt/zimbra/conf/httpd.conf
echo "</Directory>">> /opt/zimbra/conf/httpd.conf

su - zimbra -c "zmapachectl restart; logout"

###################################### R1 Soft Installation #######################################
#cd /etc/yum.repos.d
#cat>r1soft.repo <<EOL
#[r1soft]
#name=R1Soft Repository Server
#baseurl=http://repo.r1soft.com/yum/stable/$basearch/
#enabled=1
#gpgcheck=0
#EOL
#yum install r1soft-cdp-enterprise-agent -y

#################################### Adding Zabbix agent ########################################

if [ "$os" = "centos" ]; then
    cd /opt
    wget http://sourceforge.net/projects/zabbix/files/ZABBIX%20Latest%20Stable/2.2.2/zabbix-2.2.2.tar.gz
    tar -zxvf zabbix-2.2.2.tar.gz
    groupadd zabbix
    useradd -g zabbix zabbix
    cd zabbix-2.2.2
    yum install -y zlib-devel glibc-devel curl-devel gcc automake libidn-devel openssl-devel net-snmp-devel rpm-devel OpenIPMI-devel make
    ./configure --enable-agent --enable-static
    ./configure --enable-agent
    make install
    cp /usr/local/etc/zabbix_agentd.conf /usr/local/etc/zabbix_agentd.conf.bak
    sed -i /Server=127.0.0.1/s/^/#/ /usr/local/etc/zabbix_agentd.conf
    sed -i '/Server=127.0.0.1/i Server=noc.mantragrid.com ' /usr/local/etc/zabbix_agentd.conf
    cp misc/init.d/fedora/core5/zabbix_agentd /etc/init.d/
    chmod 755 /etc/init.d/zabbix_agentd
    chkconfig zabbix_agentd on
    /etc/init.d/zabbix_agentd start

elif [ "$os" = "ubuntu" ]; then
    echo "Skipping Zabbix Installation"
else
    echo ""
fi

################################## Installing and Configuring Zimbra Server Statistics ###########################

#sed -i /SYSLOGD_OPTIONS/s/^/#/ /etc/sysconfig/rsyslog
#sed -i '/SYSLOGD_OPTIONS/i SYSLOGD_OPTIONS="-r -c 5" ' /etc/sysconfig/rsyslog
#service rsyslog restart
#/opt/zimbra/libexec/zmsyslogsetup
#service rsyslog restart

# Home -> Monitor -> Server Statistics

#################################### Installing Zxtras with Zchat ###############################

    ## Installing Zxtras ##
#cd /opt
#wget http://www.zextras.com/download/zextras_suite-latest.tgz
#tar zxvf zextras_suite-latest.tgz
#cd zextras_suite-2.2.5
##./install.sh all
##printf 'y\n\ny\ny\ny\n' | ./install.sh core
#printf 'y\ny\n\ny\ny\ny\n' | ./install.sh core
#su - zimbra -c 'zmprov fc -a zimlet; logout'

    ## Installing Zextras Chat ##
#su - zimbra -c 'cd /opt/zimbra/zimlets; wget https://download.zextras.com/com_zextras_chat.zip; zmzimletctl deploy com_zextras_chat.zip; logout'
#cd /opt/zimbra/zimlets
#wget https://download.zextras.com/com_zextras_chat.zip
#zmzimletctl deploy com_zextras_chat.zip
#logout

######################### Stopping Spamming by restricting not using another From address #####################

    ## Stop user to relay emails using a different email address ##
#su - zimbra -c 'zmprov mcf zimbraMtaSmtpdSenderLoginMaps proxy:ldap:/opt/zimbra/conf/ldap-slm.cf +zimbraMtaSmtpdSenderRestrictions reject_authenticated_sender_login_mismatch; logout'
#zmprov mcf zimbraMtaSmtpdSenderLoginMaps proxy:ldap:/opt/zimbra/conf/ldap-slm.cf +zimbraMtaSmtpdSenderRestrictions reject_authenticated_sender_login_mismatch
#logout

    ## This configuration will not accept if user not use authentication/password ##
#sed -i /permit_mynetworks/s/^/#/ /opt/zimbra/conf/zmconfigd/smtpd_sender_restrictions.cf
#sed -i '/permit_mynetworks/i permit_mynetworks, reject_sender_login_mismatch ' /opt/zimbra/conf/zmconfigd/smtpd_sender_restrictions.cf
#su - zimbra -c 'zmcontrol restart; logout'

################################### Daily user email report ################################

su - zimbra -c 'zmlocalconfig -e zimbra_mtareport_max_recipients=50; zmlocalconfig -e zimbra_mtareport_max_senders=50; zmcontrol restart; logout'
#zmlocalconfig -e zimbra_mtareport_max_recipients=50
#zmlocalconfig -e zimbra_mtareport_max_senders=50
#zmcontrol restart
#logout

emailDetails=tech@mantraventures.com

adminpass=`</dev/urandom tr -dc _A-Z-a-z-0-9 | head -c8`
touch /opt/customlog/adminpass
chmod 777 /opt/customlog/adminpass
echo $adminpass> /opt/customlog/adminpass

su - zimbra -c 'adminpassword=$(cat /opt/customlog/adminpass); zmprov sp admin@$HOSTNAME $adminpassword; zmconfigdctl restart; logout'

echo "Zimbra has been installed. Please open URL https://$ip:7071  to view Admin Panel with login details admin/$adminpass"
echo "Policyd is configured. You can login at http://$ip:7780/webui/index.php with login details policyadmin/$randpass"

if [ "$os" = "ubuntu" ]; then
cat <<EOF | /opt/zimbra/postfix/sbin/sendmail -t
To: $emailDetails
Subject: New Zimbra Server $HOSTNAME ($ip) Login Details

    Zimbra and Policyd web ui login details

    Zimbra Login Details
    URL: https://$ip:7071
    Username: admin
    Password: $adminpass

    Policyd web ui login details
    URL: http://$ip:7780/webui/index.php
    Username: policyadmin
    Password: $randpass
EOF
elif [ "$os" = "centos" ]; then
    echo -e "To: $emailDetails\r\nSubject: New Zimbra Server $HOSTNAME ($ip) Login Details\r\n\nZimbra and Policyd web ui login details \n\n Zimbra Login Details \n URL: https://$ip:7071 \n Username: admin \n Password: $adminpass \n\n Policyd web ui login details \n URL: http://$ip:7780/webui/index.php \n Username: policyadmin \n Password: $randpass" | /opt/zimbra/postfix/sbin/sendmail -t
    #echo -e "Zimbra and Policyd web ui login details \n\n Zimbra Login Details \n URL: https://$ip:7071 \n Username: admin \n Password: $adminpass \n\n Policyd web ui login details \n URL: http://$ip:7780/webui/index.php \n Username: policyadmin \n Password: $randpass" | mail -s "New Zimbra Server $HOSTNAME ($ip) Login Details" $emailDetails
else
    echo ""
fi

#echo -e "Zimbra and Policyd web ui login details \n\n Zimbra Login Details \n URL: https://$ip:7071 \n Username: admin \n Password: $adminpass \n\n Policyd web ui login details \n URL: http://$ip:7780/webui/index.php \n Username: policyadmin \n Password: $randpass" | mail -s "New Zimbra Server $HOSTNAME ($ip) Login Details" $emailDetails

echo "Email also sent with these details at $emailDetails"

if [ "$os" = "ubuntu" ]; then
        service postfix stop 2>/dev/null
    update-rc.d postfix disable 2>/dev/null
elif [ "$os" = "centos" ]; then
        echo ""
else
        echo ""
fi
";}i:2;i:3;i:3;s:26814:">
#!/bin/bash

######################## Zimbra Installation ###########################

echo "\n"
echo "############### Zimbra Installation Start ######################"
echo "\n"
echo -n "Please enter domain name : "        # Asking domain name for input
read domain

mkdir /opt/customlog
echo $domain> /opt/customlog/createddomain
chmod 777 /opt/customlog/createddomain

#echo -n "Please enter email on which you want to be notified while DDOS attack: "
#read email_hack_notice

##### Password Generation #####

randpass=`</dev/urandom tr -dc _A-Z-a-z-0-9 | head -c8`

#### Getting IP ####

for IPS in `ifconfig | awk '/inet addr/{print substr($2,6)}'`
do
        if [ "$IPS" != "127.0.0.1" ]; then
                ip=$IPS
        fi
done

echo "">> /etc/hosts
#echo "$ip $domain">> /etc/hosts

#sed  -i '1i nameserver 127.0.0.1' /etc/resolv.conf

#cat>/etc/resolv.conf <<EOL
#nameserver 127.0.0.1
#nameserver 8.8.8.8
#nameserver 8.8.4.4
#EOL

echo "Before Starting installation, Please disable SELinux by 'setenforce 0' and setup your hostname"
echo -n "Are above mentioned settings correct?[yes/no]: "
read setting
case $setting in

    yes )

dist=`grep DISTRIB_ID /etc/*-release | awk -F '=' '{print $2}'`

if [ "$dist" = "Ubuntu" ]; then
        os="ubuntu"
else
        dist=`cat /etc/*-release | head -1 | awk '{print $1}'`
        if [ "$dist" = "CentOS" ]; then
                os="centos"
        fi
fi

touch /opt/customlog/alloutput.txt
chmod 777 /opt/customlog/alloutput.txt

#echo $os

if [ "$os" = "ubuntu" ]; then
    echo ""
    #echo ""> /etc/resolvconf/resolv.conf.d/head
    #echo ""> /etc/resolvconf/resolv.conf.d/base
    #echo ""> /etc/resolvconf/resolv.conf.d/tail
    #echo "nameserver 127.0.0.1">> /etc/resolvconf/resolv.conf.d/head
    #echo "nameserver 8.8.8.8">> /etc/resolvconf/resolv.conf.d/head
    #echo "nameserver 8.8.4.4">> /etc/resolvconf/resolv.conf.d/head
    #resolvconf -u

elif [ "$os" = "centos" ]; then
    echo ""> /etc/resolv.conf
    echo "nameserver 127.0.0.1">> /etc/resolv.conf
    echo "nameserver 8.8.8.8">> /etc/resolv.conf
    echo "nameserver 8.8.4.4">> /etc/resolv.conf
else
    echo ""
fi

if [ "$os" = "ubuntu" ]; then
    echo "ubuntu installation"
    #apt-get update -y
    #apt-get -y install unzip net-tools sysstat openssh-clients perl-core libaio nmap-ncat libstdc++.so.6 wget nc
    #apt-get install unzip net-tools sysstat libgmp10 libperl5.18 pax sysstat sqlite3 wget libaio1
    #setenforce 0
    #service iptables stop
    #update-rc.d iptables disable
    #service sendmail stop 2>/dev/null
    #update-rc.d sendmail disable
    #service postfix stop 2>/dev/null
    #update-rc.d postfix disable
    #service apache2 stop
    #apt-get remove apache2 -y
    #cd /opt
    #wget https://files.zimbra.com/downloads/8.6.0_GA/zcs-8.6.0_GA_1153.UBUNTU14_64.20141215151116.tgz
    #tar zcs-8.6.0_GA_1153.UBUNTU14_64.20141215151116.tgz
    #cd zcs-8.6.0_GA_1153.UBUNTU14_64.20141215151116
    #printf 'y\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\nno\nno\n\n7\n4\nnJ1vh2hS\nr\na\nyes\n\nyes\nno\n\n' | ./install.sh
    #su - zimbra -c 'zmcontrol restart; logout'

    apt-get update -y
    apt-get install -y bind9utils netcat-openbsd sudo libidn11 libpcre3 libgmp10 libexpat1 libstdc++6 libperl5.18 libaio1 resolvconf unzip pax sysstat sqlite3
    setenforce 0
    service iptables stop
    update-rc.d iptables disable
    service sendmail stop
    update-rc.d sendmail disable
    service postfix stop
    update-rc.d postfix disable
    service apache2 stop
    apt-get remove apache2 -y
    cd /opt
    wget https://files.zimbra.com/downloads/8.6.0_GA/zcs-8.6.0_GA_1153.UBUNTU14_64.20141215151116.tgz
    tar xfz zcs-8.6.0_GA_1153.UBUNTU14_64.20141215151116.tgz
    cd zcs-8.6.0_GA_1153.UBUNTU14_64.20141215151116
    #printf 'y\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\nno\n\n7\n4\nnJ1vh2hS\nr\na\nyes\n\nyes\nno\n\n' | ./install.sh
    ./install.sh
    su - zimbra -c 'zmcontrol restart; logout'

elif [ "$os" = "centos" ]; then
    echo "centos installation"
    yum update -y
    yum -y install unzip net-tools sysstat openssh-clients perl-core libaio nmap-ncat libstdc++.so.6 wget nc
    setenforce 0
    service iptables stop
    chkconfig iptables off
    service sendmail stop 2>/dev/null
    chkconfig sendmail off
    #yum remove sendmail -y
        service postfix stop 2>/opt/customlog/alloutput.txt
    chkconfig postfix off
    #yum remove postfix -y
    service httpd stop
    chkconfig httpd off
    yum remove httpd -y
    yum remove httpd-tools -y
    yum install sysstat -y
    #service sendmail stop 2>/dev/null
    #chkconfig sendmail off
    cd /opt
    wget https://files.zimbra.com/downloads/8.6.0_GA/zcs-8.6.0_GA_1153.RHEL6_64.20141215151155.tgz
    tar xfz zcs-8.6.0_GA_1153.RHEL6_64.20141215151155.tgz
    cd zcs-8.6.0_GA_1153.RHEL6_64.20141215151155
    #printf 'y\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\nno\nno\n\n7\n4\nnJ1vh2hS\nr\na\nyes\n\nyes\nno\n\n' | ./install.sh --platform-override
    ./install.sh --platform-override
    su - zimbra -c 'zmcontrol restart; logout'
    #zmcontrol restart
    #logout

else
    echo "Please choose an operating system CentOS or Ubuntu"

fi
    ;;

    no )
        echo "Please set these settings."
    ;;

    * ) echo "Please choose yes or no"
    ;;
esac

####### Adding the domain #######
su - zimbra -c 'newdomain=$(cat /opt/customlog/createddomain); zmprov cd $newdomain zimbraAuthMech zimbra; logout'
#zmprov cd $domain zimbraAuthMech zimbra

###################################################### Adding Custom Settings #########################################################

################ Bounce Message To Sender If User Quota Is Full ##################

su - zimbra -c 'zmprov gacf zimbraLmtpPermanentFailureWhenOverQuota; zmprov mcf zimbraLmtpPermanentFailureWhenOverQuota TRUE; zmcontrol restart; logout'
#zmprov gacf zimbraLmtpPermanentFailureWhenOverQuota
#zmprov mcf zimbraLmtpPermanentFailureWhenOverQuota TRUE
#zmcontrol restart
#logout

################################## IMAP and Port Setting for Hostname ################################

    ## Format of -i: IMAP-STORE:IMAP-PROXY:IMAPS-STORE:IMAPS-PROXY and similar for Ports -p ##

su - zimbra -c '/opt/zimbra/libexec/zmproxyconfig -e -m -o -H $HOSTNAME -i 143:8143:993:8993 -p 110:8110:995:8995; zmcontrol restart; zmconfigdctl restart; logout'
#/opt/zimbra/libexec/zmproxyconfig -e -m -o -H $HOSTNAME -i 143:8143:993:8993 -p 110:8110:995:8995
#zmcontrol restart
#zmconfigdctl restart
#logout

    ## From Global Settings> AS/AV, uncheck “Block Encrypted archives” and save ##
su - zimbra -c 'zmprov mcf zimbraVirusBlockEncryptedArchive FALSE; zmconfigdctl restart; logout'
#zmprov mcf zimbraVirusBlockEncryptedArchive FALSE
#zmconfigdctl restart
#logout

    ## From Global Settings> MTA, uncheck “TLS authentication only” and save. ##
su - zimbra -c 'zmprov mcf zimbraMtaTlsAuthOnly FALSE; zmcontrol restart; logout'
#zmprov mcf zimbraMtaTlsAuthOnly FALSE
#zmcontrol restart
#logout

    ## Enable HTTP in webmail, run below commands as zimbra user. ##
su - zimbra -c 'zmtlsctl both; zmcontrol stop; zmcontrol start; logout'
su - zimbra -c 'zmprov ms $HOSTNAME zimbraReverseProxyMailMode both; zmcontrol restart; logout'
su - zimbra -c 'zmprov ms $HOSTNAME zimbraReverseProxySSLToUpstreamEnabled FALSE; logout'
su - zimbra -c '/opt/zimbra/libexec/zmproxyconfig -e -w -o -a 8080:80:8443:443 -x both -H $HOSTNAME; logout'
su - zimbra -c 'zmtlsctl both; zmprov ms $HOSTNAME zimbraReverseProxyMailMode both; zmprov ms $HOSTNAME zimbraMailMode both; zmcontrol restart; logout'
#zmtlsctl both
#zmcontrol stop
#zmcontrol start
#logout

    ## Maximum attachment size ##
su - zimbra -c 'max_attach_size=26214400; zmprov mcf zimbraMtaMaxMessageSize $max_attach_size; zmconfigdctl restart; logout'
#zmprov mcf zimbraMtaMaxMessageSize 26214400
#zmconfigdctl restart
#logout

########################################## Password Policy, Failed Login Policy, Session Timeout ##########################################

    ## Password Policy ##
su - zimbra -c 'zmprov mc default zimbraPasswordMinLength 8; zmprov mc default zimbraPasswordMinUpperCaseChars 1; zmprov mc default zimbraPasswordMinLowerCaseChars 1; zmprov mc default zimbraPasswordMinNumericChars 1; zmconfigdctl restart; logout'
#zmprov mc default zimbraPasswordMinLength 8
#zmprov mc default zimbraPasswordMinUpperCaseChars 1
#zmprov mc default zimbraPasswordMinLowerCaseChars 1
#zmprov mc default zimbraPasswordMinNumericChars 1
#logout

    ## Failed Login Policy ##
su - zimbra -c 'zmprov mc default zimbraPasswordLockoutEnabled TRUE; zmprov mc default zimbraPasswordLockoutMaxFailures 5; zmprov mc default zimbraPasswordLockoutDuration 1h; zmconfigdctl restart; logout'
#zmprov mc default zimbraPasswordLockoutEnabled TRUE
#zmprov mc default zimbraPasswordLockoutMaxFailures 5
#zmprov mc default zimbraPasswordLockoutDuration 1h
#logout

    ## Session Timeout ##
su - zimbra -c 'zmprov mc default zimbraMailIdleSessionTimeout 1h; zmconfigdctl restart; logout'
#zmprov mc default zimbraMailIdleSessionTimeout 1h
#zmconfigdctl restart
#logout

########################################## Anti SPAM Settings ##########################################

    ## Set Kill Percent: 50 and Tag Percent: 25 ##
su - zimbra -c 'zmprov mcf zimbraSpamKillPercent 50; zmprov mcf zimbraSpamTagPercent 25; zmcontrol restart; logout'
#zmprov mcf zimbraSpamKillPercent 50
#zmprov mcf zimbraSpamTagPercent 25
#zmcontrol restart
#logout

    ## Mail Spoofing ##

# Reject Emails if Sender or reciever does not match from LDAP directory #
su - zimbra -c 'zmprov mcf zimbraMtaSmtpdRejectUnlistedRecipient yes; zmprov mcf zimbraMtaSmtpdRejectUnlistedSender yes; zmmtactl restart; zmcontrol restart; logout'
#zmprov mcf zimbraMtaSmtpdRejectUnlistedRecipient yes
#zmprov mcf zimbraMtaSmtpdRejectUnlistedSender yes
#zmmtactl restart
#zmcontrol restart
#echo "$HOSTNAME REJECT">> /opt/zimbra/conf/domainrestrict           #### For Zimbra older versions 8.0.x and previous
#echo "$domain REJECT">> /opt/zimbra/conf/domainrestrict             #### For Zimbra older versions 8.0.x and previous
#postmap  /opt/zimbra/conf/domainrestrict                             #### For Zimbra older versions 8.0.x and previous
#zmcontrol restart                                                    #### For Zimbra older versions 8.0.x and previous
#logout

# Enforcing a match between FROM address and sasl username #
su - zimbra -c 'zmprov mcf zimbraMtaSmtpdSenderLoginMaps  proxy:ldap:/opt/zimbra/conf/ldap-slm.cf +zimbraMtaSmtpdSenderRestrictions reject_authenticated_sender_login_mismatch; logout'
cp /opt/zimbra/conf/zmconfigd/smtpd_sender_restrictions.cf /opt/zimbra/conf/zmconfigd/smtpd_sender_restrictions.cf.bak
sed -i /permit_mynetworks/s/^/#/ /opt/zimbra/conf/zmconfigd/smtpd_sender_restrictions.cf
sed -i '/permit_mynetworks/i permit_mynetworks, reject_sender_login_mismatch ' /opt/zimbra/conf/zmconfigd/smtpd_sender_restrictions.cf
su - zimbra -c 'zmcontrol restart; logout'
#zmprov mcf zimbraMtaSmtpdSenderLoginMaps  proxy:ldap:/opt/zimbra/conf/ldap-slm.cf +zimbraMtaSmtpdSenderRestrictions reject_authenticated_sender_login_mismatch
#zmcontrol restart                                                    #### For Zimbra older versions 8.0.x and previous
#logout

    ## Enable SpamAssassin rule updates via sa-update.  ##
su - zimbra -c 'zmlocalconfig antispam_enable_rule_updates; zmlocalconfig antispam_enable_restarts; zmlocalconfig -e antispam_enable_rule_updates=true; zmlocalconfig -e antispam_enable_restarts=true; zmamavisdctl restart; zmmtactl restart; logout'
#zmlocalconfig antispam_enable_rule_updates
#zmlocalconfig antispam_enable_restarts
#zmlocalconfig -e antispam_enable_rule_updates=true
#zmlocalconfig -e antispam_enable_restarts=true
#zmamavisdctl restart
#zmmtactl restart
#logout

    ## Enable RBL and DNS checks  ##
su - zimbra -c 'zmprov mcf +zimbraMtaRestriction "reject_invalid_hostname"; zmprov mcf +zimbraMtaRestriction "reject_non_fqdn_hostname"; zmprov mcf +zimbraMtaRestriction "reject_non_fqdn_sender"; zmprov mcf +zimbraMtaRestriction "reject_unknown_sender_domain"; zmprov mcf +zimbraMtaRestriction "reject_rbl_client bl.spamcop.net"; zmprov mcf +zimbraMtaRestriction "reject_rbl_client sbl.spamhaus.org"; zmcontrol restart; zmconfigdctl restart; zmprov gcf zimbraMtaRestriction'
#zmprov mcf +zimbraMtaRestriction "reject_invalid_hostname"
#zmprov mcf +zimbraMtaRestriction "reject_non_fqdn_hostname"
#zmprov mcf +zimbraMtaRestriction "reject_non_fqdn_sender"
#zmprov mcf +zimbraMtaRestriction "reject_unknown_sender_domain"
#zmprov mcf +zimbraMtaRestriction "reject_rbl_client bl.spamcop.net"
#zmprov mcf +zimbraMtaRestriction "reject_rbl_client sbl.spamhaus.org"
#zmcontrol restart
#zmconfigdctl restart
#zmprov gcf zimbraMtaRestriction

    ## Increase SPAM Fail score.  ##

cp /opt/zimbra/data/spamassassin/rules/50_scores.cf /opt/zimbra/data/spamassassin/rules/50_scores.cf.bak

sed -i /SPF_NONE/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_NONE/i score SPF_NONE 2 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_HELO_NONE/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_HELO_NONE/i score SPF_HELO_NONE 0 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_PASS/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_PASS/i score SPF_PASS -0.001 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_HELO_PASS/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_HELO_PASS/i score SPF_HELO_PASS -0.001 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_FAIL/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_FAIL/i score SPF_FAIL 2 2.919 2 2.001 # n=0 n=2 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_HELO_FAIL/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_HELO_FAIL/i score SPF_HELO_FAIL 2 2.001 2 2.001 # n=0 n=2 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_HELO_NEUTRAL/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_HELO_NEUTRAL/i score SPF_HELO_NEUTRAL 2 2.001 2 2.112 # n=0 n=2 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_HELO_SOFTFAIL/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_HELO_SOFTFAIL/i score SPF_HELO_SOFTFAIL 2 2.896 2 2.732 # n=0 n=2 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_NEUTRAL/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_NEUTRAL/i score SPF_NEUTRAL 2 2.652 2 2.779 # n=0 n=2 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /SPF_SOFTFAIL/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/SPF_SOFTFAIL/i score SPF_SOFTFAIL 2 2.972 2 2.665 # n=0 n=2 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /BAYES_00/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/BAYES_00/i score BAYES_00  0  0 -0.001 -0.001 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /URIBL_BLOCKED/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/URIBL_BLOCKED/i score URIBL_BLOCKED 0 2.001 0 2.001 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

sed -i /FSL_HELO_NON_FQDN_1/s/^/#/ /opt/zimbra/data/spamassassin/rules/50_scores.cf
sed -i '/FSL_HELO_NON_FQDN_1/i score FSL_HELO_NON_FQDN_1 2.361 2.001 1.783 2.001 ' /opt/zimbra/data/spamassassin/rules/50_scores.cf

su - zimbra -c 'zmtrainsa --cleanup; logout'

##################################### DDOS Checks and Send Email #####################################

    ## Configuration ##
su - zimbra -c 'email_hack_notice=alerts@mantraventures.com; zmlocalconfig -e zimbra_swatch_ipacct_threshold=10; zmlocalconfig -e zimbra_swatch_acct_threshold=20; zmlocalconfig -e zimbra_swatch_ip_threshold=5; zmlocalconfig -e zimbra_swatch_total_threshold=100; zmlocalconfig -e zimbra_swatch_threshold_seconds=60; zmlocalconfig -e zimbra_swatch_notice_user=$email_hack_notice; zmauditswatchctl restart; logout'
#zmlocalconfig -e zimbra_swatch_ipacct_threshold=10
#zmlocalconfig -e zimbra_swatch_acct_threshold=20
#zmlocalconfig -e zimbra_swatch_ip_threshold=5
#zmlocalconfig -e zimbra_swatch_total_threshold=100
#zmlocalconfig -e zimbra_swatch_threshold_seconds=60
#zmlocalconfig -e zimbra_swatch_notice_user=$email_hack_notice
#zmauditswatchctl restart
#logout

    ## Activating in Boot Sequence ##
cd /opt
wget https://wiki.zimbra.com/images/d/d9/Zmauditswatch.tar      #For CentOS
tar xvf Zmauditswatch.tar
mv zmauditswatch.txt zmauditswatch
cp zmauditswatch /etc/init.d/zmauditswatch
chmod 755 /etc/init.d/zmauditswatch
if [ "$os" = "ubuntu" ]; then
        update-rc.d zmauditswatch defaults
    update-rc.d zmauditswatch enable
elif [ "$os" = "centos" ]; then
        chkconfig zmauditswatch on
else
        echo ""
fi

################################### Adding Mail Queue alert in Zimbra ###############################################

if [ "$os" = "ubuntu" ]; then
    echo ""
    #apt-get install mailutils -y
elif [ "$os" = "centos" ]; then
    yum install mailx -y
else
    echo ""
fi

cat>/root/queue.sh <<EOL
#!/bin/bash

hostname=$HOSTNAME
to=alerts@mantraventures.com
limit=250

mail="/opt/zimbra/postfix/sbin/postqueue -p | tail -n 1 |    cut -d' ' -f5"

queue=$(bash -c "$mail")

if [ $queue -gt $limit ];then

echo "Queued mails on $hostname is $queue" | mail -s "Mail Queue Alert at $hostname" $to

fi
EOL

chmod +x /root/queue.sh
(crontab -l ; echo "0 * * * *  sh /root/queue.sh") | sort - | uniq - | crontab -

########################### Policyd Installation and Configuration #######################

    ## Activate Policyd ##
su - zimbra -c 'zmprov ms $HOSTNAME +zimbraServiceInstalled cbpolicyd +zimbraServiceEnabled cbpolicyd; logout'
#zmprov ms $HOSTNAME +zimbraServiceInstalled cbpolicyd +zimbraServiceEnabled cbpolicyd

    ## Activate Policyd WebUI Run the following command as root ##
cd /opt/zimbra/httpd/htdocs/ && ln -s ../../cbpolicyd/share/webui
cp /opt/zimbra/cbpolicyd/share/webui/includes/config.php /opt/zimbra/cbpolicyd/share/webui/includes/config.php.bak
sed -i /DB_DSN/s/^/#/ /opt/zimbra/cbpolicyd/share/webui/includes/config.php
sed -i '/DB_USER/i $DB_DSN="sqlite:/opt/zimbra/data/cbpolicyd/db/cbpolicyd.sqlitedb"; ' /opt/zimbra/cbpolicyd/share/webui/includes/config.php
su - zimbra -c 'zmcontrol restart; zmapachectl restart; logout'

    ## Limit 500 email per user per hour ##

policyd_limit_emails=200         #How many emails to sent in the defined time
policyd_limit_time=3600         #time limit in seconds

cat>/root/rate-limit.sql <<EOL
BEGIN TRANSACTION;
INSERT INTO policy_groups (Name,Comment,Disabled) VALUES ('list_domains','',0);
INSERT INTO policy_group_members (PolicyGroupID,Member,Comment,Disabled) VALUES (3,'@$domain','',0);
INSERT INTO policy_group_members (PolicyGroupID,Member,Comment,Disabled) VALUES (3,'@$HOSTNAME','',0);
INSERT INTO policies (Name,Priority,Description,Disabled) VALUES ('rate limit',20,'rate limit for user',0);
INSERT INTO policy_members (PolicyID,Source,Destination,Comment,Disabled) VALUES (6,'%list_domain','any','',0);
INSERT INTO quotas (PolicyID,Name,Track,Period,Verdict,Data,LastQuota,Comment,Disabled) VALUES (6,'Rate Limit','Sender:user@domain',$policyd_limit_time,'DEFER','Sorry, your quota to sending mail in this hour has been exceeded.',0,'',0);
INSERT INTO quotas_limits (QuotasID,Type,CounterLimit,Comment,Disabled) VALUES (3,'MessageCount',$policyd_limit_emails,'',0);
COMMIT;
EOL

sqlite3 /opt/zimbra/data/cbpolicyd/db/cbpolicyd.sqlitedb </root/rate-limit.sql
rm -vf /root/rate-limit.sql

########################### Protecting Policyd Web UI #######################################
cd /opt/zimbra/cbpolicyd/share/webui/
cat>.htaccess <<EOL
AuthUserFile /opt/zimbra/cbpolicyd/share/webui/.htpasswd
AuthGroupFile /dev/null
AuthName "Username and Password"
AuthType Basic

<LIMIT GET>
require valid-user
</LIMIT>
EOL
touch .htpasswd
/opt/zimbra/httpd/bin/htpasswd -cb .htpasswd policyadmin $randpass
cp /opt/zimbra/conf/httpd.conf /opt/zimbra/conf/httpd.conf.bak
echo " ">> /opt/zimbra/conf/httpd.conf
echo "Alias /webui /opt/zimbra/cbpolicyd/share/webui/">> /opt/zimbra/conf/httpd.conf
echo "<Directory /opt/zimbra/cbpolicyd/share/webui/>">> /opt/zimbra/conf/httpd.conf
echo "# Comment out the following 3 lines to make web ui accessible from anywhere">> /opt/zimbra/conf/httpd.conf
echo "AllowOverride AuthConfig">> /opt/zimbra/conf/httpd.conf
echo "Order Deny,Allow">> /opt/zimbra/conf/httpd.conf
echo "Allow from all">> /opt/zimbra/conf/httpd.conf
echo "</Directory>">> /opt/zimbra/conf/httpd.conf

su - zimbra -c "zmapachectl restart; logout"

###################################### R1 Soft Installation #######################################
#cd /etc/yum.repos.d
#cat>r1soft.repo <<EOL
#[r1soft]
#name=R1Soft Repository Server
#baseurl=http://repo.r1soft.com/yum/stable/$basearch/
#enabled=1
#gpgcheck=0
#EOL
#yum install r1soft-cdp-enterprise-agent -y

#################################### Adding Zabbix agent ########################################

if [ "$os" = "centos" ]; then
    cd /opt
    wget http://sourceforge.net/projects/zabbix/files/ZABBIX%20Latest%20Stable/2.2.2/zabbix-2.2.2.tar.gz
    tar -zxvf zabbix-2.2.2.tar.gz
    groupadd zabbix
    useradd -g zabbix zabbix
    cd zabbix-2.2.2
    yum install -y zlib-devel glibc-devel curl-devel gcc automake libidn-devel openssl-devel net-snmp-devel rpm-devel OpenIPMI-devel make
    ./configure --enable-agent --enable-static
    ./configure --enable-agent
    make install
    cp /usr/local/etc/zabbix_agentd.conf /usr/local/etc/zabbix_agentd.conf.bak
    sed -i /Server=127.0.0.1/s/^/#/ /usr/local/etc/zabbix_agentd.conf
    sed -i '/Server=127.0.0.1/i Server=noc.mantragrid.com ' /usr/local/etc/zabbix_agentd.conf
    cp misc/init.d/fedora/core5/zabbix_agentd /etc/init.d/
    chmod 755 /etc/init.d/zabbix_agentd
    chkconfig zabbix_agentd on
    /etc/init.d/zabbix_agentd start

elif [ "$os" = "ubuntu" ]; then
    echo "Skipping Zabbix Installation"
else
    echo ""
fi

################################## Installing and Configuring Zimbra Server Statistics ###########################

#sed -i /SYSLOGD_OPTIONS/s/^/#/ /etc/sysconfig/rsyslog
#sed -i '/SYSLOGD_OPTIONS/i SYSLOGD_OPTIONS="-r -c 5" ' /etc/sysconfig/rsyslog
#service rsyslog restart
#/opt/zimbra/libexec/zmsyslogsetup
#service rsyslog restart

# Home -> Monitor -> Server Statistics

#################################### Installing Zxtras with Zchat ###############################

    ## Installing Zxtras ##
#cd /opt
#wget http://www.zextras.com/download/zextras_suite-latest.tgz
#tar zxvf zextras_suite-latest.tgz
#cd zextras_suite-2.2.5
##./install.sh all
##printf 'y\n\ny\ny\ny\n' | ./install.sh core
#printf 'y\ny\n\ny\ny\ny\n' | ./install.sh core
#su - zimbra -c 'zmprov fc -a zimlet; logout'

    ## Installing Zextras Chat ##
#su - zimbra -c 'cd /opt/zimbra/zimlets; wget https://download.zextras.com/com_zextras_chat.zip; zmzimletctl deploy com_zextras_chat.zip; logout'
#cd /opt/zimbra/zimlets
#wget https://download.zextras.com/com_zextras_chat.zip
#zmzimletctl deploy com_zextras_chat.zip
#logout

######################### Stopping Spamming by restricting not using another From address #####################

    ## Stop user to relay emails using a different email address ##
#su - zimbra -c 'zmprov mcf zimbraMtaSmtpdSenderLoginMaps proxy:ldap:/opt/zimbra/conf/ldap-slm.cf +zimbraMtaSmtpdSenderRestrictions reject_authenticated_sender_login_mismatch; logout'
#zmprov mcf zimbraMtaSmtpdSenderLoginMaps proxy:ldap:/opt/zimbra/conf/ldap-slm.cf +zimbraMtaSmtpdSenderRestrictions reject_authenticated_sender_login_mismatch
#logout

    ## This configuration will not accept if user not use authentication/password ##
#sed -i /permit_mynetworks/s/^/#/ /opt/zimbra/conf/zmconfigd/smtpd_sender_restrictions.cf
#sed -i '/permit_mynetworks/i permit_mynetworks, reject_sender_login_mismatch ' /opt/zimbra/conf/zmconfigd/smtpd_sender_restrictions.cf
#su - zimbra -c 'zmcontrol restart; logout'

################################### Daily user email report ################################

su - zimbra -c 'zmlocalconfig -e zimbra_mtareport_max_recipients=50; zmlocalconfig -e zimbra_mtareport_max_senders=50; zmcontrol restart; logout'
#zmlocalconfig -e zimbra_mtareport_max_recipients=50
#zmlocalconfig -e zimbra_mtareport_max_senders=50
#zmcontrol restart
#logout

emailDetails=tech@mantraventures.com

adminpass=`</dev/urandom tr -dc _A-Z-a-z-0-9 | head -c8`
touch /opt/customlog/adminpass
chmod 777 /opt/customlog/adminpass
echo $adminpass> /opt/customlog/adminpass

su - zimbra -c 'adminpassword=$(cat /opt/customlog/adminpass); zmprov sp admin@$HOSTNAME $adminpassword; zmconfigdctl restart; logout'

echo "Zimbra has been installed. Please open URL https://$ip:7071  to view Admin Panel with login details admin/$adminpass"
echo "Policyd is configured. You can login at http://$ip:7780/webui/index.php with login details policyadmin/$randpass"

if [ "$os" = "ubuntu" ]; then
cat <<EOF | /opt/zimbra/postfix/sbin/sendmail -t
To: $emailDetails
Subject: New Zimbra Server $HOSTNAME ($ip) Login Details

    Zimbra and Policyd web ui login details

    Zimbra Login Details
    URL: https://$ip:7071
    Username: admin
    Password: $adminpass

    Policyd web ui login details
    URL: http://$ip:7780/webui/index.php
    Username: policyadmin
    Password: $randpass
EOF
elif [ "$os" = "centos" ]; then
    echo -e "To: $emailDetails\r\nSubject: New Zimbra Server $HOSTNAME ($ip) Login Details\r\n\nZimbra and Policyd web ui login details \n\n Zimbra Login Details \n URL: https://$ip:7071 \n Username: admin \n Password: $adminpass \n\n Policyd web ui login details \n URL: http://$ip:7780/webui/index.php \n Username: policyadmin \n Password: $randpass" | /opt/zimbra/postfix/sbin/sendmail -t
    #echo -e "Zimbra and Policyd web ui login details \n\n Zimbra Login Details \n URL: https://$ip:7071 \n Username: admin \n Password: $adminpass \n\n Policyd web ui login details \n URL: http://$ip:7780/webui/index.php \n Username: policyadmin \n Password: $randpass" | mail -s "New Zimbra Server $HOSTNAME ($ip) Login Details" $emailDetails
else
    echo ""
fi

#echo -e "Zimbra and Policyd web ui login details \n\n Zimbra Login Details \n URL: https://$ip:7071 \n Username: admin \n Password: $adminpass \n\n Policyd web ui login details \n URL: http://$ip:7780/webui/index.php \n Username: policyadmin \n Password: $randpass" | mail -s "New Zimbra Server $HOSTNAME ($ip) Login Details" $emailDetails

echo "Email also sent with these details at $emailDetails"

if [ "$os" = "ubuntu" ]; then
        service postfix stop 2>/dev/null
    update-rc.d postfix disable 2>/dev/null
elif [ "$os" = "centos" ]; then
        echo ""
else
        echo ""
fi
";}i:2;i:6;}i:3;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:26827;}i:4;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:26827;}}