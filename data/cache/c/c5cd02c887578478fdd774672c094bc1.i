a:56:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:10:"listu_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:0;}i:3;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:0;}i:4;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:83:" Create the fourth partition on all 4 drives (standard RAID-10 nodes have 4 drives)";}i:2;i:4;}i:5;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:87;}i:6;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:87;}i:7;a:3:{i:0;s:11:"listu_close";i:1;a:0:{}i:2;i:87;}i:8;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:87;}i:9;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:222:"
parted /dev/sda
(parted) p
(parted) mkpart primary
File system type?  [ext2]? ext3
Start? 72G (Depends on where the 3rd partition ends)                                                  
End? 1000G (Depends on drive size)
";}i:2;i:3;i:3;s:223:">
parted /dev/sda
(parted) p
(parted) mkpart primary
File system type?  [ext2]? ext3
Start? 72G (Depends on where the 3rd partition ends)                                                  
End? 1000G (Depends on drive size)
";}i:2;i:93;}i:10;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:323;}i:11;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:323;}i:12;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:42:"*For one step partition use below command
";}i:2;i:325;}i:13;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:33:"
mkpart primary ext3 58.3G 1000G ";}i:2;i:3;i:3;s:34:">
mkpart primary ext3 58.3G 1000G ";}i:2;i:372;}i:14;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:41:"
 Depends on where the 3rd partition ends";}i:2;i:413;}i:15;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:455;}i:16;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:455;}i:17;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:29:"Repeat this for all 4 drives.";}i:2;i:457;}i:18;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:486;}i:19;a:3:{i:0;s:10:"listu_open";i:1;a:0:{}i:2;i:486;}i:20;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:486;}i:21;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:486;}i:22;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:18:" Reboot the server";}i:2;i:490;}i:23;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:508;}i:24;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:508;}i:25;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:508;}i:26;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:508;}i:27;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:21:" Create RAID-10 array";}i:2;i:512;}i:28;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:533;}i:29;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:533;}i:30;a:3:{i:0;s:11:"listu_close";i:1;a:0:{}i:2;i:533;}i:31;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:533;}i:32;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:93:"
mdadm --create /dev/md3 --level=10 --raid-devices=4 /dev/sda4 /dev/sdb4 /dev/sdc4 /dev/sdd4
";}i:2;i:3;i:3;s:94:">
mdadm --create /dev/md3 --level=10 --raid-devices=4 /dev/sda4 /dev/sdb4 /dev/sdc4 /dev/sdd4
";}i:2;i:539;}i:33;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:640;}i:34;a:3:{i:0;s:10:"listu_open";i:1;a:0:{}i:2;i:640;}i:35;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:640;}i:36;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:640;}i:37;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:23:" Create Physical Volume";}i:2;i:644;}i:38;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:667;}i:39;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:667;}i:40;a:3:{i:0;s:11:"listu_close";i:1;a:0:{}i:2;i:667;}i:41;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:667;}i:42;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:19:"
pvcreate /dev/md3
";}i:2;i:3;i:3;s:20:">
pvcreate /dev/md3
";}i:2;i:673;}i:43;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:700;}i:44;a:3:{i:0;s:10:"listu_open";i:1;a:0:{}i:2;i:700;}i:45;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:700;}i:46;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:700;}i:47;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:20:" Create Volume Group";}i:2;i:704;}i:48;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:724;}i:49;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:724;}i:50;a:3:{i:0;s:11:"listu_close";i:1;a:0:{}i:2;i:724;}i:51;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:724;}i:52;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:30:"
vgcreate VolGroup00 /dev/md3
";}i:2;i:3;i:3;s:31:">
vgcreate VolGroup00 /dev/md3
";}i:2;i:730;}i:53;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:0:"";}i:2;i:768;}i:54;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:768;}i:55;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:768;}}