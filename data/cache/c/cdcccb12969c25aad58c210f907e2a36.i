a:37:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:1;}i:3;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:29:"1. Check for Ebury infection
";}i:2;i:3;}i:4;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:32;}i:5;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:"
";}i:2;i:34;}i:6;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:9:"
ipcs -m
";}i:2;i:3;i:3;s:10:">
ipcs -m
";}i:2;i:40;}i:7;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:57;}i:8;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:57;}i:9;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:146:"If the output shows one or more large segments (at least 3 megabytes) with full permissions (666), the system is most likely infected with Ebury. ";}i:2;i:59;}i:10;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:205;}i:11;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:208;}i:12;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:210;}i:13;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:210;}i:14;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:212;}i:15;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:284:"2. A replacement of the shared library 'libkeyutils' can be identified by looking at the file size. Legitimate versions of the library usually are less than 15 kilobytes in size, while the malicious ones are larger than 25 kilobytes. To check the file size, run the following command
";}i:2;i:214;}i:16;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:498;}i:17;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:61:"
find /lib* -type f -name libkeyutils.so* -exec ls -la {} \;
";}i:2;i:3;i:3;s:62:">
find /lib* -type f -name libkeyutils.so* -exec ls -la {} \;
";}i:2;i:505;}i:18;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:574;}i:19;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:577;}i:20;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:579;}i:21;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:579;}i:22;a:3:{i:0;s:14:"underline_open";i:1;a:0:{}i:2;i:581;}i:23;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:583;}i:24;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:16:"What to do now?
";}i:2;i:585;}i:25;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:601;}i:26;a:3:{i:0;s:15:"underline_close";i:1;a:0:{}i:2;i:603;}i:27;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:605;}i:28;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:606;}i:29;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:107:"
1. Replace the libkeyutils.so file that was found to be infected with a new file from non-infected system.";}i:2;i:608;}i:30;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:715;}i:31;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:715;}i:32;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:93:"2. Remove the shared memory segment associated with the infected binary with 666 permissions
";}i:2;i:717;}i:33;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:19:"
ipcrm -M <shmkey>
";}i:2;i:3;i:3;s:20:">
ipcrm -M <shmkey>
";}i:2;i:815;}i:34;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:0:"";}i:2;i:842;}i:35;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:842;}i:36;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:842;}}