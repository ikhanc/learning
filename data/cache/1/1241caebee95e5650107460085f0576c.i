a:38:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:1;}i:3;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:31:"Creating Bulk Account On Zimbra";}i:2;i:3;}i:4;a:3:{i:0;s:12:"strong_close";i:1;a:0:{}i:2;i:34;}i:5;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:36;}i:6;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:36;}i:7;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:81:"- Login via shell and create a shell script (bulkuser.sh) with following content.";}i:2;i:38;}i:8;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:119;}i:9;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:915:"   
           #!/usr/bin/perl
           # Lookup the valid COS (Class of Service) ID in the interface or like this
            my $cosid = `su - zimbra -c 'zmprov gc Default |grep zimbraId:'`;
            $cosid =~ s/zimbraId:\s*|\s*$//g;

            while (<>) {
            chomp;
               my ($email, $password, $first, $last) = split(/\,/, $_, 4);
               my ($uid, $domain) = split(/@/, $email, 2);
               print qq{ca $uid\@$domain $password\n};
               print qq{ma $uid\@$domain zimbraCOSid "$cosid"\n};
               print qq{ma $uid\@$domain givenName "$first"\n};
               print qq{ma $uid\@$domain sn "$last"\n};
               print qq{ma $uid\@$domain cn "$uid"\n};
               print qq{ma $uid\@$domain displayName "$first $last"\n};
               print qq{ma $uid\@$domain zimbraPasswordMustChange FALSE\n};
               print qq{\n};
          }
          ";}i:2;i:119;}i:10;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:119;}i:11;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:110:"- Create a csv file (userlist.csv) such that email,password,first-name,last-name written with separated comma.";}i:2;i:1076;}i:12;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1186;}i:13;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:147:"              
              e.g.    example1@domain.com,mal023nf,Firoz,Khan
                      example2@domain.com,ndk*aLp,Ganesh,Bhat         ";}i:2;i:1186;}i:14;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1186;}i:15;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:30:"- Now run the follwing command";}i:2;i:1342;}i:16;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1372;}i:17;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:260:"     
          ./bulkuser.sh userlist.csv > zmprov_commands.out 
          
      (NOTE: If there is any permission issue then just change the permission of these files (bulkuser.sh,userlist.csv and zmprov_commands.out) to 777 then execute the command)
      ";}i:2;i:1372;}i:18;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1372;}i:19;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:131:"- Now there is some content written in mprov_commands.out. Now move all these files to zimbra home directory ( you can check it by ";}i:2;i:1644;}i:20;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:1775;}i:21;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:11:"su - zimbra";}i:2;i:1776;}i:22;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:1787;}i:23;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:" then ";}i:2;i:1788;}i:24;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:1794;}i:25;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"pwd";}i:2;i:1795;}i:26;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:1798;}i:27;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:10:" command).";}i:2;i:1799;}i:28;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1809;}i:29;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1809;}i:30;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:35:"- Now login as zimbra by executing ";}i:2;i:1816;}i:31;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:1851;}i:32;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:11:"su - zimbra";}i:2;i:1852;}i:33;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:1863;}i:34;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:27:" and run following command.";}i:2;i:1864;}i:35;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1892;}i:36;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:188:"            zmprov < zmprov_commands.out
            
If successfully executed then all accounts will be created with details present in userlist.csv, just check it out.                   ";}i:2;i:1892;}i:37;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:1892;}}