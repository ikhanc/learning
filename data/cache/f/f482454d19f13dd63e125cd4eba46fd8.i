a:37:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:34:"Setup Hetzner Node for Xen(PV+HVM)";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1;}i:4;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:48:"1) Add the following lines to /etc/sysctl.conf:
";}i:2;i:53;}i:5;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:179:"
net.ipv4.ip_forward = 1
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0
net.ipv4.conf.lo.send_redirects = 0
net.ipv4.conf.xenbr0.send_redirects = 0
";}i:2;i:3;i:3;s:180:">
net.ipv4.ip_forward = 1
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0
net.ipv4.conf.lo.send_redirects = 0
net.ipv4.conf.xenbr0.send_redirects = 0
";}i:2;i:106;}i:6;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:293;}i:7;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:293;}i:8;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:53:"2) Change directory to /etc/sysconfig/network-scripts";}i:2;i:296;}i:9;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:349;}i:10;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:349;}i:11;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:59:"3) Create a new file ifcfg-xenbr0:1 and add the following:
";}i:2;i:354;}i:12;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:128:"
DEVICE = xenbr0:1
ONBOOT = yes
BOOTPROTO = static
NETMASK = <netmask of additional IP's>
IPADDR = <first IP address of subnet>
";}i:2;i:3;i:3;s:129:">
DEVICE = xenbr0:1
ONBOOT = yes
BOOTPROTO = static
NETMASK = <netmask of additional IP's>
IPADDR = <first IP address of subnet>
";}i:2;i:418;}i:13;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:554;}i:14;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:554;}i:15;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:71:"4) Edit the route-eth0 file and add all the additional IP's as follows:";}i:2;i:557;}i:16;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:628;}i:17;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:628;}i:18;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:31:"ADDRESS0=<IP address in subnet>";}i:2;i:630;}i:19;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:661;}i:20;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:661;}i:21;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:16:"NETMASK0=0.0.0.0";}i:2;i:663;}i:22;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:679;}i:23;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:679;}i:24;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:16:"GATEWAY0=0.0.0.0";}i:2;i:681;}i:25;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:697;}i:26;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:697;}i:27;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:39:"5) Add this to /etc/rc.d/rc.local file:";}i:2;i:702;}i:28;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:741;}i:29;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:741;}i:30;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:19:"/sbin/ifup xenbr0:1";}i:2;i:743;}i:31;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:762;}i:32;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:762;}i:33;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:47:"6) Comment out the BROADCAST line in eth0 file.";}i:2;i:767;}i:34;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:814;}i:35;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:815;}i:36;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:815;}}