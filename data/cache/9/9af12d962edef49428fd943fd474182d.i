a:33:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:22:"Setting Up Disk Quotas";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1;}i:4;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:197:"Disk quotas let you set the maximum amount of disk space a particular user is allowed to have on your server.  The disk space used by a user is the sum of all the file sizes owned by their user id.";}i:2;i:40;}i:5;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:237;}i:6;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:239;}i:7;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:35:"Enabling disk quotas via /etc/fstab";i:1;i:3;i:2;i:239;}i:2;i:239;}i:8;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:239;}i:9;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:239;}i:10;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:424:"
Disk quotas will only work if you have set a quota option in your /etc/fstab.

You need to have a usrquota and/or grpquota option on the file system on which you wish to use quotas.  e.g. in /etc/fstab you would have something like:
/dev/xvda1 / ext3 defaults,noatime,usrquota,grpquota 1 0
The xvda1 bit may be something like sda1, hda1 etc.  Leave that part alone in your /etc/fstab.  Just add the usrquota,grpquota part. ";}i:2;i:3;i:3;s:425:">
Disk quotas will only work if you have set a quota option in your /etc/fstab.

You need to have a usrquota and/or grpquota option on the file system on which you wish to use quotas.  e.g. in /etc/fstab you would have something like:
/dev/xvda1 / ext3 defaults,noatime,usrquota,grpquota 1 0
The xvda1 bit may be something like sda1, hda1 etc.  Leave that part alone in your /etc/fstab.  Just add the usrquota,grpquota part. ";}i:2;i:290;}i:11;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:722;}i:12;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:724;}i:13;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:22:"Setting Up Disk Quotas";i:1;i:3;i:2;i:724;}i:2;i:724;}i:14;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:724;}i:15;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:724;}i:16;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:"Then:
";}i:2;i:758;}i:17;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:684:"
reload the mount points with the quota settings enabled
mount -o remount /
#install quota (if not there alread)
apt-get install quota

will set up the quota files
quotacheck -acguvm
-m is needed, else you may get:
quotacheck: Cannot remount filesystem mounted on / read-only so counted values might not be right.
Please stop all programs writing to filesystem or use -m flag to force checking.
-c is needed, else you may get an error like
quotacheck: Can't find filesystem to check or filesystem not mounted with quota
edquota: Quota file not found or has wrong format.
quota: Quota file not found or has wrong format.
should happen on reboot (in /etc/rc.d/rc.sysinit)
quotaon -avug ";}i:2;i:3;i:3;s:685:">
reload the mount points with the quota settings enabled
mount -o remount /
#install quota (if not there alread)
apt-get install quota

will set up the quota files
quotacheck -acguvm
-m is needed, else you may get:
quotacheck: Cannot remount filesystem mounted on / read-only so counted values might not be right.
Please stop all programs writing to filesystem or use -m flag to force checking.
-c is needed, else you may get an error like
quotacheck: Can't find filesystem to check or filesystem not mounted with quota
edquota: Quota file not found or has wrong format.
quota: Quota file not found or has wrong format.
should happen on reboot (in /etc/rc.d/rc.sysinit)
quotaon -avug ";}i:2;i:769;}i:18;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:0:"";}i:2;i:1461;}i:19;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1462;}i:20;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1462;}i:21;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:17:"Using disk quotas";i:1;i:3;i:2;i:1462;}i:2;i:1462;}i:22;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:1462;}i:23;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1462;}i:24;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:87:"<code>
Now if you run:
repquota -a
It will show how much disk space each user is using.";}i:2;i:1490;}i:25;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1577;}i:26;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1577;}i:27;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:33:"To assign a quota to a user (per ";}i:2;i:1579;}i:28;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:38:"http://www.tldp.org/HOWTO/Quota-4.html";i:1;N;}i:2;i:1612;}i:29;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:30:"):
edquota -u ausername </code";}i:2;i:1650;}i:30;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1680;}i:31;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1680;}i:32;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:1680;}}