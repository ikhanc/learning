a:13:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:34:"Monitor Remote Server using Nagios";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1;}i:4;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:1321:"
Remote Server Installation and Setup
useradd nagios
passwd nagios
(Enter any password)

mkdir -p /opt/Nagios/Nagios_Plugins
cd /opt/Nagios/Nagios_Plugins

Download Nagios plugins package from http://www.nagios.org/download/download.php
tar -xzf nagios-plugins-xxx
cd nagios-plugins-xxx

yum -y install openssl-devel
./configure - -with-nagios-user=nagios --with-nagios-group=nagios (Remove space between hyphens)
make
make install

chown nagios.nagios /usr/local/nagios
chown -R nagios.nagios /usr/local/nagios/libexec

yum install xinetd

mkdir -p /opt/Nagios/Nagios_NRPE
cd /opt/Nagios/Nagios_NRPE

Download NRPE package from http://www.nagios.org/download/download.php

tar -xzf nrpe-xxx
cd nrpe-xxx

yum -y install openssl-devel
./configure

General Options:
-----
NRPE port: 5666
NRPE user: nagios
NRPE group: nagios
Nagios user: nagios
Nagios group: nagios

make all
make install-plugin
make install-daemon
make install-daemon-config
make install-xinetd

vi /etc/xinetd.d/nrpe

Change this line:
only_from = 127.0.0.1 <nagios_ip_address>

vi /etc/services

Add this line:
nrpe 5666/tcp # NRPE

chkconfig xinetd on
service xinetd restart

NOTE: If you get this error:

gcc: /usr/lib/mysql/libmysqlclient.so: No such file or directory

Use command:

ln -s /usr/lib/libmysqlclient.so /usr/lib/mysql/libmysqlclient.so
";}i:2;i:3;i:3;s:1322:">
Remote Server Installation and Setup
useradd nagios
passwd nagios
(Enter any password)

mkdir -p /opt/Nagios/Nagios_Plugins
cd /opt/Nagios/Nagios_Plugins

Download Nagios plugins package from http://www.nagios.org/download/download.php
tar -xzf nagios-plugins-xxx
cd nagios-plugins-xxx

yum -y install openssl-devel
./configure - -with-nagios-user=nagios --with-nagios-group=nagios (Remove space between hyphens)
make
make install

chown nagios.nagios /usr/local/nagios
chown -R nagios.nagios /usr/local/nagios/libexec

yum install xinetd

mkdir -p /opt/Nagios/Nagios_NRPE
cd /opt/Nagios/Nagios_NRPE

Download NRPE package from http://www.nagios.org/download/download.php

tar -xzf nrpe-xxx
cd nrpe-xxx

yum -y install openssl-devel
./configure

General Options:
-----
NRPE port: 5666
NRPE user: nagios
NRPE group: nagios
Nagios user: nagios
Nagios group: nagios

make all
make install-plugin
make install-daemon
make install-daemon-config
make install-xinetd

vi /etc/xinetd.d/nrpe

Change this line:
only_from = 127.0.0.1 <nagios_ip_address>

vi /etc/services

Add this line:
nrpe 5666/tcp # NRPE

chkconfig xinetd on
service xinetd restart

NOTE: If you get this error:

gcc: /usr/lib/mysql/libmysqlclient.so: No such file or directory

Use command:

ln -s /usr/lib/libmysqlclient.so /usr/lib/mysql/libmysqlclient.so
";}i:2;i:56;}i:5;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1385;}i:6;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1385;}i:7;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:30:"MONITORING HOST SERVER SETUP:
";}i:2;i:1389;}i:8;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:2138:"
If NRPE not already installed:
mkdir -p /opt/Nagios/Nagios_NRPE
cd /opt/Nagios/Nagios_NRPE

Download NRPE package from http://www.nagios.org/download/download.php

tar -xzf nrpe-xxx
cd nrpe-xxx
./configure
make all
make install-plugin

/usr/local/nagios/libexec/check_nrpe -H <IP of Remote Server>
NRPE v2.12

vi /usr/local/nagios/etc/objects/commands.cfg
Then add the following:

define command{
command_name check_nrpe
command_line $USER1$/check_nrpe -H $HOSTADDRESS$ -c $ARG1$
}

vi /usr/local/nagios/etc/objects/linux-box-remote.cfg

Add the following and replace the values “host_name” “alias” “address” with the values that match your setup:

define host{
name linux-box-remote ; Name of this template
use generic-host ; Inherit default values
check_period 24x7
check_interval 5
retry_interval 1
max_check_attempts 10
check_command check-host-alive
notification_period 24x7
notification_interval 30
notification_options d,r
contact_groups admins
register 0 ; DONT REGISTER THIS - ITS A TEMPLATE
}

define host{
use linux-box-remote ; Inherit default values from a template
host_name Centos5 ; The name we're giving to this server
alias Centos5 ; A longer name for the server
address 192.168.0.5 ; IP address of the server
}

define service{
use generic-service
host_name Centos5
service_description CPU Load
check_command check_nrpe!check_load
}
define service{
use generic-service
host_name Centos5
service_description Current Users
check_command check_nrpe!check_users
}
define service{
use generic-service
host_name Centos5
service_description /dev/hda1 Free Space
check_command check_nrpe!check_hda1
}
define service{
use generic-service
host_name Centos5
service_description Total Processes
check_command check_nrpe!check_total_procs
}
define service{
use generic-service
host_name Centos5
service_description Zombie Processes
check_command check_nrpe!check_zombie_procs
}

vi /usr/local/nagios/etc/nagios.cfg
Add the following:
cfg_file=/usr/local/nagios/etc/objects/linux-box-remote.cfg

/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg
Total Warnings: 0
Total Errors: 0

service nagios restart  ";}i:2;i:3;i:3;s:2139:">
If NRPE not already installed:
mkdir -p /opt/Nagios/Nagios_NRPE
cd /opt/Nagios/Nagios_NRPE

Download NRPE package from http://www.nagios.org/download/download.php

tar -xzf nrpe-xxx
cd nrpe-xxx
./configure
make all
make install-plugin

/usr/local/nagios/libexec/check_nrpe -H <IP of Remote Server>
NRPE v2.12

vi /usr/local/nagios/etc/objects/commands.cfg
Then add the following:

define command{
command_name check_nrpe
command_line $USER1$/check_nrpe -H $HOSTADDRESS$ -c $ARG1$
}

vi /usr/local/nagios/etc/objects/linux-box-remote.cfg

Add the following and replace the values “host_name” “alias” “address” with the values that match your setup:

define host{
name linux-box-remote ; Name of this template
use generic-host ; Inherit default values
check_period 24x7
check_interval 5
retry_interval 1
max_check_attempts 10
check_command check-host-alive
notification_period 24x7
notification_interval 30
notification_options d,r
contact_groups admins
register 0 ; DONT REGISTER THIS - ITS A TEMPLATE
}

define host{
use linux-box-remote ; Inherit default values from a template
host_name Centos5 ; The name we're giving to this server
alias Centos5 ; A longer name for the server
address 192.168.0.5 ; IP address of the server
}

define service{
use generic-service
host_name Centos5
service_description CPU Load
check_command check_nrpe!check_load
}
define service{
use generic-service
host_name Centos5
service_description Current Users
check_command check_nrpe!check_users
}
define service{
use generic-service
host_name Centos5
service_description /dev/hda1 Free Space
check_command check_nrpe!check_hda1
}
define service{
use generic-service
host_name Centos5
service_description Total Processes
check_command check_nrpe!check_total_procs
}
define service{
use generic-service
host_name Centos5
service_description Zombie Processes
check_command check_nrpe!check_zombie_procs
}

vi /usr/local/nagios/etc/nagios.cfg
Add the following:
cfg_file=/usr/local/nagios/etc/objects/linux-box-remote.cfg

/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg
Total Warnings: 0
Total Errors: 0

service nagios restart  ";}i:2;i:1424;}i:9;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:0:"";}i:2;i:3570;}i:10;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3570;}i:11;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:3570;}i:12;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:3570;}}