a:146:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:27:"Setup Nagios for monitoring";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1;}i:4;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:24:"Monitoring Server Steps:";}i:2;i:46;}i:5;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:70;}i:6;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:70;}i:7;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:102:"1) Use yum to install these packages by running the following commands (as root):yum install httpd php";}i:2;i:72;}i:8;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:174;}i:9;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:35:"
yum install gcc glibc glibc-common";}i:2;i:176;}i:10;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:211;}i:11;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:24:"
yum install gd gd-devel";}i:2;i:213;}i:12;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:237;}i:13;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:239;}i:14;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:239;}i:15;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:59:"2) Create a new nagios user account and give it a password.";}i:2;i:241;}i:16;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:300;}i:17;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:15:"
useradd nagios";}i:2;i:302;}i:18;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:317;}i:19;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:15:"
passwd nagios ";}i:2;i:319;}i:20;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:334;}i:21;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:336;}i:22;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:336;}i:23;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:163:"3) Create a new naggroup group for allowing external commands to be submitted through the web interface. Add both the nagios user and the apache user to the group.";}i:2;i:338;}i:24;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:501;}i:25;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:18:"
groupadd naggroup";}i:2;i:503;}i:26;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:521;}i:27;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:30:"
usermod -a -G naggroup nagios";}i:2;i:523;}i:28;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:553;}i:29;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:30:"
usermod -a -G naggroup apache";}i:2;i:555;}i:30;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:585;}i:31;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:587;}i:32;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:587;}i:33;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:76:"4) Download the source code tarballs of both Nagios and the Nagios plugins (";}i:2;i:589;}i:34;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:31:"http://www.nagios.org/download/";i:1;N;}i:2;i:665;}i:35;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:21:" for latest versions)";}i:2;i:696;}i:36;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:717;}i:37;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:719;}i:38;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:719;}i:39;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"wget ";}i:2;i:721;}i:40;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:73:"http://prdownloads.sourceforge.net/sourceforge/nagios/nagios-3.3.1.tar.gz";i:1;N;}i:2;i:726;}i:41;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:799;}i:42;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:800;}i:43;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:"
wget ";}i:2;i:802;}i:44;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:86:"http://prdownloads.sourceforge.net/sourceforge/nagiosplug/nagios-plugins-1.4.15.tar.gz";i:1;N;}i:2;i:808;}i:45;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:894;}i:46;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:895;}i:47;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:897;}i:48;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:897;}i:49;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:43:"5) Extract the Nagios source code tarball: ";}i:2;i:899;}i:50;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:942;}i:51;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:944;}i:52;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:944;}i:53;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:30:"tar -xvzf nagios-3.3.1.tar.gz ";}i:2;i:946;}i:54;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:976;}i:55;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:11:"
cd nagios ";}i:2;i:978;}i:56;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:989;}i:57;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:991;}i:58;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:991;}i:59;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:87:"6) Run the Nagios configure script, passing the name of the group you created earlier: ";}i:2;i:993;}i:60;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:1080;}i:61;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1082;}i:62;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1082;}i:63;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"./configure ";}i:2;i:1084;}i:64;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"--";}i:2;i:1096;}i:65;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:27:"with-command-group=naggroup";}i:2;i:1098;}i:66;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1125;}i:67;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1125;}i:68;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:35:"7) Compile the Nagios source code.
";}i:2;i:1127;}i:69;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:108:"
make all
make install

make install-init

make install-config
make install-commandmodemake install-webconf ";}i:2;i:3;i:3;s:109:">
make all
make install

make install-init

make install-config
make install-commandmodemake install-webconf ";}i:2;i:1167;}i:70;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1283;}i:71;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1283;}i:72;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:54:"8) Edit the /usr/local/nagios/etc/objects/contacts.cfg";}i:2;i:1285;}i:73;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1339;}i:74;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1339;}i:75;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:130:"Change the email address associated with the nagiosadmin contact definition to the address you'd like to use for receiving alerts.";}i:2;i:1341;}i:76;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1471;}i:77;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1471;}i:78;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:74:"9) Create a nagiosadmin account for logging into the Nagios web interface.";}i:2;i:1473;}i:79;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1547;}i:80;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1547;}i:81;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:79:"useradd nagiosadminhtpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin";}i:2;i:1549;}i:82;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1628;}i:83;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1628;}i:84;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:78:"10) Restart Apache to make the new settings take effect.
service httpd restart";}i:2;i:1630;}i:85;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1708;}i:86;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1708;}i:87;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:51:"11) Extract the Nagios plugins source code tarball.";}i:2;i:1710;}i:88;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:1761;}i:89;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:40:"
tar -xvzf nagios-plugins-1.4.15.tar.gz ";}i:2;i:1763;}i:90;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:1803;}i:91;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:25:"
cd nagios-plugins-1.4.15";}i:2;i:1805;}i:92;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1830;}i:93;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1830;}i:94;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:37:"12) Compile and install the plugins. ";}i:2;i:1832;}i:95;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:1869;}i:96;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:13:"
./configure ";}i:2;i:1871;}i:97;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"--";}i:2;i:1884;}i:98;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:24:"with-nagios-user=nagios ";}i:2;i:1886;}i:99;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"--";}i:2;i:1910;}i:100;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:25:"with-nagios-group=nagios ";}i:2;i:1912;}i:101;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1937;}i:102;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1937;}i:103;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:37:"If there are no errors, start Nagios.";}i:2;i:1939;}i:104;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1976;}i:105;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1976;}i:106;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:20:"service nagios start";}i:2;i:1978;}i:107;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1998;}i:108;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1998;}i:109;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:23:"13) Download NRPE from ";}i:2;i:2000;}i:110;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:64:"http://sourceforge.net/projects/nagios/files/nrpe-2.x/nrpe-2.12/";i:1;N;}i:2;i:2023;}i:111;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2087;}i:112;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2087;}i:113;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:26:"tar -xvzf nrpe-2.12.tar.gz";}i:2;i:2089;}i:114;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2115;}i:115;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2115;}i:116;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"cd nrpe-2.12";}i:2;i:2117;}i:117;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2129;}i:118;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2129;}i:119;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:28:"yum -y install openssl-devel";}i:2;i:2131;}i:120;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2159;}i:121;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2159;}i:122;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:42:"
./configure
make all
make install-plugin ";}i:2;i:3;i:3;s:43:">
./configure
make all
make install-plugin ";}i:2;i:2166;}i:123;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2216;}i:124;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2216;}i:125;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:34:"Test connection to remote server: ";}i:2;i:2218;}i:126;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:2252;}i:127;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:73:"
/usr/local/nagios/libexec/check_nrpe -H <IP of Remote Server>
NRPE v2.12";}i:2;i:2254;}i:128;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2327;}i:129;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2327;}i:130;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:101:"14) Create NRPE command definition:
vi /usr/local/nagios/etc/objects/commands.cfg
Add the following:
";}i:2;i:2329;}i:131;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:105:"
define command {
command_name check_nrpe
command_line $USER1$/check_nrpe -H $HOSTADDRESS$ -c $ARG1$
} 

";}i:2;i:3;i:3;s:106:">
define command {
command_name check_nrpe
command_line $USER1$/check_nrpe -H $HOSTADDRESS$ -c $ARG1$
} 

";}i:2;i:2435;}i:132;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2548;}i:133;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2548;}i:134;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:209:"15) Create Linux Object Template
vi /usr/local/nagios/etc/objects/linux-box-remote.cfg
Add the following and replace the values “host_name” “alias” “address” with the values that match your setup:
";}i:2;i:2550;}i:135;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:1252:"
define host {
name linux-box-remote ; Name of this template
use generic-host ; Inherit default values
check_period 24x7
check_interval 5
retry_interval 1
max_check_attempts 10
check_command check-host-alive
notification_period 24x7
notification_interval 30
notification_options d,r
contact_groups admins
register 0 ; DONT REGISTER THIS - ITS A TEMPLATE
}   

define host {
use linux-box-remote ; Inherit default values from a template
host_name Centos5 ; The name we're giving to this server
alias Centos5 ; A longer name for the server
address 192.168.0.5 ; IP address of the server
}

define service {
use generic-service
host_name Centos5
service_description CPU Load
check_command check_nrpe!check_load
}
define service {

use generic-service

host_name Centos5

service_description Current Users

check_command check_nrpe!check_users

}

define service {
use generic-service
host_name Centos5
service_description /dev/hda1 Free Space
check_command check_nrpe!check_hda1
}
define service {
use generic-service
host_name Centos5
service_description Total Processes
check_command check_nrpe!check_total_procs
}
define service {
use generic-service
host_name Centos5
service_description Zombie Processes
check_command check_nrpe!check_zombie_procs
} ";}i:2;i:3;i:3;s:1253:">
define host {
name linux-box-remote ; Name of this template
use generic-host ; Inherit default values
check_period 24x7
check_interval 5
retry_interval 1
max_check_attempts 10
check_command check-host-alive
notification_period 24x7
notification_interval 30
notification_options d,r
contact_groups admins
register 0 ; DONT REGISTER THIS - ITS A TEMPLATE
}   

define host {
use linux-box-remote ; Inherit default values from a template
host_name Centos5 ; The name we're giving to this server
alias Centos5 ; A longer name for the server
address 192.168.0.5 ; IP address of the server
}

define service {
use generic-service
host_name Centos5
service_description CPU Load
check_command check_nrpe!check_load
}
define service {

use generic-service

host_name Centos5

service_description Current Users

check_command check_nrpe!check_users

}

define service {
use generic-service
host_name Centos5
service_description /dev/hda1 Free Space
check_command check_nrpe!check_hda1
}
define service {
use generic-service
host_name Centos5
service_description Total Processes
check_command check_nrpe!check_total_procs
}
define service {
use generic-service
host_name Centos5
service_description Zombie Processes
check_command check_nrpe!check_zombie_procs
} ";}i:2;i:2764;}i:136;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4024;}i:137;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4024;}i:138;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:49:"16) Activate the linux-box-remote.cfg template: 
";}i:2;i:4026;}i:139;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:245:"
vi /usr/local/nagios/etc/nagios.cfg
And add:

cfg_file=/usr/local/nagios/etc/objects/linux-box-remote.cfg

Verify Nagios Configuration Files:

/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg

Total Warnings: 0

Total Errors: 0
";}i:2;i:3;i:3;s:246:">
vi /usr/local/nagios/etc/nagios.cfg
And add:

cfg_file=/usr/local/nagios/etc/objects/linux-box-remote.cfg

Verify Nagios Configuration Files:

/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg

Total Warnings: 0

Total Errors: 0
";}i:2;i:4080;}i:140;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:4:"
17)";}i:2;i:4333;}i:141;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:1425:"
Restart Nagios:
service nagios restart

Remote Linux Machine Steps:

1) Create user nagios:

useradd nagios

passwd nagios

2)  Download plugins and extract:

wget http://prdownloads.sourceforge.net/sourceforge/nagiosplug/nagios-plugins-1.4.15.tar.gz

tar -xvzf nagios-plugins-1.4.15.tar.gz

cd nagios-plugins-1.4.15

yum -y install openssl-devel

3) Install Plugins:./configure --with-nagios-user=nagios --with-nagios-group=nagios
make
make install

4) The permissions on the plugin directory and the plugins will need to be changed to nagios user:
chown nagios.nagios /usr/local/nagios
chown -R nagios.nagios /usr/local/nagios/libexec
yum install xinetd
5) Download NRPE from http://sourceforge.net/projects/nagios/files/nrpe-2.x/nrpe-2.12/
tar -xvzf nrpe-2.12.tar.gz
cd nrpe-2.12

6) Install and configure:
./configure
make all
make install-plugin
make install-daemon
make install-daemon-config
make install-xinetd

7) Add Nagios Monitoring server to the “only_from” directive:
vi /etc/xinetd.d/nrpe
only_from = 127.0.0.1 <nagios_ip_address>

8) Edit services file entry:
vi /etc/services
nrpe 5666/tcp # NRPE

9) Restart Xinetd and Set to start at boot:
chkconfig xinetd on
service xinetd restart

10) Check NRPE daemon is running and listening on port 5666:
netstat -at |grep nrpe
Output should be:
tcp 0 0 *:nrpe  *.* LISTEN

11) Check NRPE daemon is functioning:
/usr/local/nagios/libexec/check_nrpe -H localhost ";}i:2;i:3;i:3;s:1426:">
Restart Nagios:
service nagios restart

Remote Linux Machine Steps:

1) Create user nagios:

useradd nagios

passwd nagios

2)  Download plugins and extract:

wget http://prdownloads.sourceforge.net/sourceforge/nagiosplug/nagios-plugins-1.4.15.tar.gz

tar -xvzf nagios-plugins-1.4.15.tar.gz

cd nagios-plugins-1.4.15

yum -y install openssl-devel

3) Install Plugins:./configure --with-nagios-user=nagios --with-nagios-group=nagios
make
make install

4) The permissions on the plugin directory and the plugins will need to be changed to nagios user:
chown nagios.nagios /usr/local/nagios
chown -R nagios.nagios /usr/local/nagios/libexec
yum install xinetd
5) Download NRPE from http://sourceforge.net/projects/nagios/files/nrpe-2.x/nrpe-2.12/
tar -xvzf nrpe-2.12.tar.gz
cd nrpe-2.12

6) Install and configure:
./configure
make all
make install-plugin
make install-daemon
make install-daemon-config
make install-xinetd

7) Add Nagios Monitoring server to the “only_from” directive:
vi /etc/xinetd.d/nrpe
only_from = 127.0.0.1 <nagios_ip_address>

8) Edit services file entry:
vi /etc/services
nrpe 5666/tcp # NRPE

9) Restart Xinetd and Set to start at boot:
chkconfig xinetd on
service xinetd restart

10) Check NRPE daemon is running and listening on port 5666:
netstat -at |grep nrpe
Output should be:
tcp 0 0 *:nrpe  *.* LISTEN

11) Check NRPE daemon is functioning:
/usr/local/nagios/libexec/check_nrpe -H localhost ";}i:2;i:4342;}i:142;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:0:"";}i:2;i:5775;}i:143;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5775;}i:144;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:5775;}i:145;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:5775;}}