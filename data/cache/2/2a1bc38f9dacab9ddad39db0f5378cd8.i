a:98:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:67:"[What is TOR and how to install it on ubuntu 14.04 LTS trusty tahr]";}i:2;i:1;}i:3;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:68;}i:4;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:68;}i:5;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:164:"The TOR (third-generation onion routing) browser is a web browser 
designed for encryption-protected anonymous web surfing and protection 
against traffic analysis.";}i:2;i:72;}i:6;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:236;}i:7;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:236;}i:8;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:386:"TOR was originally developed by and for the United States Navy 
to protect sensitive U.S. government communications. While Tor
continues to be used by government it is now an open source,
multi-platform browser that is available to the public. TOR 
is used by reporters, activists, whistle blowers, law enforcement 
officials, business professionals and security-conscious individuals. ";}i:2;i:238;}i:9;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:624;}i:10;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:624;}i:11;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:332:"Although TOR enhances web browsing privacy, the makers caution that 
users should take further measures to protect their privacy and security.
TOR uses exit relays and encrypted tunnels to hide user traffic within the
network, but leaves the end points more easily observable and has no effect 
beyond the boundaries of the network.";}i:2;i:626;}i:12;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:958;}i:13;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:958;}i:14;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:334:"Although TOR is more secure than most commonly-used browsers, it is not
impervious to attack. Malware, such as the Chewbacca Trojan, has 
successfully targeted the TOR network and browser. The FBI has also 
breached TOR security, in one case, for example tracking threats associated 
with a bomb hoax at Harvard across the network.   ";}i:2;i:960;}i:15;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1294;}i:16;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1294;}i:17;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:43:"Let jump in how to install this cool stuff.";}i:2;i:1297;}i:18;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1340;}i:19;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1340;}i:20;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:22:"Install from Terminal:";}i:2;i:1343;}i:21;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1365;}i:22;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1365;}i:23;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:183:"Note: If you installed older version of Tor-Browser, 
in first you must clear the old .tor-browser-en folder 
from home folder. Just clear that by using: cd ~ && rm -r .tor-browser-en";}i:2;i:1367;}i:24;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1550;}i:25;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1550;}i:26;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:86:"Then type following line one by one in Terminal ( Ctrl+Alt+T ) to install Tor-Browser.";}i:2;i:1552;}i:27;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1638;}i:28;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1638;}i:29;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:107:" sudo add-apt-repository ppa:webupd8team/tor-browser
 sudo apt-get update
 sudo apt-get install tor-browser";}i:2;i:1640;}i:30;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1747;}i:31;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1747;}i:32;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:23:"Now open dash and type ";}i:2;i:1749;}i:33;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:1772;}i:34;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"Tor";}i:2;i:1773;}i:35;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:1776;}i:36;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:19:", click on and use.";}i:2;i:1777;}i:37;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1796;}i:38;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1796;}i:39;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:17:"Have fun and USE.";}i:2;i:1799;}i:40;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1816;}i:41;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1816;}i:42;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:32:"WHAT IS ANONYMOUS WEB SURFING!!!";}i:2;i:1821;}i:43;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1853;}i:44;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1853;}i:45;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:138:"Anonymous Web surfing allows a user to visit Web sites 
without allowing anyone to gather information about which 
sites the user visited.";}i:2;i:1856;}i:46;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1994;}i:47;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1994;}i:48;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:390:"Anonymous Web surfing allows a user to visit Web sites 
without allowing anyone to gather information about 
which sites the user visited. Services that provide 
anonymity disable pop-up windows and cookies and conceal
the visitor's IP address. These services typically use 
a proxy server to process each HTTP request. When the user
requests a Web page by clicking a hyperlink or typing a ";}i:2;i:1996;}i:49;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:3:"URL";}i:2;i:2386;}i:50;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:456:"
into their browser, the service retrieves and displays the 
information using its own server. The remote server (where
the requested Web page resides) receives information about
the anonymous Web surfing service in place of the user's
information. Anonymous Web surfing is popular for two reasons: 
to protect the user's privacy and/or to bypass blocking applications 
that would prevent access to Web sites or parts of sites that the
user wants to visit.";}i:2;i:2389;}i:51;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2845;}i:52;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2845;}i:53;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:480:"An anonymous surfing service can make a user feel more secure
on the Internet, but it doesn't permit a site to often the 
visitor personalization. This means that the a site cannot 
tailor its content or advertising to suit the individual user. 
SafeWeb and the Anonymizer are the most commonly used such services.
Lucent's Bell Labs created its own version, called Lucent Personalized 
Web Assistant (LPWA), as did the Naval Research Labs, whose project was
called Onion Routing.";}i:2;i:2847;}i:54;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3327;}i:55;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3327;}i:56;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:19:"1. Use Tor browsers";}i:2;i:3330;}i:57;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3349;}i:58;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3349;}i:59;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:287:"Just because it is the anonymous network, that doesn’t 
mean that Tor will protect your system completely. It 
protects only the applications and programs that are 
configured correctly. That is why you should use a Tor 
browser bundle, which is pre-programmed to suit the Tor network.";}i:2;i:3351;}i:60;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3638;}i:61;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3638;}i:62;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:33:"2. Say goodbye to browser plugins";}i:2;i:3640;}i:63;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3673;}i:64;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3673;}i:65;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:419:"You would notice that the Tor browser disables things like 
Quicktime, RealPlayer and Flash. This it does in order to 
protect your privacy, as these applications have been known 
to give out your IP address. Similarly, you should avoid 
installing any add-ons to the Tor browser because they may 
cause it to malfunction. This can in turn be a hindrance to the 
browser’s original purpose of protecting your privacy.";}i:2;i:3675;}i:66;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4094;}i:67;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4094;}i:68;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:53:"3. Avoid opening downloaded files when working on Tor";}i:2;i:4096;}i:69;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4149;}i:70;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4149;}i:71;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:116:"Opening PDF or other files using a different application 
can reveal your non-IP address. You should avoid doing so.";}i:2;i:4151;}i:72;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4267;}i:73;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4267;}i:74;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:38:"4. Tor and Torrent don’t go together";}i:2;i:4269;}i:75;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4307;}i:76;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4307;}i:77;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:145:"Even though it may seem so, downloading torrents while 
on Tor is not a good idea. You can use the network but 
don’t try to download torrents.";}i:2;i:4309;}i:78;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4454;}i:79;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4454;}i:80;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:8:"5. HTTPS";}i:2;i:4456;}i:81;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4464;}i:82;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4464;}i:83;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:159:"Yes, use this at all times while you are on the Tor network. 
It shouldn’t be any trouble, since the network automatically 
goes for the always HTTPS option.";}i:2;i:4466;}i:84;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4625;}i:85;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4625;}i:86;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:14:"6. Tor bridges";}i:2;i:4627;}i:87;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4641;}i:88;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4641;}i:89;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:127:"This is also important since Tor doesn’t protect you from 
being watched. Someone can still find out that you’re using Tor.";}i:2;i:4643;}i:90;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4770;}i:91;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4770;}i:92;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:18:"7. Get more people";}i:2;i:4772;}i:93;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4790;}i:94;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4790;}i:95;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:99:"The more people near you who use Tor, better will be the 
protection that you get from the network.";}i:2;i:4792;}i:96;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4891;}i:97;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:4891;}}