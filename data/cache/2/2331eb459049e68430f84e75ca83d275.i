a:20:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:23:"OpenVZ monitoring Tools";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1;}i:4;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:454:"The standard linux task list shows you each process and the resources (e.g. CPU, Memory) that they are consuming. However, if you run top on an OpenVZ host server, it doesn't show you the container ID of each process. Fortunately, there is a tool imaginatively named, vztop, which can show you a slightly more useful list of container and host processes, along with their container ID. To install the vztop and vzps tools, just run the following command:";}i:2;i:41;}i:5;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:495;}i:6;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:495;}i:7;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"rpm -ivh ";}i:2;i:497;}i:8;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:77:"http://download.openvz.org/contrib/utils/vzprocps-2.0.11-6.13.swsoft.i386.rpm";i:1;N;}i:2;i:506;}i:9;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:583;}i:10;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:583;}i:11;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:107:"Once the utilities are installed, you can get a list of all processes running and their container ids with:";}i:2;i:585;}i:12;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:692;}i:13;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:692;}i:14;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"vztop -E -1
";}i:2;i:694;}i:15;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:9:"code_code";i:1;a:4:{i:0;s:4:"code";i:1;N;i:2;s:0:"";i:3;s:3122:"
This will return a list of tasks like the following output:

 13:07:09  up 3 days, 22:02,  3 users,  load average: 0.26, 0.67, 2.41
358 processes: 357 sleeping, 1 running, 0 zombie, 0 stopped
CPU0 states:   0.4% user   0.3% system    0.0% nice   0.0% iowait  98.3% idle
CPU1 states:   0.1% user   0.2% system    0.0% nice   0.1% iowait  99.1% idle
CPU2 states:   0.1% user   0.3% system    0.0% nice   0.4% iowait  98.2% idle
CPU3 states:   1.0% user   0.3% system    0.0% nice   0.0% iowait  98.1% idle
Mem:  4150972k av, 2404600k used, 1746372k free,       0k shrd,  161888k buff
      1269832k active,             489584k inactive
Swap: 2040244k av,       0k used, 2040244k free                  992664k cached

  PID USER     PRI  NI  SIZE  RSS SHARE  VEID STAT %CPU %MEM   TIME CPU COMMAND
  375 root      15   0  5360 1832   780     0 S     0.9  0.0   1:09   2 iptraf
31693 root      16   0  2456 1188   792     0 S     0.5  0.0   0:10   0 top
12866 root      18   0  4728 1416   824     0 R     0.3  0.0   0:00   3 vztop
 7042 zabbix    22   5  7164  484   280   N/A S N   0.1  0.0  10:33   0 zabbix_agentd
11014 root      15   0 10148 3036  2208   209 S     0.1  0.0   0:02   2 sshd
11015 root      15   0  9980 2864  2208   209 S     0.1  0.0   0:02   1 sshd
11019 root      18   0  6792 1884  1144   209 S     0.1  0.0   0:01   3 sftp-server
31333 root      16   0 10008 2828  2284     0 S     0.1  0.0   0:00   1 sshd
    1 root      15   0  2064  628   540   N/A S     0.0  0.0   0:14   1 init
    2 root      RT  -5     0    0     0     0 SW<   0.0  0.0   0:00   0 migration/0/0
    3 root      39  19     0    0     0     0 SWN   0.0  0.0   0:00   0 ksoftirqd/0
    4 root      RT  -5     0    0     0     0 SW<   0.0  0.0   0:00   1 migration/0/1
    5 root      34  19     0    0     0     0 SWN   0.0  0.0   0:00   1 ksoftirqd/1
    6 root      RT  -5     0    0     0     0 SW<   0.0  0.0   0:00   2 migration/0/2
    7 root      34  19     0    0     0     0 SWN   0.0  0.0   0:00   2 ksoftirqd/2
    8 root      RT  -5     0    0     0     0 SW<   0.0  0.0   0:00   3 migration/0/3
    9 root      34  19     0    0     0     0 SWN   0.0  0.0   0:00   3 ksoftirqd/3
   10 root      10  -5     0    0     0     0 SW<   0.0  0.0   0:00   0 events/0
   11 root      10  -5     0    0     0     0 SW<   0.0  0.0   0:00   1 events/1
   12 root      10  -5     0    0     0     0 SW<   0.0  0.0   0:00   2 events/2
   13 root      12  -5     0    0     0     0 SW<   0.0  0.0   0:00   3 events/3
   14 root      10  -5     0    0     0     0 SW<   0.0  0.0   0:03   3 khelper
   15 root      10  -5     0    0     0     0 SW<   0.0  0.0   0:00   0 kthread
   21 root      10  -5     0    0     0     0 SW<   0.0  0.0   0:00   0 kblockd/0
   22 root      10  -5     0    0     0     0 SW<   0.0  0.0   0:00   1 kblockd/1
   23 root      10  -5     0    0     0     0 SW<   0.0  0.0   0:00   2 kblockd/2
Notes

VEID - this is the Container id. A container may also be known as a VE or a VPS.
You can view the processes on a specific container by providing the container id instead of -1 in the command above. ";}i:2;i:3;i:3;s:3123:">
This will return a list of tasks like the following output:

 13:07:09  up 3 days, 22:02,  3 users,  load average: 0.26, 0.67, 2.41
358 processes: 357 sleeping, 1 running, 0 zombie, 0 stopped
CPU0 states:   0.4% user   0.3% system    0.0% nice   0.0% iowait  98.3% idle
CPU1 states:   0.1% user   0.2% system    0.0% nice   0.1% iowait  99.1% idle
CPU2 states:   0.1% user   0.3% system    0.0% nice   0.4% iowait  98.2% idle
CPU3 states:   1.0% user   0.3% system    0.0% nice   0.0% iowait  98.1% idle
Mem:  4150972k av, 2404600k used, 1746372k free,       0k shrd,  161888k buff
      1269832k active,             489584k inactive
Swap: 2040244k av,       0k used, 2040244k free                  992664k cached

  PID USER     PRI  NI  SIZE  RSS SHARE  VEID STAT %CPU %MEM   TIME CPU COMMAND
  375 root      15   0  5360 1832   780     0 S     0.9  0.0   1:09   2 iptraf
31693 root      16   0  2456 1188   792     0 S     0.5  0.0   0:10   0 top
12866 root      18   0  4728 1416   824     0 R     0.3  0.0   0:00   3 vztop
 7042 zabbix    22   5  7164  484   280   N/A S N   0.1  0.0  10:33   0 zabbix_agentd
11014 root      15   0 10148 3036  2208   209 S     0.1  0.0   0:02   2 sshd
11015 root      15   0  9980 2864  2208   209 S     0.1  0.0   0:02   1 sshd
11019 root      18   0  6792 1884  1144   209 S     0.1  0.0   0:01   3 sftp-server
31333 root      16   0 10008 2828  2284     0 S     0.1  0.0   0:00   1 sshd
    1 root      15   0  2064  628   540   N/A S     0.0  0.0   0:14   1 init
    2 root      RT  -5     0    0     0     0 SW<   0.0  0.0   0:00   0 migration/0/0
    3 root      39  19     0    0     0     0 SWN   0.0  0.0   0:00   0 ksoftirqd/0
    4 root      RT  -5     0    0     0     0 SW<   0.0  0.0   0:00   1 migration/0/1
    5 root      34  19     0    0     0     0 SWN   0.0  0.0   0:00   1 ksoftirqd/1
    6 root      RT  -5     0    0     0     0 SW<   0.0  0.0   0:00   2 migration/0/2
    7 root      34  19     0    0     0     0 SWN   0.0  0.0   0:00   2 ksoftirqd/2
    8 root      RT  -5     0    0     0     0 SW<   0.0  0.0   0:00   3 migration/0/3
    9 root      34  19     0    0     0     0 SWN   0.0  0.0   0:00   3 ksoftirqd/3
   10 root      10  -5     0    0     0     0 SW<   0.0  0.0   0:00   0 events/0
   11 root      10  -5     0    0     0     0 SW<   0.0  0.0   0:00   1 events/1
   12 root      10  -5     0    0     0     0 SW<   0.0  0.0   0:00   2 events/2
   13 root      12  -5     0    0     0     0 SW<   0.0  0.0   0:00   3 events/3
   14 root      10  -5     0    0     0     0 SW<   0.0  0.0   0:03   3 khelper
   15 root      10  -5     0    0     0     0 SW<   0.0  0.0   0:00   0 kthread
   21 root      10  -5     0    0     0     0 SW<   0.0  0.0   0:00   0 kblockd/0
   22 root      10  -5     0    0     0     0 SW<   0.0  0.0   0:00   1 kblockd/1
   23 root      10  -5     0    0     0     0 SW<   0.0  0.0   0:00   2 kblockd/2
Notes

VEID - this is the Container id. A container may also be known as a VE or a VPS.
You can view the processes on a specific container by providing the container id instead of -1 in the command above. ";}i:2;i:711;}i:16;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:0:"";}i:2;i:3841;}i:17;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3841;}i:18;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:3841;}i:19;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:3841;}}