**To install PHP 5.4 from the Software Collections repository, perform the following steps:**


1. Log into your server over SSH as root

2. Setup the SCL repository with this command: yum install centos-release-SCL

3. Install PHP 5.4 and scl-utils with this command: yum install php54 php54-php-cli php54-php-mysql php54-php-gd


**Configuring the second PHP Version in Virtualmin**

Once you have completed the installation of a second PHP version, you can verify that Virtualmin sees it by 

logging into Virtualmin, and clicking System Settings -> Re-Check Config. You should see something like this:

The following PHP versions are available : 5.3.3 (/usr/bin/php-cgi), 5.4.16 (/opt/rh/php54/root/usr/bin/php-cgi)

You can configure which one is the default PHP version used on new Virtual Servers. The default is to use the newest available. You can change that default in System Settings -> Server Templates -> Default -> Apache Website, and on that screen you can set the default PHP version to use in the field Default PHP version.



**//How to install php-mcrypt with PHP 5.4 in this setup"//**


wget http://rpms.southbridge.ru/rhel6/php-5.4/x86_64/php-mcrypt-5.4.16-1.el6.x86_64.rpm\\
rpm2cpio php-mcrypt-5.4.16-1.el6.x86_64.rpm | cpio -idmv

Copy the mcrypt.so file created:

cp /usr/lib64/php/modules/mcrypt.so /opt/rh/php54/root/usr/lib64/php/modules/

Add extension=mcrypt.so in php.ini file


If php page get downloaded then do below.


vi /etc/httpd/conf.d/php54-php.conf


comment below


#<FilesMatch \.php$>


#    SetHandler application/x-httpd-php


#</FilesMatch>


and


AddHandler php5-script .php



AddType text/html .php

then restart httpd service.

For PHP 5.5 and PHP 5.6 follow below link.



https://www.mojowill.com/geek/howto-install-php-5-4-5-5-or-5-6-on-centos-6-and-centos-7/