====== Install squirrelmail on webmin server ======


Login as root in the server and do the following:

$ yum install squirrelmail

Restart httpd(service httpd restart)

NOTE: If you installed SquirrelMail onto a Debian server then you may need to add this alias line to your apache conf file(/etc/httpd/conf/httpd.conf):

Alias /webmail/ "/usr/share/squirrelmail/"

Then, goto http://IP/webmail/index.php

Done!