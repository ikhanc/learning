

**1. Set Kill and Tag Percent.**

i. Login to zimbra admin panel.

ii. Navigate to Home→Configure→Global Settings→AS/AV

iii. Set Kill Percent: 50 and Tag Percent: 25

(These values can be reduced intend to strict SPAM filter)

**2. Rejecting false "mail from" addresses (mail spoofing), follow below article**

[[https://wiki.zimbra.com/wiki/Rejecting_false_%22mail_from%22_addresses|https://wiki.zimbra.com/wiki/Rejecting_false_%22mail_from%22_addresses]]

**Note: For versions 8.5 and above if method not worked said in above article then follow instructions for 8.0.x but add below line in /opt/zimbra/conf/zmconfigd/smtpd_sender_restrictions.cf instead of given one **
<code>
 check_sender_access lmdb:/opt/zimbra/conf/domainrestrict
</code>

**3. Enable SpamAssassin rule updates via sa-update. **

Check that these are set to true, and if not, set them to true and restart amavisd and the MTA:

<code>
  zmlocalconfig antispam_enable_rule_updates
  zmlocalconfig antispam_enable_restarts
</code>

If above two are showing "false" then run below commands to enable.

<code>
  zmlocalconfig -e antispam_enable_rule_updates=true
  zmlocalconfig -e antispam_enable_restarts=true
  zmamavisdctl restart
  zmmtactl restart
</code>

**4. Enable RBL and DNS checks. ** Run below commands to enable RBL and required DNS checks.
<code>
   zmprov mcf +zimbraMtaRestriction "reject_invalid_hostname"
   zmprov mcf +zimbraMtaRestriction "reject_non_fqdn_hostname"
   zmprov mcf +zimbraMtaRestriction "reject_non_fqdn_sender"
   zmprov mcf +zimbraMtaRestriction "reject_unknown_sender_domain"
   zmprov mcf +zimbraMtaRestriction "reject_rbl_client bl.spamcop.net"
   zmprov mcf +zimbraMtaRestriction "reject_rbl_client sbl.spamhaus.org"
</code>

Then restart zimbra services. To check configured restrictions run below commands.

<code>
   zmprov gcf zimbraMtaRestriction
</code>

You can add below RBL's also if required.

<code>
 dnsbl.sorbs.net
 b.barracudacentral.org
</code>

**NOTE: After enabling RBLs do following.**

i. Open /opt/zimbra/postfix-2.10.1.2z/conf/main.cf (postfix version could be different).

ii. For "smtpd_recipient_restrictions" make "permit_sasl_authenticated" prior to RBLs check. e.g.

<code>
  smtpd_recipient_restrictions = reject_non_fqdn_recipient, check_sender_access hash:/opt/zimbra/postfix/conf/restricted_senders, permit_sasl_authenticated, permit_mynetworks, reject_unlisted_recipient, reject_non_fqdn_sender, reject_unknown_sender_domain, reject_rbl_client bl.spamcop.net, reject_rbl_client sbl.spamhaus.org, permit
</code>

iii. restart zimbra service.

**5. Increase SPAM Fail score.**

i. Open /opt/zimbra/conf/spamassassin/50_scores.cf (location would be change for this file).

<code>
  For Zimbra 8.6 file location is>> /opt/zimbra/data/spamassassin/rules/50_scores.cf
</code>

ii. Change it's permission to 644 and search for "SPF_FAIL", now change values as below (increase value by 2 for SPF fail).

<code>
  ifplugin Mail::SpamAssassin::Plugin::SPF
  score SPF_NONE 2
  score SPF_HELO_NONE 0
  score SPF_PASS -0.001
  score SPF_HELO_PASS -0.001
  # <gen:mutable>
  score SPF_FAIL 2 2.919 2 2.001 # n=0 n=2
  score SPF_HELO_FAIL 2 2.001 2 2.001 # n=0 n=2
  score SPF_HELO_NEUTRAL 2 2.001 2 2.112 # n=0 n=2
  score SPF_HELO_SOFTFAIL 2 2.896 2 2.732 # n=0 n=2
  score SPF_NEUTRAL 2 2.652 2 2.779 # n=0 n=2
  score SPF_SOFTFAIL 2 2.972 2 2.665 # n=0 n=2
  # </gen:mutable>
  endif # Mail::SpamAssassin::Plugin::SPF
</code>

iii. Few more changes required to be done in 50_scores.cf file.

<code>
score BAYES_00  0  0 -0.001 -0.001
score URIBL_BLOCKED 0 2.001 0 2.001
score FSL_HELO_NON_FQDN_1 2.361 2.001 1.783 2.001
</code>

iv. Now revert the file permission to back 444 and restart zimbra services.

**6. Check if auto spam training running or not using below command as zimbra user.**
<code>
   zmtrainsa --cleanup
</code>

Useful links.

[[https://wiki.zimbra.com/wiki/Anti-spam_Strategies|https://wiki.zimbra.com/wiki/Anti-spam_Strategies]]

[[https://wiki.zimbra.com/wiki/Improving_Anti-spam_system|https://wiki.zimbra.com/wiki/Improving_Anti-spam_system]]

