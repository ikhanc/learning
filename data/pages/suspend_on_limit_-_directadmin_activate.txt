====== Suspend on limit - DirectAdmin ACTIVATE ======


The option you will place in your directadmin.conf is:

disk_usage_suspend=1

The default will not be in the file, but will be set to 0 internally.