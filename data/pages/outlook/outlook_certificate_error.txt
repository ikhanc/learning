If you’re using Outlook and getting the the following error message when you try to collect your email, read below for a couple of possible solutions to the problem:


“The server you are connected to is using a security certificate that cannot be verified. The target principal name is incorrect”"The target principle name is incorrect”"Do you want to continue using this server?”

The cause of the problem
This problem is caused when the POP3 address (email collection) doesn’t match the SSL certificate that is being used.

The solution to the problem

  - Open Outlook
  - Click File --> Account Setting--> 
  - Highlight the email account you are having trouble with and press ‘Change’
  - From the ‘Change e-mail account’ page click on ‘More settings’
  - From the ‘Internet email settings’ page choose the ‘advanced’ tab
  - Un-tick the ‘This server requires authentication’ option
  - click ‘OK’, then ‘next’ then ‘Finish’

Image attached for reference of step no. 6.

