Introduction to Virtual Hosts

Source : http://www.wip.rackspacecloud.com/knowledge_center/node/89

 

The term Virtual Hosts is used a great deal when setting up your Cloud Server. Let's take a look at what Virtual Hosts are and why they are important.

It is also worth noting that the term Virtual Hosts is used with all web servers. Once you have the principle clear in your mind, it makes setting them up on any web server much easier.

 ==== Contents ====
 

  -  Process
  -  Single IP address
  -  Web Server
  -  Virtual Hosts
  -  Is that it?
  -  What if I enter the IP address?

==== 1. Process ====

Let's take a quick look at what happens when you type a web address into your browser.

This is not a technical document so forgive the simplicity but the basics are quite simple:

  * Your browser sends an HTTP request to your ISP.
  * The request is routed, via your ISP and other servers, to where the IP address is located (i.e. your Cloud Server).
  * The Cloud Server web server processes the HTTP request and returns the HTML representation of the request to your browser.
  * Your browser interprets the HTML representation and parses the page for you.
  * You click 'next' and the whole process is repeated.

==== Single IP address ====

In this simple outline there is an important issue to note in that the web address you type into the browser is turned into an IP address.  Behind the scenes the more "human-friendly" domain name is turned into a series of numbers, like "123.45.67.89".  Your browser uses that to find the site you want to visit on the Internet.

So if my Cloud Server only has one IP address can I only host one domain?

Nope. When your browser visits a site it still tells it what the domain name was that it looked up to get there.  This is where virtual hosts come in...

You can allocate many domains to one IP address.

So lets say you have three domains: domain1.com, domain2.com and domain3.com and all of them have been configured to point to your Cloud Server's IP address.

Using exactly the same (simplified) routing shown above, when any of your 3 domains are entered into a browser, the requests end up at your Cloud Server.


==== Web Server ====

Enter the Web Server. It doesn't really matter what Web Server we are talking about here, it could be Apache, Litespeed, Nginx, lighttpd or any of the dozens of Web Servers available.

The point is that the request for 3 different domains has arrived at a single point: Your Cloud Server and your Web Server.


==== Virtual Hosts ====

You see Web Servers are quite cool.

They are able to look at those requests and say "Hey, this guy wants an HTML representation of domain1.com and look, this guy wants one of domain2.com".

The server knows which file to parse and what to send back to the browser because you defined different settings and file locations for each domain in your Virtual Hosts file.

Each web server differs slightly in how to set up the Virtual Hosts file but they all have the same base settings:

Virtual Host Name: The name of the domain being requested - e.g. domain3.com.

Domain Directory: The location of the files for the requested domain.

 

==== Is that it? ====

Pretty much.

Of course, there are loads of settings you can customise for each Virtual Host but the basics remain the same: Receive a request for a domain, locate the requested domain's directory and, depending on what was requested, serve the requested files.

 

==== What if I enter the IP address? ====

Ah. Now you've hit on the weak spot of name based Virtual Hosts: If all the domains have the same IP address, what happens when I enter the IP address?

There is usually a 'default' domain. You may not have expressly set it up that way, but if the IP address is entered it is 'usual' for the first domain (alphabetically) to be served.

As an example, in the Debian Etch Apache install, the default Virtual Host is named '000-default'. So if you have not expressly defined a Virtual Host for the IP address, then 000-default will be used.

It is worth saying that different web servers do act differently here. The main point is that, in this example, there is only one IP address so only one domain can be served.

However, it is relatively rare for people to browse