<code>mysqlcheck --auto-repair --check --optimize --all-databases</code>

It will repair and optimize your Mysql databases. Then restart MySQL after you've done the above:

<code>/etc/init.d/mysql restart</code>