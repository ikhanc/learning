Requirements

A registered domain name with valid A DNS records to point back to server public IP Address.
Nginx web server installed with SSL enabled and Virtual Hosts enabled (only for multiple domains or subdomains hosting).

If Nginx is already running, stop the daemon with the following command and run ss utility to confirm that port 80 is no longer in use in network stack.


#Stop nginx
<code>service nginx stop</code>

#Check if port 80 is no longer used.
<code>ss -tln</code>

Install git (If not already installed)
<code>yum install git</code>

#Clone lets encrypt.
<code>cd /opt
git clone https://github.com/letsencrypt/letsencrypt</code>

#Change directory to lets encrypt.
<code>cd letsencrypt/ 

./letsencrypt-auto certonly --standalone -d your_domain.tld -d www.yourdomain.tld</code>


Append Below Parameters in your virtualhost file(/etc/nginx/conf.d/defaults.conf) or in fastcgi_params file (/etc/nginx/fastcgi_params). Make sure to include them in your nginx virtual host file.

<code>include        fastcgi_params;</code>

<code>
fastcgi_param   HTTPS               on;
fastcgi_param   HTTP_SCHEME         https;
</code>


Add SSL conf nginx excerpt:
<code>
# SSL configuration
listen 443 ssl default_server;
ssl_certificate /etc/letsencrypt/live/your_domain.tld/fullchain.pem;
ssl_certificate_key /etc/letsencrypt/live/your_domain.tld/privkey.pem;
ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
ssl_prefer_server_ciphers on;
ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';
</code>

Restart nginx
<code>
service nginx restart
</code>

<code>

if ($scheme = http) {
       return 301 https://$server_name$request_uri;
      }
      
      </code>