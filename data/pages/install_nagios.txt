====== Install Nagios ======


Login as root in the Host server and type the following commands:
<code>
yum install httpd gcc glibc glibc-common gd gd-devel php

useradd -m nagios
groupadd nagcmd
usermod -a -G nagcmd nagios
usermod -a -G nagcmd apache

mkdir /opt/Nagios
cd /opt/Nagios
Now, download the Nagios package from http://www.nagios.org/download/download.php
tar xzf nagios-xxx (version of nagios)
cd nagios-xxx

./configure --with-command-group=nagcmd
make all
make install
make install-init
make install-config
make install-commandmode

make install-webconf
htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin
(Remember ths password you give here!!)

service httpd restart

http://wiki.mantraventures.com/tiki-editpage.php?page=Install+Nagios&hdr=1

INSTALL NAGIOS PLUGINS:
cd /opt/Nagios
Download plugins package from http://www.nagios.org/download/download.php
tar xzf nagios-plugins-xxx
cd nagios-plugins-xxx

./configure - -with-nagios-user=nagios --with-nagios-group=nagios (Remove space between hyphens)
make
make install
vi /usr/local/nagios/etc/objects/contacts.cfg

Change the email address to yours.

/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg
Total Warnings: 0
Total Errors: 0

chkconfig --add nagios
chkconfig nagios on
chkconfig httpd on
service nagios start

Login to Nagios from: http://ip-address/nagios/ </code>