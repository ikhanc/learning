====== GRUB re-installation ======


<code>
rpm -e grub

wget ftp://ftp.pbone.net/mirror/archive.fedoraproject.org/fedora/linux/releases/10/Everything/x86_64/os/Packages/grub-0.97-38.fc10.x86_64.rpm

rpm -i grub-0.97-38.fc10.x86_64.rpm

cp /usr/share/grub/x86_64-redhat/* /boot/grub

grub

> root (hd0,0)
root (hd0,0)

> setup (hd0)
setup (hd0)

</code>