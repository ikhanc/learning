===== Soft ether VPN client setup on Ubuntu =====

----

Soft ether VPN client setup on Ubuntu

Follow the undermentioned procedure to setup VPN client on ubuntu :

1. Download the soft ether VPN client. (Run the commands as root user)

Download URL : [[http://www.softether-download.com/files/softether/v4.20-9608-rtm-2016.04.17-tree/Linux/SoftEther_VPN_Client/64bit_-_Intel_x64_or_AMD64/softether-vpnclient-v4.20-9608-rtm-2016.04.17-linux-x64-64bit.tar.gz|http://www.softether-download.com/files/softether/v4.20-9608-rtm-2016.04.17-tree/Linux/SoftEther_VPN_Client/64bit_-_Intel_x64_or_AMD64/softether-vpnclient-v4.20-9608-rtm-2016.04.17-linux-x64-64bit.tar.gz]]

2. Move the downloaded file to /root : mv softether-vpnclient-v4.20-9608-rtm-2016.04.17-linux-x64-64bit.tar.gz /root

3. Follow the below procedure :

tar -xf softether-vpnclient-v4.20-9608-rtm-2016.04.17-linux-x64-64bit.tar.gz

cd vpnclient

make (press 1 ; "Agree", every time it prompts for user input )

cd && mv vpnclient /usr/local

cd /usr/local/vpnclient

chmod 644 * && find . -type d -exec chmod 755 {} \; && chmod +x vpnc*

4. Create a init script file /etc/init.d/vpnclient and paste below content in it.

//#!/bin/sh//

//### BEGIN INIT INFO\\
# Provides: vpnclient\\
# Required-Start: $network $remote_fs $syslog\\
# Required-Stop: $network $remote_fs $syslog\\
# Default-Start: 2 3 4 5\\
# Default-Stop: 0 1 6\\
# Short-Description: Soft Ether VPN client\\
# Description: Soft Ether VPN client daemon\\
### END INIT INFO//

//DAEMON=/usr/local/vpnclient/vpnclient\\
LOCK=/var/lock/subsys/vpnclient//

//test -x $DAEMON || exit 0\\
case "$1" in//

//start)\\
$DAEMON start\\
if [ "$?" -eq "0" ]; then touch $LOCK ; fi\\
;;\\
stop)\\
$DAEMON stop\\
rm $LOCK\\
;;\\
restart)\\
$DAEMON stop\\
if [ "$?" -eq "0" ]; then rm "$LOCK" ; fi\\
sleep 1\\
$DAEMON start\\
if [ "$?" -eq "0" ]; then touch "$LOCK" ; fi\\
;;\\
status)\\
if [ -f "$LOCK" ]; then\\
echo "vpnclient [ running ]";\\
else\\
echo "vpnclient [ stopped ]";\\
fi\\
;;\\
*)\\
echo "Usage: $0 {start|stop|restart|status}"\\
exit 1\\
esac\\
exit 0//

5. Set execution permission on the init script and start the vpnclient process.

chmod +x /etc/init.d/vpnclient

service vpnclient start

update-rc.d vpnclient defaults

6. To configure the vpn client follow below procedure :

cd /usr/lcoal/vpnclient

./vpncmd

VPN Client> Select/Press 2 & then press enter (to enter into Management of VPN Client)

VPN Client> Press enter as input to Hostname of IP Address of Destination: (connection will be made to localhost to operate VPN client from your computer.)

VPN Client> NicCreate ethervpn

VPN Client> exit

7. Install wine and winetricks.

IMAGE [[http://wiki.mantraventures.com/_media/softether/selection_052.png|http://wiki.mantraventures.com/_media/softether/selection_052.png]]

8. Download the windows vpn client manager and install it using wine.

Download link : [[http://www.softether-download.com/files/softether/v4.20-9608-rtm-2016.04.17-tree/Windows/SoftEther_VPN_Client/softether-vpnclient-v4.20-9608-rtm-2016.04.17-windows-x86_x64-intel.exe|http://www.softether-download.com/files/softether/v4.20-9608-rtm-2016.04.17-tree/Windows/SoftEther_VPN_Client/softether-vpnclient-v4.20-9608-rtm-2016.04.17-windows-x86_x64-intel.exe]]

(Install wine with the default installtion option only, simply press NEXT only.)

9. Run the vpn client via the icon on your desktop using wine. Then click on to create Virtual Adapter & enter "ethervpn" in popup window.

Note : First time this aplication will give you the error, then close the application and restart it. Next time a virtual adapter is already detected by application which we have create in step 6.

10. Then Click on Add VPN connection. Then fill the required details in the blanks highlighted in the image below :

IMAGE http://wiki.mantraventures.com/_media/softether/selection_054.png

11. After adding a vpn connection, right click on the connection name and click on connect.

IMAGE

12. After it gets connected, return to shell and run the below commands as root :

dhclient vpn_ethervpn

ip route add 146.88.26.95 via <YOUR IP> dev <Wireless NIC name> proto static

ip route del default

ip route add default via 192.168.30.1 dev vpn_ethervpn

13. Add nameservers in your /etc/resolve.conf

nameserver 8.8.8.8

nameserver 8.8.4.4
