====== Delete attachments of mailman and free disk usage ======


1) Login to shell of the server.

2) Change directory:

cd /usr/local/cpanel/3rdparty/mailman/archives/private/

3) Then goto the directory of the mailman list of which you want to delete the attachments.

4) Delete the contents of the folder attachments which is inside the mailman list directory

rm -rf attachments/*