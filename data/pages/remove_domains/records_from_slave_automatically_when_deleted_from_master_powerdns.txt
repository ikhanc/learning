====== Remove domains/records from slave automatically when deleted from Master (PowerDNS) ======


1) Login to shell of slave.

2) Create a new file pdns_clean.php with the content as given in the attachment (Pdns_slave_delete.txt)

Edit the details in the initial part of file to the one's of the slave. Keep testing mode as enabled to see if it works well.

3) When confirmed about its working, make the testing mode as disabled.

4) Now, add the script to cron jobs and let it be executed once a day.