====== Copy Incoming or Outgoing mails to another ID ======


For cPanel 11, you need to edit the file /etc/cpanel_exim_system_filter.

 For older versions, the file is /etc/antivirus.exim.

Add the following to send a BCC to another email-ID:

==== For Incoming mails: ====

<code>
if first_delivery
and ( ("$h_from:" contains "test1@test.com")
or ("$h_from:" contains "test2@test.com")
or ("$h_from:" contains "test3@test.com")
)
then
unseen deliver "tracker@test.com"
endif  </code>

==== For Outgoing mails: ====

<code>
if first_delivery
and ( ("$h_to:" contains "test1@test.com")
or ("$h_to:" contains "test2@test.com")
or ("$h_to:" contains "test3@test.com")
)
then
unseen deliver "tracker@test.com"
endif </code>