Requirement 1: Install and maintain a firewall configuration to protect data

1.1.4 (a) Is a firewall required and implemented at each Internet connection and between any demilitarized zone (DMZ) and the internal network zone? A software firewall (using iptables) is installed and maintained on server.

1.1.4 (b) Is the current network diagram consistent with the firewall configuration standards? We maintain and monitor firewalls for all eCommerce clients.

1.1.6 (a) Do firewall and router configuration standards include a documented list of services, protocols, and ports, including business justification (for example, hypertext transfer protocol (HTTP), Secure Sockets Layer (SSL), Secure Shell (SSH), and Virtual Private Network (VPN) protocols)? Yes, only required protocols/ports are open in firewall.

1.1.6 (b) Are all insecure services, protocols, and ports identified, and are security features documented and implemented for each identified service? All other protocols/ports are blocked by default.

1.2.1 (a) Is inbound and outbound traffic restricted to that which is necessary for the cardholder data environment? Yes.

1.2.1 (b) Is all other inbound and outbound traffic specifically denied (for example by using an explicit “deny all” or an implicit deny after allow statement)? Deny all for all ports not in use.

1.3.4 Are anti-spoofing measures implemented to detect and block forged sourced IP addresses from entering the network? Internal addresses cannot pass from the internet into the DMZ.

1.3.5 Is outbound traffic from the cardholder data environment to the Internet explicitly authorized? No.

1.3.6 Is stateful inspection, also known as dynamic packet filtering, implemented—that is, only established connections are allowed into the network? Currently access for HTTP and FTP protocols are allowed for all IP addresses. We can limit this to only limited IP address or ranges if required.

1.3.8 (a) Are methods in place to prevent the disclosure of private IP addresses and routing information to the Internet? Yes.

1.3.8 (b) Is any disclosure of private IP addresses and routing information to external entities authorized? Yes.

Requirement 6: Develop and maintain secure systems and applications

6.5.2 Do coding techniques address buffer overflow vulnerabilities? This is something code/Magento/application related.

6.6 For public-facing web applications, are new threats and vulnerabilities addressed on an ongoing basis, and are these applications protected against known attacks by applying either of the following methods? We inform on any new security patches for Magento. We get server side patches done when required from time to time.

Requirement 8: Identify and authenticate access to system components

8.1.6 (a Are repeated access attempts limited by locking out the user ID after no more than six attempts? Yes.

8.1.7 Once a user account is locked out, is the lockout duration set to a minimum of 30 minutes or until an administrator enables the user ID? Yes.

8.3 Is two-factor authentication incorporated for remote network access originating from outside the network by personnel (including users and administrators) and all third parties (including vendor access for support or maintenance)? No.
