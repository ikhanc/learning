====== configure rsync on windows ======


Windows backup on Linux (RSYNC)
<code>
Install cygwin on Winserver
Select packages-
Editor-nano
Net-rsync

Add C:\cygwin\bin; to the windows PATH statement
MyComputer Properties->Advance->Environmental Variables->PATH

Start Cygwin
nano /secret
enter password
chmod 600 /secret
chown Administrator:SYSTEM /secret

Create a BAT file
C:\Cygwin\bin\rsync.exe -vrtz password-file=c:\cygwin\secret delete /cygdrive/d/Data root@IP::rootbackup
save as C:\rsync.bat

Add to task scheduler

--------
In LINUX machine

sudo nano /etc/rsyncd.conf
update following:

rootbackup
path = /root/rootbackup
comment=Backup
uid=root
gid=root
read only=false
auth users=root
secrets file=/etc/rsyncd.secrets
hosts allow=*, localhost, 127.0.0.1, 184.154.216.62
log file=/var/log/rsyncd.log
transfer logging=yes
use chroot=false

test
path = /cygdrive/c/data
read only = false
transfer logging = yes

files
path = /cygdrive/v
read only = false
transfer logging = yes

sudo chmod 644 /etc/rsyncd.conf

Create a file named rsyncd.secrets in /etc
sudo nano /etc/rsyncd.secrets
add following- username:password

sudo chmod 600 /etc/rsyncd.secrets

RSYNC_ENABLE=true

rsync --daemon </code>