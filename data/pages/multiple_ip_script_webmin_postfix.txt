====== Multiple IP script (Webmin+Postfix) ======



1) Install Webmin on a VPS with CentOS installed!

2) Install required components:

yum install rpm-build gcc db4-devel openldap-devel pcre-devel postgresql-devel cyrus-sasl-devel ed make

**Download the package**

wget http://ftp.wl0.org/official/2.9/SRPMS/postfix-2.9.1-1.src.rpm

**Execute the following commands**

<code>
rpm -Uvh postfix-2.9.1-1.src.rpm
cd `rpm --eval '%{_sourcedir}'`
export POSTFIX_LDAP=1
export POSTFIX_MYSQL_REDHAT=1
export POSTFIX_PCRE=1
export POSTFIX_PGSQL=1
export POSTFIX_RBL_MAPS=1
export POSTFIX_SASL=2
export POSTFIX_TLS=1
export POSTFIX_VDA=1
export POSTFIX_DB=4
</code> 
 

 

<code>
$ sh `rpm --eval '%{_sourcedir}'`/make-postfix.spec
$ cd `rpm --eval '%{_specdir}'`
$ vi postfix.spec
</code>
 

Search for CCARGS variable (line 376)

Default value is empty.

modify to this:

 

<code>
CCARGS="-DSNAPSHOT" </code>
 

Also Make following changes in postfix.spec
Replace all rhel5 to rhel6
Comment out lines with greeting.patch
#Patch99: ftp://ftp.wl0.org/SOURCES/postfix-2.8.3-multiline-greeting.patch
#%patch99  -p1 -b .multiline
Comment out lines with patch5 OR Patch5
#%patch5 -p1 -b .notls
#Patch5: postfix-makedefs.patch
Comment out lines for Patch4
#Patch4:  http://vda.sourceforge.net/VDA/postfix-%{V_vda}-vda-ng.patch.gz
#%if %{with_vda}

#  test -e  %{_sourcedir}/postfix-%{V_vda}-vda-ng.patch.gz && \

#  gzip -dc %{_sourcedir}/postfix-%{V_vda}-vda-ng.patch.gz | patch -p1 -b --suffix .vda -s || \

#    die "POSTFIX RPM: %{_sourcedir}/postfix-%{V_vda}-vda.patch.gz not found or patch failed"

#%endif
 

**After Changes Execute Following**

<code>
$ rpmbuild -ba postfix.spec
$ rpm -Uvh `rpm --eval'%{_rpmdir}'`../x86_64/postfix-...rpm
</code>

Now test if tcp is compiled with postconf -m

If successfully installed there will be tcp in the output of command


<code>
$ postconf -m
btree
cdb
cidr
environ
hash
ldap
mysql
nis
pcre
pgsql
proxy
regexp
static
tcp
unix
</code>

**Now Implement this script:**

 

<code>
# cd /etc/postfix
# vi random.pl </code>

<code>
#!/usr/bin/perl -w
# author: Hari Hendaryanto <hari.h -at- csmcom.com>
 
use strict;
use warnings;
use Sys::Syslog qw(:DEFAULT setlogsock);
 
#
# our transports array, we will define this in master.cf as transport services
#
 
our @array = (
'rotate1:',
'rotate2:',
'rotate3:',
'rotate4:',
'rotate5:'
);
 
#
# Initalize and open syslog.
#
openlog('postfix/randomizer','pid','mail');
 
#
# Autoflush standard output.
#
select STDOUT; $|++;
 
while (<>) {
        chomp;
        # randomizing transports array
        my $random_smtp = int(rand(scalar(@array)));
        if (/^get\s(.+)$/i) {
                print "200 $array[$random_smtp]\n";
                syslog("info","Using: %s Transport Service", $random_smtp);
                next;
        }
 
    print "200 smtp:";
}
</code>

Make it executable

<code>
# chmod 755 random.pl </code>
 

master.cf parts

Run the scripts via postfix spawn daemon service.


<code>
127.0.0.1:2527 inet  n       n       n       -       0      spawn
          user=nobody argv=/etc/postfix/random.pl </code>
 

add 5 smtp client services called rotate1, rotate2, rotate3, rotate4, rotate5, that bind to its own ip
address and has uniq syslog/helo name.


<code>
# random smtp
rotate1  unix -       -       n       -       -       smtp
          -o syslog_name=postfix-rotate1
          -o smtp_helo_name=smtp1.example.com
          -o smtp_bind_address=1.2.3.1
 
rotate2  unix -       -       n       -       -       smtp
          -o syslog_name=postfix-rotate2
          -o smtp_helo_name=smtp2.example.com
          -o smtp_bind_address=1.2.3.2
 
rotate3  unix -       -       n       -       -       smtp
          -o syslog_name=postfix-rotate3
          -o smtp_helo_name=smtp3.example.com
          -o smtp_bind_address=1.2.3.3
 
rotate4  unix -       -       n       -       -       smtp
          -o syslog_name=postfix-rotate4
          -o smtp_helo_name=smtp4.example.com
          -o smtp_bind_address=1.2.3.4
 
rotate5  unix -       -       n       -       -       smtp
          -o syslog_name=postfix-rotate5
          -o smtp_helo_name=smtp5.example.com
          -o smtp_bind_address=1.2.3.5 </code>
 

Reload postfix
<code>
# postfix reload </code> \\
Run this query fiew times, and you’ll see the perl script will return “random answer” transport


<code>
# postmap -q "whatever" tcp:127.0.0.1:2527
rotate1: </code>
 

 

<code>
# postmap -q "whatever" tcp:127.0.0.1:2527
rotate5: </code>
 

In **main.cf** parts add these lines


<code>
transport_maps = tcp:[127.0.0.1]:2527
127.0.0.1:2527_time_limit = 3600s
 </code>

Reload postfix

 

Original Scripts are following

http://www.kutukupret.com/2009/11/17/centos-5-compile-postfix-with-tcp-table-support/

Then

http://www.kutukupret.com/2010/12/06/postfix-randomizing-outgoing-ip-using-tcp_table-and-perl/

 