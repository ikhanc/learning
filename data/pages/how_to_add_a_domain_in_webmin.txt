====== How to add a domain in Webmin ======


1. Login to your webmin panel.

2. Click on BIND DNS Server under Servers.

3. In the BIND DNS Server, click on "Create Master Zone":

» Domain name / Network (this is the domain name you want to host)

» Email address (this is the webmaster's email address)

» IP address for template records (enter your server's ip address)

4. Click on the "Address" icon:

» Name (Enter www)
» Address (Enter your server's IP)
» CLICK "Create"

5. At the top menu you will find a link that says 'Apply Configuration' click on it to apply the changes you have made.

6. Click on "Apache Webserver" link.

7. Click "Create virtual host":

» Handle connections to address: (select: Specific Address... enter the server's ip address)

» Port: (here you can select Default or port 80)

» Document Root: (specify in which directory domain's files will be saved.Eg: /var/www/html/mydomain)

» Server Name (enter domain name like this: www.mydomain.com:80 include the :80 part also)

» Add virtual server to file (leave default as Standard httpd.conf file)

» Copy directives from (leave default: Nowhere)

8. After you have created the virtual host, you need to save the changes. there is a link "Apply Changes" click on it.

NOTE: Normally, all virtual hosts point to the same main default page.
Now, to point different virtual hosts to their respective directories i.e you are serving the domain www.domain.tld and you wish to add the virtual host www.otherdomain.tld, which points at the same IP address. Then you simply add the following to httpd.conf(/etc/httpd/conf/httpd.conf):

NameVirtualHost *:80

<VirtualHost *:80>
ServerName www.domain.tld
ServerAlias domain.tld *.domain.tld
DocumentRoot /www/domain
</VirtualHost>

<VirtualHost *:80>
ServerName www.otherdomain.tld
DocumentRoot /www/otherdomain
</VirtualHost>

Then, restart httpd service.

That's it!