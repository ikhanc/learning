{{tag>magento}}

If you are on a magento server and want to take database dump of database currently used by magento website then follow below procedure :

1. Go to magento web root directory. (it is like **///home/???/public_html// **)** **

2. See** **//app/etc/local.xml//** ** file to extract database** ****//host, user, password, name// **** ** from it.** **

3. Take database dump. Use below command: ( if database host is //**localhost**, // then you can skip **//-h//** option below. )

''mysqldump -h {db-host} -u {db-user} -p {db-name}> {db-dump-file}.sql ''
