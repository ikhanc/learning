===== Create a STAGING for magento clients =====

----

===   Note : For LEMP (Nginx) + Virtual-Min setups only. (Server's provisioned using stack script.)   ===

----

For creating staging setup use following steps:

1. After login to server, Go to ''/root/mantra'' directory.'' ''

2. Run STAGING.sh script on screen. ''bash STAGING.sh''

3. Script will prompt for inputs, give accordingly. And after completion you will receive a mail containing details at //tech@mantraventures.com.//

Note : If you don't receive any mail then find details in **/root** directory in a **hidden file** containing //staging domain name//. Use ''ls -a'' command.
