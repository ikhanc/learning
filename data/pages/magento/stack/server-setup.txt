{{tag>magento}}

===== How to use the script for preparing hosting stack ? =====

----

1. [[http://wiki.mantraventures.com/magento/stack/clone-script-from-bitbucket|Clone the script]] bundle from bitbucket.

2. Move into the directory 'stack'. " ''cd stack'' ".

(Make sure to run screen command)

3. Run SERVER-SETUP file using bash. " ''bash SERVER-SETUP'' ". This will show you various options available for use.

4. Script do store all the logs in stacksetup.log file and brief logging stores in STACK.log file. In case you face any problem then you can trace error occurred using these 2 log files.

5. Some important scripts are moved in ''/root/mantra'' directory.
