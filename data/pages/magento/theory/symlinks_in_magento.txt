{{tag>magento}}

To add any hack to the magento store using symlinks, it need to enable the use of symlinks. Follow the below steps to do so:

1. In the admin backend : System > Configuration > ADVANCED > Developer > Template Settings > Allow Symlinks = Yes

Or

If you are using [[https://github.com/netz98/n98-magerun|n98-magerun]] tool then do the following:

1. Move to the magento install directory.

2. n98-magerun.par dev:symlinks

Done!!
