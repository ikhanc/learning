Create a file facebook.com.php in Plugins folder. Add the following code to the file:

<code php | facebook.php>

<?php
/****************************************************************/

* Plugin: Facebook
* Description:
* Changes the user-agent so Facebook sends us the mobile version.

function preRequest() {

global $toSet;
$toSet\[CURLOPT_USERAGENT\] = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows CE; IEMobile 6.1)';

}
?>

</code>
Save and enjoy Facebook with Glype\!