Login to MSSQL server with sa account. 

Execute below set of command.

  - use master
  - go
  - create LOGIN webuser14 with password='UwyZpTp2392Zpo'
  - go
  - use SQLDB12
  - go
  - create user webuser14
  - grant SELECT, INSERT, UPDATE, DELETE on dbo.Web_Integration TO webuser14
  - go



Where webuser14 is new user, UwyZpTp2392Zpo is password for the same.


It will provide read/write access on Web_Integration table of DB SQLDB12


So amend them accordingly to your needs.